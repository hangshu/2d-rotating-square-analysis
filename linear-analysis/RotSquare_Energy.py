import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import signal
# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 36        # Original fornt size is 8, multipy by above number
plt.rcParams['text.usetex']     = False
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
#plt.rcParams['text.latex.preamble'] = '\usepackage{sfmath}'
plt.rcParams['lines.linewidth'] = 4.   
plt.rcParams['lines.markersize'] = 8. 
plt.rcParams['legend.fontsize'] = 30        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 36        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 36        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in' 
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300

#==============================================================================
# Rotating square chain
#==============================================================================
class RotSquare_EnergyAnalysis:
    def __init__(self,L_side,q0_Lin,chain_config,
                 k_sp_Lin0,k_sp_Rot0,A_const,alpha,r1_morse, q_cube0=np.radians(45)):
        # Dimension
        self.L_side  = L_side
        self.q0_Lin  = q0_Lin
        self.q_cube0 = q_cube0
        self.chain_config = chain_config
        self.geo_L   = L_side/(2.*np.cos(q_cube0))  #[m] Half of the diagonal length of a cube
        print('Length L = {}'.format(self.geo_L*1e3))
        # Material properties ---------------------
        # Linear torsion spring
        self.k_sp_Lin0 = k_sp_Lin0
        self.k_sp_Rot0 = k_sp_Rot0
        # Morse potential
        self.A_const   = A_const
        self.alpha     = alpha
        self.r1_morse  = r1_morse
        self.r2_morse  = -r1_morse
        self.q_minima  = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)
        self.angle0, self.height0 = self.InitialConfiguration()

    def Calculate_k_sp_Rot0_exp(self, t_hinge=0.75*1e-3):
        # Parameters for nonlinear springsl
        width   = 10.*1e-3  # [m]: Height of the cube
        Eyng    = 0.26*1e6   # [Pa]: Young's modulus of PDMS (Based on experiments)
        coef_Larc = [ 4.48865275e-01, -1.30434577e-04] # Average
#        t_hinge   = 0.75*1e-3  # [m]: Hinge thickness
        L_arc = coef_Larc[0]*t_hinge + coef_Larc[1]
        # Analytical prediction
        I_2mom = width*(t_hinge**3)/12.
        k_sp_ana = Eyng*I_2mom/L_arc
        print('Calculated k_sp_Rot0 = {}'.format(k_sp_ana))
        return()
    
    def Find_LocalMinima(self, dQ_angle, fig_flag=False):
        E_TTL = self.PotentialEnergy(dQ_angle, self.k_sp_Rot0 , fig_flag=fig_flag)
        # Find local minimum points
#        minid = signal.argrelextrema(E_TTL, np.less)
        minid = signal.argrelmin(E_TTL, order=1)
        q_minima = np.zeros(len(minid[0]))
        for i_min in range(len(minid[0])):
            q_minima[i_min]  = 0.5*opt.newton(self.ForceAngle_relation, dQ_angle[minid[0][i_min]],
                                          args=(),tol=1e-8, maxiter=int(1e6))
        print('Local minima (deg): {}'.format(np.degrees(q_minima)))
        return(q_minima)
        
    
    def InitialConfiguration(self):
        if len(self.q_minima)==1:
            rot_angl0 = self.q_minima[0]
        elif len(self.q_minima)==2:
            if self.chain_config=='mono_L': # Uniform pre-compression/tension
                rot_angl0 = self.q_minima[0]
            elif self.chain_config=='mono_R': # Uniform pre-compression/tension
                rot_angl0 = self.q_minima[1]
            elif self.chain_config=='mono_C': # Uniform pre-compression/tension
                rot_angl0 = self.q_minima[1]
        else:
            if self.chain_config=='mono_C': # Uniform pre-compression/tension
                rot_angl0 = self.q_minima[1]
            elif self.chain_config=='mono_L': # Uniform pre-compression/tension
                rot_angl0 = self.q_minima[0]
            elif self.chain_config=='mono_R': # Uniform pre-compression/tension
                rot_angl0 = self.q_minima[2]
            else:
                rot_angl0 = self.q_minima[1]
        height0   = 2.*self.geo_L*np.cos(rot_angl0)  #[m]
        return(rot_angl0, height0)

    def ForceAngle_relation(self, Q_angle, fig_flag=False):
        # Q_angle = q_angle + angle0
        F_Lin    = self.k_sp_Rot0*(Q_angle-2.*self.q0_Lin)
        F_Morse1 =  2.*self.alpha*self.A_const*( np.exp(2.*self.alpha*(Q_angle-2.*self.r1_morse)) +\
                                             -np.exp(   self.alpha*(Q_angle-2.*self.r1_morse)))
        F_Morse2 = -2.*self.alpha*self.A_const*( np.exp(-2.*self.alpha*(Q_angle-2.*self.r2_morse)) +\
                                             -np.exp(-   self.alpha*(Q_angle-2.*self.r2_morse)))
        F_TTL = F_Lin + F_Morse1 + F_Morse2
        
        # Figure
        if fig_flag==True:
            plt.figure('Moment-Angle')
            plt.axhline(y=0, linewidth=2, color='k')
            plt.axvline(x=0, linewidth=2, color='k')
            plt.plot(np.degrees(Q_angle), F_TTL, 'b')
            plt.xlabel('Rotational angle (deg)')
            plt.ylabel('Force')
            plt.ylim(-0.003, 0.003)    
        return(F_TTL)
    
    
    
    def PotentialEnergy(self, dQ_angle, k_rot, fig_flag=False):
        # Q_angle = q_angle + angle0
        E_Lin    = 0.5*k_rot*(dQ_angle-2.*self.q0_Lin)**2
        E_Morse1 = self.A_const*( np.exp(2.*self.alpha*(dQ_angle-2.*self.r1_morse)) \
                             - 2.*np.exp(   self.alpha*(dQ_angle-2.*self.r1_morse)) )
        E_Morse2 = self.A_const*( np.exp(-2.*self.alpha*(dQ_angle-2.*self.r2_morse)) \
                             - 2.*np.exp(   -self.alpha*(dQ_angle-2.*self.r2_morse)) )
        E_TTL    = E_Lin + E_Morse1 + E_Morse2
        
        
        if fig_flag==True:
            # Find local maximum point
            maxid = signal.argrelextrema(E_TTL, np.greater)
            r_maxima = np.zeros(len(maxid[0]))
            for i_max in range(len(maxid[0])):
                r_maxima[i_max] = opt.newton(self.ForceAngle_relation, dQ_angle[maxid[0][i_max]],
                                          args=(),tol=1e-8, maxiter=int(1e6))
            # Find local minimum points
            minid = signal.argrelextrema(E_TTL, np.less)
            r_minima = np.zeros(len(minid[0]))
            for i_min in range(len(minid[0])):
                r_minima[i_min]  = opt.newton(self.ForceAngle_relation, dQ_angle[minid[0][i_min]],
                                              args=(),tol=1e-8, maxiter=int(1e6))            
            plt.figure('Elastic energy')
#            plt.plot(r_disp, E_Morse1, 'k')
#            plt.plot(r_disp, E_Morse2, 'r')
            plt.plot(np.degrees(dQ_angle), E_Lin, 'k', label='Linear spring')
            plt.plot(np.degrees(dQ_angle), E_TTL, 'b', label='Resultant potential')
            plt.plot(np.degrees(dQ_angle[minid]), E_TTL[minid], 'rx')
            plt.plot(np.degrees(dQ_angle[maxid]), E_TTL[maxid], 'kx')
            plt.plot(np.degrees(r_maxima), E_TTL[maxid], 'yx')
            plt.legend(loc='upper center')
            plt.xlabel('Rotational angle $\\Theta=2(\\theta+\\theta_0)$(deg)')
            plt.ylabel('Energy')
#            plt.ylim(self.A_const, 20)
            
            fig = plt.figure('Potential energy', figsize=(8,6)) # For journal: figsize=(9,7)
    #        ax  = fig.add_subplot(111)
            plt.title('$\\theta^{{(0)}}_{{Lin}}={0:1.1f}^\circ$'.format(np.degrees(self.q0_Lin)))
    #        plt.axvline(x=0, color='k', linestyle='solid', linewidth=1)
    #        plt.axvline(x=0.5*np.pi, color='k', linestyle='solid', linewidth=1)
            plt.plot(0.5*dQ_angle, E_TTL/self.A_const, color='b', linewidth=7)
            plt.plot(0.5*dQ_angle[minid], E_TTL[minid]/self.A_const, 'v', color='k', markersize=18)
    #        plt.ylabel('Normalized energy, $U(\\theta)/A$')
            plt.ylabel('Energy, $U/A$')
            plt.xlabel('Angle, $\\theta+\\theta^{(0)}$')
            plt.ylim(ymax=1.3)
#            plt.xlim(min(q_hinge)+q0_Lin-0.1, max(q_hinge)+q0_Lin+0.1)
            plt.xticks([-0.25*np.pi, 0, 0.25*np.pi],['$-\pi/4$', '$0$', '$\pi/4$'])
        return(E_TTL)
  
    def EnergyPlot_3D(self, Q0_range, zmax=20, init_view=(19, -77)):
        N_angle  = 301
        dQ_angle = 2.*np.radians(np.linspace(-45., 45, N_angle))
        N_step   = len(Q0_range)
        Energy   = []
        E_min    = []
        E_offset = 0.7
        for i_step in range(N_step):
            self.q0_Lin = Q0_range[i_step]
            self.q_minima  = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)
            self.angle0, self.height0 = self.InitialConfiguration()
            E_TTL = self.PotentialEnergy(dQ_angle, self.k_sp_Rot0, fig_flag=False)
            # Find local minimum points
            minid = signal.argrelextrema(E_TTL, np.less)
            # Store data
            Energy.append(E_TTL/self.A_const+E_offset)
            E_min.append(minid[0])
        # Figure ==============================================================
        fig = plt.figure('Energy 3D plot', figsize=(18,7))
        ax = fig.add_subplot(111, projection='3d')
        plt.subplots_adjust(left=0.01, right=0.97, top=0.92, bottom=0.15)
        z_offset = -0
        alpha    = 0.3
        markersize = 90
        
        # Plot energy curves
#        col = plt.cm.Set1(np.linspace(0, 1, N_step))
        col = ['red', 'lightcoral', 'green', 'royalblue', 'blue']
        for ii in range(N_step):
            i_step = ii
            Q0 = Q0_range[i_step]*np.ones(N_angle) 
            Q0_min = Q0_range[i_step]*np.ones(len(E_min[i_step]))
            verts = [(np.degrees(Q0[i]), np.degrees(0.5*dQ_angle[i]), Energy[i_step][i]) for i in range(N_angle)] # Upper curve
            verts = verts + [(np.degrees(Q0[-i-1]), np.degrees(0.5*dQ_angle[-i-1]), z_offset) for i in range(N_angle)] # Lower curve
            ax.add_collection3d(Poly3DCollection([verts], alpha = alpha, facecolor=col[i_step], linewidths=1)) # Add a polygon instead of fill_between
            ax.plot(np.degrees(Q0), np.degrees(0.5*dQ_angle), Energy[i_step], color=col[i_step], zorder=3, linewidth=6)
            ax.scatter(np.degrees(Q0_min), np.degrees(0.5*dQ_angle[E_min[i_step]]), Energy[i_step][E_min[i_step]], c='k', marker='v', s=markersize, zorder=10)
#        # Origin
#        ax.scatter(0., 0., E_tmp[-1], c='k', marker='o', s=markersize)
        
        
#        ax.legend()
#        ax.set_xlim3d(0., 1.)
##        ax.set_ylim3d(-0.5*np.pi, 0.5*np.pi)
##        ax.set_zlim3d(z_offset, max(E_tmp))
            
        tmp_planes = ax.zaxis._PLANES 
        ax.zaxis._PLANES = ( tmp_planes[2], tmp_planes[3], 
                             tmp_planes[0], tmp_planes[1], 
                             tmp_planes[4], tmp_planes[5])
        
        ax.axes.set_xlim3d(np.degrees(Q0_range[0])+0.6, np.degrees(Q0_range[-1])-0.6)
        ax.axes.set_ylim3d(np.degrees(0.5*dQ_angle[0])+1.8, np.degrees(0.5*dQ_angle[-1])-1.8) 
        ax.axes.set_zlim3d(bottom=0.04) 
        ax.set_xlabel('$\\theta_{Lin}$ ($^\circ$)')
        ax.set_ylabel('Angle, $\\theta+\\theta^{(0)}$ ($^\circ$)')
        ax.set_zlabel('Energy, $E/A$')
#        ax.zaxis.set_rotate_label(False)  # disable automatic rotation
#        ax.set_zlabel('Energy, $E/k_{\\theta}$', rotation=90)
        ax.yaxis.labelpad=40
        ax.xaxis.labelpad=80
        ax.zaxis.labelpad=20
        ax.set_yticks([-45, 0, 45])
        ax.set_yticklabels(['$-45$', '$0$', '$45$'])
#        ax.set_xticks([0., 45., 90.])
#        ax.set_xticklabels(['$0$', '$45$', '$90$'])
        ax.set_zticks([0.])
        ax.set_zticklabels([''])
        ax.xaxis._axinfo['tick']['inward_factor'] = 0
        ax.xaxis._axinfo['tick']['outward_factor'] = 0.4
        ax.yaxis._axinfo['tick']['inward_factor'] = 0
        ax.yaxis._axinfo['tick']['outward_factor'] = 0.4
        ax.zaxis._axinfo['tick']['inward_factor'] = 0
        ax.zaxis._axinfo['tick']['outward_factor'] = 0.4
        ax.zaxis._axinfo['tick']['outward_factor'] = 0.4

        ax.grid(False)
        ax.xaxis.pane.set_edgecolor('black')
        ax.yaxis.pane.set_edgecolor('black')
        ax.zaxis.pane.set_edgecolor('black')
        ax.xaxis.pane.fill = False
        ax.yaxis.pane.fill = False
        ax.zaxis.pane.fill = False
        
        ax.view_init(*init_view)
        return()

if __name__ == "__main__":
    #==============================================================================
    # Geometry
    #==============================================================================
    L_side = 12.*1e-3  #[m] Side length of the cube
    # Initial configuration
    q0_Lin  = np.radians(1e-4)
    # Configuration of the chain
    # 'mono_C', 'mono_L', 'mono_R', 'PB_1', 'PB_2'
    chain_config = 'mono_L' 
    #==============================================================================
    # Material propeties
    #==============================================================================
    # Spring constant for linear spring
    k_sp_Lin0 = 670.8
    k_sp_Rot0 = 0.00044325837185769376  # for t_hinge=0.75*1e-3 [m]
    # Parameters for the Morse potential function -----
    A_const  = 0.0005477163091763655
    alpha    = 4.966064409064443
#    r_morse_appx = {'0.75': 0.74422634,
#                    '1.00': 0.72538542,
#                    '1.40': 0.70040489,
#                    '1.70': 0.68699766}
    r1_morse = 0.74422634 # for t_hinge=0.75*1e-3 [m]
    r2_morse = -r1_morse


    rotsq_ene = RotSquare_EnergyAnalysis(L_side,q0_Lin,chain_config,
                            k_sp_Lin0,k_sp_Rot0,A_const,alpha,r1_morse)
    # Calculate the linear torsion spring constant from the experiment
#    rotsq_ene.Calculate_k_sp_Rot0_exp(t_hinge=0.75*1e-3)
    # Moment
#    rotsq_ene.ForceAngle_relation(Q_angle=2.*np.radians(np.linspace(-45., 45, 301)), fig_flag=True)
    # Potential energy landscape
    rotsq_ene.PotentialEnergy(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), k_rot=k_sp_Rot0, fig_flag=True)

#    rotsq_ene.EnergyPlot_3D(Q0_range=np.linspace(-np.radians(15),np.radians(15), 5))