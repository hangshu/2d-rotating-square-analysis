import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.colorbar import Colorbar
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import cmath
import scipy.optimize as opt
from scipy import signal
from scipy import integrate
from scipy import linalg
from scipy.interpolate import interp1d
import os
import sys
import shutil   #High level file operation
from matplotlib import cm
from matplotlib.ticker import LinearLocator
#from IMS_VTK4Animation import CubicModule_VTKanimation
sys.path.append('/Documents/2d-rotating-square-analysis/linear-analysis/')
from RotSquare_Energy import RotSquare_EnergyAnalysis
# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 24       # Original fornt size is 8, multipy by above number
plt.rcParams['text.usetex']     = True
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['lines.linewidth'] = 3.   
plt.rcParams['lines.markersize'] = 10. 
plt.rcParams['legend.fontsize'] = 16        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in' 
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300

#==============================================================================
# Rotating square chain
#==============================================================================
class RotSq_Eigenvalue_unit(RotSquare_EnergyAnalysis):
    def __init__(self, L_side, q0_Lin, chain_config, k_u, k_v, k_s, mass1, r_inrt1, \
                 A_const, alpha, r1_morse, q_cube0=np.radians(45)):
        # Dimension
        self.L_side  = L_side
        self.q0_Lin  = q0_Lin
        self.chain_config = chain_config
        self.geo_L   = L_side/(2.*np.cos(q_cube0))  #[m] Half of the diagonal length of a cube
        # Material properties
        # self.k_sp_Lin  = k_sp_Lin
        # self.k_sp_Rot  = k_sp_Rot
        self.A_const   = A_const
        self.alpha     = alpha
        self.r1_morse  = r1_morse
        self.r2_morse  = -r1_morse
        
        super().__init__(L_side, q0_Lin, chain_config,
                             k_u, k_s, A_const, alpha, r1_morse)
        self.q_minima   = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)
        self.angle0,self.height0 = self.InitialConfiguration()
        print(self.angle0)
        #spring constants
        self.k_u = k_u
        self.k_v = k_v
        self.k_s = k_s
        
        #mass 
        self.M = mass1
        self.J = r_inrt1
        
    def Hamiltonian_mono(self, kn, km, nm_flag):
        Hmat = np.zeros((3,3),dtype=complex)
        exp_pn = cmath.exp(1j*kn*self.height0)
        exp_mn = cmath.exp(-1j*kn*self.height0)
        exp_pm = cmath.exp(1j*km*self.height0)
        exp_mm = cmath.exp(-1j*km*self.height0)
        
        # Coefficient for Morse potential function
        wrk1 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(2.*self.alpha*(2.*self.angle0-2.*self.r1_morse)) \
                 - np.exp(   self.alpha*(2.*self.angle0-2.*self.r1_morse)) )
        wrk2 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(-2.*self.alpha*(2.*self.angle0-2.*self.r2_morse)) \
                 - np.exp(   -self.alpha*(2.*self.angle0-2.*self.r2_morse)) )
        T_slope = wrk1 + wrk2
        
        #################matrix for n direction 
#         # alpha_uu
        Auu_n = self.k_u*(exp_pn+exp_mn-2.)/self.M
        Auu_m = self.k_v*(exp_pm+exp_mm-2.)/self.M
#         # alpha_up
        Aup_n = self.k_u*self.geo_L*np.sin(self.angle0)*(exp_pn-exp_mn)/self.M
        Aup_m = nm_flag*self.k_v*self.geo_L*np.cos(self.angle0)*(exp_pm-exp_mm)/self.M
#         # alpha_vv
        Avv_n = self.k_v*(exp_pn+exp_mn-2.)/self.M
        Avv_m = self.k_u*(exp_pm+exp_mm-2.)/self.M    
        
       # alpha_vp
        Avp_n = -nm_flag*self.k_v*self.geo_L*np.cos(self.angle0)*(exp_pn-exp_mn)/self.M
        Avp_m = self.k_u*self.geo_L*np.sin(self.angle0)*(exp_pm-exp_mm)/self.M
        
        # alpha_pu
        Apu_n = self.k_u*self.geo_L*np.sin(self.angle0)*(-exp_pn+exp_mn)/self.J
        Apu_m = nm_flag*self.k_v*self.geo_L*np.cos(self.angle0)*(exp_pm-exp_mm)/self.J
        
        
        # alpha_pv
        Apv_n = nm_flag*self.k_v*self.geo_L*np.cos(self.angle0)*(-exp_pn+exp_mn)/self.J
        Apv_m = self.k_u*self.geo_L*np.sin(self.angle0)*(-exp_pm+exp_mm)/self.J
        # alpha_pp

        # alpha_pp
        a1n = (-self.k_s-self.k_u*self.geo_L**2*np.sin(self.angle0)**2+ \
              self.k_v*self.geo_L**2*np.cos(self.angle0)**2 \
              - T_slope)*(exp_pn+exp_mn)
            
        a1m = (-self.k_s-self.k_u*self.geo_L**2*np.sin(self.angle0)**2+ \
              self.k_v*self.geo_L**2*np.cos(self.angle0)**2 \
              - T_slope)*(exp_pm+exp_mm)
            
#        a2 = -2.*self.k_sp_Lin*self.geo_L**2*(1.+np.sin(self.angle0)**2)
        a2 = -4.*(self.k_u*np.sin(self.angle0)**2+self.k_v*np.cos(self.angle0)**2)*self.geo_L**2
        a3 = -4.*self.k_s
        a4 = -4.*T_slope
        App = (a1n+a1m+a2+a3+a4)/self.J
        
        Hmat[0,0] = Auu_n+Auu_m
        Hmat[0,2] = Aup_n+Aup_m
        Hmat[1,1] = Avv_n+Avv_m
        Hmat[1,2] = Avp_n+Avp_m
        Hmat[2,0] = Apu_n+Apu_m
        Hmat[2,1] = Apv_n+Apv_m
        Hmat[2,2] = App

        return(Hmat)
    
 
    
    def Analyze_DispersionRelation_Monomer(self, k_step, nm_flag, v_Pu_min=1e-4, fig_flag=True):
        df = 3
        k_vecn = np.linspace(0, np.pi/self.height0, k_step)
        k_vecm = np.linspace(0, np.pi/self.height0, k_step)
        k_veck = np.linspace(np.sqrt(2)*np.pi/self.height0, 0, k_step)
        omega = np.zeros((3,k_step*3),dtype=complex)
        Pu    = np.zeros((3,k_step*3))
        v_eig0 = np.zeros((1,k_step*3),dtype=complex)
        v_eig1 = np.zeros((1,k_step*3),dtype=complex)
        v_eig2 = np.zeros((1,k_step*3),dtype=complex)
        # Explicit form
        # eig_value = np.zeros((2,k_step),dtype=complex)
        # eig_value[0,:],eig_value[1,:],v_eig1,v_eig2 = self.Eigenvalue_homogeneous(k_vec, mass1, r_inrt1, k_Lin1, k_Rot1)
        
        for i in range(k_step):
            i_lower = np.zeros(3)
            i_mid = np.zeros(3)
            i_upper = np.zeros(3)
            Hmat_m = self.Hamiltonian_mono(k_vecn[i], 0, nm_flag)
            Hmat_r = self.Hamiltonian_mono(np.pi/self.height0, k_vecm[i], nm_flag)
            Hmat_k = self.Hamiltonian_mono(np.sin(np.radians(45))*k_veck[i], np.cos(np.radians(45))*k_veck[i], nm_flag)
            #eigen calc for n 
            la_m,v_m = np.linalg.eig(-Hmat_m)
            la_r,v_r = np.linalg.eig(-Hmat_r)
            la_k,v_k = np.linalg.eig(-Hmat_k)
            Eig_sortm = np.argsort(la_m)      
            Eig_sortr = np.argsort(la_r)  
            Eig_sortk = np.argsort(la_k)  
            
            i_lower[0]  = Eig_sortm[0]
            i_mid[0] = Eig_sortm[1]
            i_upper[0]  = Eig_sortm[2]    
            i_lower[1]  = Eig_sortr[0]
            i_mid[1] = Eig_sortr[1]
            i_upper[1]  = Eig_sortr[2] 
            i_lower[2]  = Eig_sortk[0]
            i_mid[2] = Eig_sortk[1]
            i_upper[2]  = Eig_sortk[2] 

            omega[0,i] = cmath.sqrt(la_m[int(i_lower[0])])
            omega[0,i+k_step] = cmath.sqrt(la_r[int(i_lower[1])])
            omega[0,i+2*k_step] = cmath.sqrt(la_k[int(i_lower[2])])
            omega[1,i] = cmath.sqrt(la_m[int(i_mid[0])])
            omega[1,i+k_step] = cmath.sqrt(la_r[int(i_mid[1])])
            omega[1,i+2*k_step] = cmath.sqrt(la_k[int(i_mid[2])]) 
            omega[2,i] = cmath.sqrt(la_m[int(i_upper[0])])
            omega[2,i+k_step] = cmath.sqrt(la_r[int(i_upper[1])])
            omega[2,i+2*k_step] = cmath.sqrt(la_k[int(i_upper[2])])
            

            # Polarization
            Pu[0,i] = np.linalg.norm(v_m[0,int(i_lower[0])])/(np.linalg.norm(v_m[0,int(i_lower[0])]) \
                                       +np.linalg.norm(v_m[1,int(i_lower[0])])+np.linalg.norm(v_m[2,int(i_lower[0])]))
            Pu[0,i+k_step] = np.linalg.norm(v_r[0,int(i_lower[1])])/(np.linalg.norm(v_r[0,int(i_lower[1])]) \
                                       +np.linalg.norm(v_r[1,int(i_lower[1])])+np.linalg.norm(v_r[2,int(i_lower[1])]))
                
            Pu[0,i+2*k_step] = np.linalg.norm(v_k[0,int(i_lower[2])])/(np.linalg.norm(v_k[0,int(i_lower[2])]) \
                                       +np.linalg.norm(v_k[1,int(i_lower[2])])+np.linalg.norm(v_k[2,int(i_lower[2])]))
                
            Pu[1,i] = np.linalg.norm(v_m[0,int(i_mid[0])])/(np.linalg.norm(v_m[0,int(i_mid[0])]) \
                                       +np.linalg.norm(v_m[1,int(i_mid[0])])+np.linalg.norm(v_m[2,int(i_mid[0])]))
            Pu[1,i+k_step] = np.linalg.norm(v_r[0,int(i_mid[1])])/(np.linalg.norm(v_r[0,int(i_mid[1])]) \
                                       +np.linalg.norm(v_r[1,int(i_mid[1])])+np.linalg.norm(v_r[2,int(i_mid[1])]))
            Pu[1,i+2*k_step] = np.linalg.norm(v_k[0,int(i_mid[2])])/(np.linalg.norm(v_k[0,int(i_mid[2])]) \
                                       +np.linalg.norm(v_k[1,int(i_mid[2])])+np.linalg.norm(v_k[2,int(i_mid[2])]))
                
            Pu[2,i] = np.linalg.norm(v_m[0,int(i_upper[0])])/(np.linalg.norm(v_m[0,int(i_upper[0])]) \
                                       +np.linalg.norm(v_m[1,int(i_upper[0])])+np.linalg.norm(v_m[2,int(i_upper[0])]))
            Pu[2,i+k_step] = np.linalg.norm(v_r[0,int(i_upper[1])])/(np.linalg.norm(v_r[0,int(i_upper[1])]) \
                                       +np.linalg.norm(v_r[1,int(i_upper[1])])+np.linalg.norm(v_r[2,int(i_upper[1])]))
            Pu[2,i+2*k_step] = np.linalg.norm(v_k[0,int(i_upper[2])])/(np.linalg.norm(v_k[0,int(i_upper[2])]) \
                                       +np.linalg.norm(v_k[1,int(i_upper[2])])+np.linalg.norm(v_k[2,int(i_upper[2])]))
            # Eigen vectors
        w0 = np.sqrt(self.k_u/self.M)
    
    
        
        # Plot ----------------------------------------------------------------
        if fig_flag==True:
            v_Pu_min = 1e-4
            k_vec = np.linspace(0, 3*np.pi/self.height0, 3*k_step)
            fig1 = plt.figure('2D Dispersion relation')
        
            fig1.subplots_adjust(top=0.75,bottom=0.2,left=0.2,right=0.75)
            ax1 = fig1.gca()
            gp_offset = 6
            for i_branch in range(2):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max+gp_offset < w2_min:
                    plt.fill_between(k_vec, w1_max/(w0), w2_min/(w0), where=None, facecolor='gray', alpha=0.6)
                    
            pu_map = plt.cm.get_cmap('jet')
            cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm2.set_array([])
            for j in range(3*k_step):
                plt.plot(k_vec[j], np.real(omega[0,j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,j]) )
                plt.plot(k_vec[j], np.real(omega[1,j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,j]) )
                plt.plot(k_vec[j], np.real(omega[2,j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,j]) )
                
            ax_divider = make_axes_locatable(ax1)
            cax4 = ax_divider.append_axes("top", size="7%", pad="5%")
            cbar4 = plt.colorbar(cm2,cax=cax4,orientation="horizontal")
            cbar4.ax.minorticks_on()
            cbar4.ax.set_xticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            cbar4.ax.set_xticks(minor=True)
            cax4.xaxis.set_ticks([1e-4,1e-2,1], minor=True)
            cax4.xaxis.set_ticks_position("top")
            cax4.xaxis.set_label_position("top")
            cbar4.solids.set_edgecolor("face")
            cbar4.set_label('Polarization index, $P_u$')
            cbar4.ax.minorticks_on()    
            
            #ax1.set_title('Dispersion relation')
            ax1.set_xlabel('Wave number $k$')
            ax1.set_ylabel('Frequency $\omega/\omega_0$')
            ax1.set_xticks([0.,np.pi/self.height0, 2*np.pi/self.height0, 3*np.pi/self.height0])
            ax1.set_xticklabels(['$\Gamma$','$X$', '$M$', '$\Gamma$'])
            
            
            fig2 = plt.figure('2D dispersion_1')
            fig2.subplots_adjust(top=0.75,bottom=0.2,left=0.2,right=0.75)
            ax2 = fig2.gca()
            gp_offset = 6
            for i_branch in range(2):
                w1_max = max(omega[i_branch,0:k_step])
                w2_min = min(omega[i_branch+1,0:k_step])
                if w1_max+gp_offset < w2_min:
                    plt.fill_between(k_vec[0:k_step], w1_max/(w0), w2_min/(w0), where=None, facecolor='gray', alpha=0.6)
                    plt.fill_between(-k_vec[0:k_step], w1_max/(w0), w2_min/(w0), where=None, facecolor='gray', alpha=0.6)           
            pu_map = plt.cm.get_cmap('jet')
            cm3 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm3.set_array([])
            for j in range(k_step):
                plt.plot(k_vec[j], np.real(omega[0,j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,j]) )
                plt.plot(k_vec[j], np.real(omega[1,j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,j]) )
                plt.plot(k_vec[j], np.real(omega[2,j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,j]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[0,k_step-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,k_step-j]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[1,k_step-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,k_step-j]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[2,k_step-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,k_step-j]) )                
            ax_divider = make_axes_locatable(ax2)
            cax3 = ax_divider.append_axes("top", size="7%", pad="5%")
            cbar3 = plt.colorbar(cm3,cax=cax3,orientation="horizontal")
            cbar3.ax.minorticks_on()
            cbar3.ax.set_xticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            cbar3.ax.set_xticks(minor=True)
            cax3.xaxis.set_ticks([1e-4,1e-2,1], minor=True)
            cax3.xaxis.set_ticks_position("top")
            cax3.xaxis.set_label_position("top")
            cbar3.solids.set_edgecolor("face")
            cbar3.set_label('Polarization index, $P_u$')
            cbar3.ax.minorticks_on()    
            
            #ax1.set_title('Dispersion relation')
            ax2.set_xlabel('Wave number $k$')
            ax2.set_ylabel('Frequency $\omega/\omega_0$')
            ax2.set_xticks([-np.pi/self.height0, 0.,np.pi/self.height0])
            ax2.set_xticklabels(['$-X$','$\Gamma$', '$X$'])
            
            
            fig3 = plt.figure('2D dispersion_2')
            fig3.subplots_adjust(top=0.75,bottom=0.2,left=0.2,right=0.75)
            ax3 = fig3.gca()
            gp_offset = 6
            for i_branch in range(2):
                w1_max = max(omega[i_branch,k_step:2*k_step])
                w2_min = min(omega[i_branch+1,k_step:2*k_step])
                if w1_max+gp_offset < w2_min:
                    plt.fill_between(k_vec[0:k_step], w1_max/(w0), w2_min/(w0), where=None, facecolor='gray', alpha=0.6)
                    plt.fill_between(-k_vec[0:k_step], w1_max/(w0), w2_min/(w0), where=None, facecolor='gray', alpha=0.6)                    
            pu_map = plt.cm.get_cmap('jet')
            cm4 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm4.set_array([])
            for j in range(k_step):
                plt.plot(k_vec[j], np.real(omega[0,j+k_step])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,j+k_step]) )
                plt.plot(k_vec[j], np.real(omega[1,j+k_step])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,j+k_step]) )
                plt.plot(k_vec[j], np.real(omega[2,j+k_step])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,j+k_step]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[0,2*k_step-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,2*k_step-j]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[1,2*k_step-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,2*k_step-j]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[2,2*k_step-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,2*k_step-j]) )                
            ax_divider = make_axes_locatable(ax3)
            cax2 = ax_divider.append_axes("top", size="7%", pad="5%")
            cbar2 = plt.colorbar(cm4,cax=cax2,orientation="horizontal")
            cbar2.ax.minorticks_on()
            cbar2.ax.set_xticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            cbar2.ax.set_xticks(minor=True)
            cax2.xaxis.set_ticks([1e-4,1e-2,1], minor=True)
            cax2.xaxis.set_ticks_position("top")
            cax2.xaxis.set_label_position("top")
            cbar2.solids.set_edgecolor("face")
            cbar2.set_label('Polarization index, $P_u$')
            cbar2.ax.minorticks_on()    
            
            #ax1.set_title('Dispersion relation')
            ax3.set_xlabel('Wave number $k$')
            ax3.set_ylabel('Frequency $\omega/\omega_0$')
            ax3.set_xticks([-np.pi/self.height0, 0.,np.pi/self.height0])
            ax3.set_xticklabels(['$-M$','$X$', '$M$'])
            
            
            
            fig4 = plt.figure('2D dispersion_3')
            fig4.subplots_adjust(top=0.75,bottom=0.2,left=0.2,right=0.75)
            ax4 = fig4.gca()
            gp_offset = 6
            for i_branch in range(2):
                w1_max = max(omega[i_branch,2*k_step:])
                w2_min = min(omega[i_branch+1,2*k_step:])
                if w1_max+gp_offset < w2_min:
                    plt.fill_between(k_vec[0:k_step], w1_max/(w0), w2_min/(w0), where=None, facecolor='gray', alpha=0.6)
                    plt.fill_between(-k_vec[0:k_step], w1_max/(w0), w2_min/(w0), where=None, facecolor='gray', alpha=0.6)                    
            pu_map = plt.cm.get_cmap('jet')
            cm5= plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm5.set_array([])
            for j in range(k_step):
                plt.plot(k_vec[j], np.real(omega[0,j+2*k_step])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,j+2*k_step]) )
                plt.plot(k_vec[j], np.real(omega[1,j+2*k_step])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,j+2*k_step]) )
                plt.plot(k_vec[j], np.real(omega[2,j+2*k_step])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,j+2*k_step]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[0,3*k_step-1-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,3*k_step-1-j]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[1,3*k_step-1-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,3*k_step-1-j]) )
                plt.plot(-k_vec[k_step-j], np.real(omega[2,3*k_step-1-j])/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,3*k_step-1-j]) )                
            ax_divider = make_axes_locatable(ax4)
            cax1 = ax_divider.append_axes("top", size="7%", pad="5%")
            cbar1 = plt.colorbar(cm5,cax=cax1,orientation="horizontal")
            cbar1.ax.minorticks_on()
            cbar1.ax.set_xticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            cbar1.ax.set_xticks(minor=True)
            cax1.xaxis.set_ticks([1e-4,1e-2,1], minor=True)
            cax1.xaxis.set_ticks_position("top")
            cax1.xaxis.set_label_position("top")
            cbar1.solids.set_edgecolor("face")
            cbar1.set_label('Polarization index, $P_u$')
            cbar1.ax.minorticks_on()    
            
            #ax1.set_title('Dispersion relation')
            ax4.set_xlabel('Wave number $k$')
            ax4.set_ylabel('Frequency $\omega/\omega_0$')
            ax4.set_xticks([-np.pi/self.height0, 0.,np.pi/self.height0])
            ax4.set_xticklabels(['$-M$','$\Gamma$', '$M$'])
            # ax1.set_yticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax1.set_yticklabels(['$-\\frac{\pi}{2}$', '$0$', '$\\frac{\pi}{2}$'])
            # fig1 = plt.figure('2D Dispersion lower branch')
            # ax1 = fig1.gca()
            # cmap = plt.get_cmap('jet')
            # sm1 = plt.imshow(np.real(omega_l)/w0, aspect='auto', 
            #                   origin = 'lower', interpolation ='spline16', cmap =cmap,
            #                                   extent=[k_vecn[0],k_vecn[-1],k_vecn[0],k_vecn[-1]])
            # ax1.set_title('Dispersion relation lower branch')
            # ax1.set_ylabel('Wave number $k_y$')
            # ax1.set_xlabel('Wave number $k_x$')
         
            # cbar1 = fig1.colorbar(sm1,shrink=0.8,cmap=cmap)
            # cbar1.set_label('Normalized Frequency $\omega/\omega_0$')
            # ax1.set_xticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax1.set_xticklabels(['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
            # ax1.set_yticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax1.set_yticklabels(['$-\\frac{\pi}{2}$', '$0$', '$\\frac{\pi}{2}$'])
    
    
            # fig2 = plt.figure('2D Dispersion middle branch')
            # ax2 = fig2.gca()
            # cmap = plt.get_cmap('jet')
            # sm2 = plt.imshow(np.real(omega_m)/w0, aspect='auto', 
            #                   origin = 'lower', interpolation ='spline16', cmap =cmap,
            #                                   extent=[k_vecn[0],k_vecn[-1],k_vecn[0],k_vecn[-1]])
            
            # ax2.set_title('Dispersion relation middle branch')
            # ax2.set_ylabel('Wave number $k_y$')
            # ax2.set_xlabel('Wave number $k_x$')
           
            # cbar2 = fig2.colorbar(sm2,shrink=0.8,cmap=cmap)
            # cbar2.set_label('Normalized Frequency $\omega/\omega_0$')
            # ax2.set_xticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax2.set_xticklabels(['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
            # ax2.set_yticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax2.set_yticklabels(['$-\\frac{\pi}{2}$', '$0$', '$\\frac{\pi}{2}$'])
            
            
            # fig3 = plt.figure('2D Dispersion upper branch')
            # ax3 = fig3.gca()
            # cmap = plt.get_cmap('jet')
            # sm3 = plt.imshow(np.real(omega_u)/w0, aspect='auto', 
            #                   origin = 'lower', interpolation ='spline16', cmap =cmap,
            #                                   extent=[k_vecn[0],k_vecn[-1],k_vecn[0],k_vecn[-1]])
            
            # ax3.set_title('Dispersion relation upper branch')
            # ax3.set_ylabel('Wave number $k_y$')
            # ax3.set_xlabel('Wave number $k_x$')
            
            # cbar3 = fig3.colorbar(sm3,shrink=0.8,cmap=cmap)
            # cbar3.set_label('Normalized Frequency $\omega/\omega_0$')
            # ax3.set_xticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax3.set_xticklabels(['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
            # ax3.set_yticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax3.set_yticklabels(['$-\\frac{\pi}{2}$', '$0$', '$\\frac{\pi}{2}$'])
            
            
            #surface plots 
            # pu_map = plt.cm.get_cmap('jet')
            # cm4 = plt.cm.ScalarMappable(cmap=pu_map)
            # cm4.set_array([])
            # fcolor_u = cm4.to_rgba(Pu_u)
            # fcolor_m = cm4.to_rgba(Pu_m)
            # fcolor_l = cm4.to_rgba(Pu_l)
            # N,M = np.meshgrid(k_vecn, k_vecm)
            # fig4 = plt.figure('3D surface')
            # ax4 = fig4.add_subplot(111, projection='3d')
            # # for i in range(0, k_step, 10):
            # #     for j in range(0, k_step,10):
            # #         ax4.scatter(k_vecn[i], k_vecm[j], np.real(omega_u[i,j]/w0), marker = 'o', color=fcolor_u[i,j])
            # surf1 = ax4.plot_surface(N, M, np.real(omega_u/w0), rstride=1, cstride=1, facecolors=fcolor_u,
            #            linewidth=0, antialiased=False)          
            # surf2 = ax4.plot_surface(N, M, np.real(omega_m/w0), rstride=1, cstride=1, facecolors=fcolor_m,
            #            linewidth=0, antialiased=False)
            # surf3 = ax4.plot_surface(N, M, np.real(omega_l/w0), rstride=1, cstride=1, facecolors=fcolor_l,
            #             linewidth=0, antialiased=False)
            
            # # cbar4 = fig4.colorbar(surf1, shrink=0.8)
            # # cbar4.set_label('Normalized Frequency $\omega/\omega_0$')
            # cbar4 = plt.colorbar(cm4, shrink = 0.8)
            # cbar4.solids.set_edgecolor("face")
            # cbar4.set_label('Polarization index, $P_u$')
            # cbar4.set_ticks([0,1])
            # ax4.set_xticks([-np.pi/self.height0,0.,np.pi/self.height0])
            # ax4.set_xticklabels(['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
            # ax4.set_yticks([-np.pi/self.height0,0.,np.pi/self.height0])
         #   ax4.set_yticklabels(['$-\\frac{\pi}{2}$', '$0$', '$\\frac{\pi}{2}$'])
           # ax4.set_ylabel('Wave number $k_y$')
            #ax4.set_xlabel('Wave number $k_x$')
            #ax4.set_zlabel('Normalized Frequency $\omega/\omega_0$')
            # Lower branch ---------------------------------------
    #         # verts = [(k_vec[i],omega_n[0,i]/w0, omega_m[0,i]/w0) for i in range(k_step)] # Upper curve
    #         # verts = verts + [(k_vec[-i-1],omega_n[0,-i-1]/w0,z_offset) for i in range(k_step)] # Lower curve
    #         # ax.add_collection3d(Poly3DCollection([verts], alpha = alpha, facecolor='red', linewidths=1, zorder=0)) # Add a polygon instead of fill_between
    #         # ax.plot(k_vec, omega_n[0,:]/w0, omega_m[0,:]/w0, 'red', label="CCW", zorder=0)
    #         # ax.plot(k_vec, omega_n[0,:]/w0, 0.*omega_m[0,:], 'grey', alpha=0.7, zorder=0)
    #         # # ax.scatter(fratio[0], qs_diff[0], E_ccw[0], c='k', marker='o', s=markersize)
            
    #         # # Mid branch ---------------------------------------
    #         # verts = [(k_vec[i],omega_n[1,i]/w0, omega_m[1,i]/w0) for i in range(k_step)] # Upper curve
    #         # verts = verts + [(k_vec[-i-1],omega_n[1,-i-1]/w0,z_offset) for i in range(k_step)] # Lower curve
    #         # ax.add_collection3d(Poly3DCollection([verts], alpha = alpha, facecolor='green', linewidths=1, zorder=0)) # Add a polygon instead of fill_between
    #         # ax.plot(k_vec, omega_n[1,:]/w0, omega_m[1,:]/w0 ,'green', label="CCW", zorder=0)
    #         # ax.plot(k_vec, omega_n[1,:]/w0, 0.*omega_m[1,:], 'grey', alpha=0.7, zorder=0)
    #         # # ax.scatter(fratio[0], qs_diff[0], E_ccw[0], c='k', marker='o', s=markersize))
            
    #         # Upper branch 
    #         verts = [(omega_n[2,i]/w0, omega_m[2,i]/w0,k_vec[i]) for i in range(k_step)] # Upper curve
    #         verts = verts + [(omega_n[2,-i-1]/w0, omega_m[2,-i-1]/w0,z_offset) for i in range(k_step)] # Lower curve
    #         ax.add_collection3d(Poly3DCollection([verts], alpha = alpha, facecolor='blue', linewidths=1, zorder=0)) # Add a polygon instead of fill_between
    #         ax.plot(omega_n[2,:]/w0, omega_m[2,:]/w0, k_vec, 'blue', label="CCW", zorder=0)
    #         ax.plot(omega_n[2,:]/w0, 0.*omega_m[2,:], k_vec, 'grey', alpha=0.7, zorder=0)
    #         # ax.scatter(fratio[0], qs_diff[0], E_ccw[0], c='k', marker='o', s=markersize)
    #         # Origin
    #         # ax.scatter(0., 0., E_tmp[-1], c='k', marker='o', s=markersize, zorder=40)
            
            
    # #        ax.legend()
    # #        ax.set_xlim3d(0., 1.)
    # ##        ax.set_ylim3d(-0.5*np.pi, 0.5*np.pi)
    # #        ax.set_zlim3d(z_offset, max(E_tmp))
    #         eps = 1e-4
    #         # ax.axes.set_xlim3d(0.+0.02, 1-0.02) 
    #         # ax.axes.set_ylim3d(-0.7*np.pi-eps, 0.7*np.pi+eps)
    #         # ax.axes.set_zlim3d(bottom=0.+0.6+0.3, top=max(E_tmp)+2) # Qm0 = 90deg
    #         ax.set_xlabel('$k$')
    #         ax.set_ylabel('$\\omega_n/\\omega_0$')
    #         ax.set_zlabel('$\\omega_m/\\omega_0$')
    #         ax.xaxis.labelpad=30
    #         ax.yaxis.labelpad=30
    #         ax.zaxis.labelpad=30
    #         ax.set_xticks([-np.pi/self.height0,0.,np.pi/self.height0])
    #         ax.set_xticklabels(['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
    #         # ax.set_yticks([-0.5*np.pi, 0., 0.5*np.pi])
    #         # ax.set_yticklabels(['$-\\frac{\pi}{2}$', '$0$', '$\\frac{\pi}{2}$'])
    # #            ax.set_zticks([10, 20])
    # #            ax.set_zticklabels(['$10$', '$20$'])
            
    #         ax.xaxis._axinfo['tick']['inward_factor'] = 0
    #         ax.xaxis._axinfo['tick']['outward_factor'] = 0.4
    #         ax.yaxis._axinfo['tick']['inward_factor'] = 0
    #         ax.yaxis._axinfo['tick']['outward_factor'] = 0.4
    #         ax.zaxis._axinfo['tick']['inward_factor'] = 0
    #         ax.zaxis._axinfo['tick']['outward_factor'] = 0.4
    
    #         ax.grid(False)
    #         ax.xaxis.pane.set_edgecolor('black')
    #         ax.yaxis.pane.set_edgecolor('black')
    #         ax.zaxis.pane.set_edgecolor('black')
    #         ax.xaxis.pane.fill = False
    #         ax.yaxis.pane.fill = False
    # #        ax.zaxis.pane.fill = False
    #         tmp_planes = ax.zaxis._PLANES 
    #         ax.zaxis._PLANES = ( tmp_planes[2], tmp_planes[3], 
    #                              tmp_planes[0], tmp_planes[1], 
    #                              tmp_planes[4], tmp_planes[5])

            # Polarization ----------------------------------------------------
            # plt.figure('Polarization')
            # plt.plot(k_vec, Pu[0,:],'r--', label='Lower')
            # plt.plot(k_vec, Pu[1,:],'g--', label='mid')
            # plt.plot(k_vec, Pu[2,:],'b--', label='upper')
            # plt.legend(loc='best')
            # plt.xlabel('Wave number')
            # plt.ylabel('Polarization, $P_u$')
            
            # Impose polarization factor
#             v_Pu_min = 1e-8
#             plt.figure('Dispersion_Polarization')
# #            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
# #                plt.grid(True)
#             # Draw band gap
#             for i_branch in range(1):
#                 w1_max = max(omega[i_branch,:])
#                 w2_min = min(omega[i_branch+1,:])
#                 if w1_max < w2_min:
#                     plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
#             # Setting up a colormap
#             pu_map = plt.cm.get_cmap('jet')
#             cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
#             cm2.set_array([])
#             for i in range(k_step):
#                 plt.plot(k_vec[i], omega[0,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[0,i]) )
#                 plt.plot(k_vec[i], omega[1,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[1,i]) )
#                 plt.plot(k_vec[i], omega[2,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[2,i]) )
#             cbar2 = plt.colorbar(cm2)
#             cbar2.solids.set_edgecolor("face")
#             cbar2.set_label('Polarization index, $P_u$')
# #            cbar2.set_ticks([0.,v_Pu_max])
# #            cbar2.set_ticklabels(['0','{}'.format(v_Pu_max)])
#             cbar2.set_ticks([1e-8,1e-4,1])
#             cbar2.set_ticklabels(['$<10^{-8}$','$10^{-4}$', '$10^0$'])
#             plt.xlabel('Wave number')
# #                plt.ylabel('Frequency $\omega$')
#             plt.ylabel('Frequency $f$ [Hz]')
#             plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
# #                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
#             plt.xlim(-np.pi/self.height0, np.pi/self.height0)
            #plt.ylim(ymin=0)
            
            
            
#             v_Pu_min = 1e-8
#             plt.figure('Dispersion_Polarization(Homo)')
# #            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
# #                plt.grid(True)
#             # Draw band gap
#             for i_branch in range(1):
#                 w1_max = max(omega[i_branch,:])
#                 w2_min = min(omega[i_branch+1,:])
#                 if w1_max < w2_min:
#                     plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
#             # Setting up a colormap
#             pu_map = plt.cm.get_cmap('jet')
#             cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
#             cm2.set_array([])
#             for i in range(k_step):
#                 plt.plot(k_vec[i], omega[0,i]/(2.*np.pi), '.',  color=cm2.to_rgba(Pu[0,i]) )
#                 plt.plot(k_vec[i], omega[1,i]/(2.*np.pi), '.',  color=cm2.to_rgba(Pu[1,i]) )
#                 plt.plot(k_vec[i], omega[2,i]/(2.*np.pi), '.',  color=cm2.to_rgba(Pu[2,i]) )
#             # cbar2 = plt.colorbar(cm2)
#             # cbar2.solids.set_edgecolor("face")
#             # cbar2.set_label('Polarization index, $P_u$')
# #            cbar2.set_ticks([0.,v_Pu_max])
# #            cbar2.set_ticklabels(['0','{}'.format(v_Pu_max)])
#             # cbar2.set_ticks([1e-4,1e-2,1])
#             # cbar2.set_ticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
#             plt.xlabel('Wave number')
# #                plt.ylabel('Frequency $\omega$')
#             plt.ylabel('Frequency $f$ [Hz]')
#             plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{h_0}$','$0$', '$\\frac{\pi}{h_0}$'])
# #                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
#             plt.xlim(-np.pi/self.height0, np.pi/self.height0)
#             #plt.ylim(ymin=0)
            
            
            
            
            
            # Polarization ----------------------------------------------------
            
            # Impose polarization factor
#             fig1 = plt.figure('Polarization (Normalized)', figsize=(9,7))
#             fig1.subplots_adjust(top=0.75,bottom=0.2,left=0.2,right=0.75)
#             ax1 = fig1.gca()
# #            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
# #                plt.grid(True)
#             # Draw band gap
#             for i_branch in range(1):
#                 w1_max = max(omega[i_branch,:])
#                 w2_min = min(omega[i_branch+1,:])
#                 if w1_max < w2_min:
#                     ax1.fill_between(k_vec, w1_max/w0, w2_min/w0, where=None, facecolor='gray', alpha=0.6)
#             # Setting up a colormap
#             pu_map = plt.cm.get_cmap('jet')
#             cm3 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
#             cm3.set_array([])
#             for i in range(k_step):
#                 ax1.plot(k_vec[i], omega[0,i]/w0, '.', color=cm3.to_rgba(Pu[0,i]) )
#                 ax1.plot(k_vec[i], omega[1,i]/w0, '.',  color=cm3.to_rgba(Pu[1,i]) )
#                 ax1.plot(k_vec[i], omega[2,i]/w0,'.', color=cm2.to_rgba(Pu[2,i]) )
#             ax_divider = make_axes_locatable(ax1)
#             cax4 = ax_divider.append_axes("top", size="7%", pad="5%")
#             cbar4 = plt.colorbar(cm3,cax=cax4,orientation="horizontal")
#             cbar4.ax.minorticks_on()
#             cbar4.ax.set_xticklabels(['$<10^{-8}$','$10^{-4}$', '$10^0$'])
#             cbar4.ax.set_xticks(minor=True)
#             cax4.xaxis.set_ticks([1e-8,1e-4,1], minor=True)
#             cax4.xaxis.set_ticks_position("top")
#             cax4.xaxis.set_label_position("top")
#             cbar4.solids.set_edgecolor("face")
#             cbar4.set_label('Polarization index, $P_u$')
#             cbar4.ax.minorticks_on()
            
#             ax1.set_xlabel('Wave number')
#             ax1.set_ylabel('Frequency $\omega/\omega_0$')
#             ax1.set_xlim(-np.pi/self.height0, np.pi/self.height0)
#             ax1.set_ylim(ymin=0)
#             ax1.set_xticks([-np.pi/self.height0, 0., np.pi/self.height0])
# #            ax1.set_xticklabels(['$-\pi/a$','$0$', '$\pi/a$'])
#             ax1.set_xticklabels(['$-\\frac{\pi}{h_0}$','$0$', '$\\frac{\pi}{h_0}$'])
        return(k_vecn,omega, w0,Pu)
    


           
        
if __name__ == "__main__":
    #==============================================================================
    # Geometry
    #==============================================================================
    L_side = 12.*1e-3  #[m] Side length of the cube
    H_cube = 10.*1e-3  # [m] Height of the cube
    
    # Initial configuration
    q0_Lin  = np.radians(25)
    # Configuration of the chain
    # 'mono_C', 'mono_L', 'mono_R', 'dime_LR', 'dime_RL', 'dime_CL', 'dime_CR'
    chain_config = 'mono_C' 
    #==============================================================================
    # Material propeties
    #==============================================================================
    # Spring constant for linear spring --------------
    # Config1
    k_u = 670.8
    k_v = k_u
    k_s= 0.4*0.00044325837185769376  # for t_hinge=0.75*1e-3 [m]
    # Config2

    # Parameters for the Morse potential function -----
    A_const  = 0. #0.0005477163091763655
    alpha    = 4.966064409064443
    r1_morse = 0.74422634 # for t_hinge=0.75*1e-3 [m]
    # # Material property for the disk
    # weight = 1.9888*1e-3 #[kg] including joints, screws, and bearing
    # r_inrt = 46.608*1e-9 #[kgm^2]
    # Material property -----------------------
    # mass1 = 1.9888*1e-3 #[kg] (Measured)
    # r_inrt1 = 46.608*1e-9 #[kgm^2] (Measured)
    
    # Config 1
    dens1 = 1400. #[kg/m^3]
    mass1 = dens1*L_side*L_side*H_cube
    r_inrt1 = (1./12.)*mass1*L_side*L_side
    # Config 2
    dens2 = 2.*dens1 #[kg/m^3]
    mass2 = dens2*L_side*L_side*H_cube
    r_inrt2 = (1./12.)*mass2*L_side*L_side

    rotsq_eig = RotSq_Eigenvalue_unit(L_side, q0_Lin, chain_config, k_u, k_v, k_s, mass1, r_inrt1, \
                                          A_const, alpha, r1_morse)
    #steps 
    k_step = 50
    
    #mp relation
    nm_flag = 1
    # =========================================================================
    # Analyze frequency band structure
    # =========================================================================
    
    # Dispersion for homogeneous configuration ------------------------
    # Numerically calculated eigenvalues
    #k_vecn,omega, w0 ,Pu = rotsq_eig.Analyze_DispersionRelation_Monomer(k_step, nm_flag)
    # Explicit form
    #omega, v_eig1, v_eig2 = rotsq_eig.Explicit_Dispersion_Homo(mass1, r_inrt1, k_Lin1, k_Rot1)
    
    # Dispersion for dimer configuration ------------------------
    k_vec,omega,w0,Pu = rotsq_eig.Analyze_DispersionRelation_Dimer(mass1, r_inrt1, mass2, r_inrt2,
                                                                 k_Lin1, k_Rot1, k_Lin2, k_Rot2)
    
