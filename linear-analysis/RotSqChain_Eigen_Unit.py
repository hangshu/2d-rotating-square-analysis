import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.colorbar import Colorbar
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import cmath
import scipy.optimize as opt
from scipy import signal
from scipy import integrate
from scipy import linalg
from scipy.interpolate import interp1d
import os
import sys
import shutil   #High level file operation
from IMS_VTK4Animation import CubicModule_VTKanimation
sys.path.append('..')
from RotSquare_Energy import RotSquare_EnergyAnalysis
# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 34        # Original fornt size is 8, multipy by above number
plt.rcParams['text.usetex']     = True
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['lines.linewidth'] = 3.   
plt.rcParams['lines.markersize'] = 8. 
plt.rcParams['legend.fontsize'] = 28        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 34        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 34        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in' 
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300

#==============================================================================
# Rotating square chain
#==============================================================================
class RotSq_Eigenvalue_unit(RotSquare_EnergyAnalysis):
    def __init__(self, L_side, q0_Lin, chain_config, k_Lin1, k_Rot1,
                 A_const, alpha, r1_morse, q_cube0=np.radians(45)):
        # Dimension
        self.L_side  = L_side
        self.q0_Lin  = q0_Lin
        self.chain_config = chain_config
        self.geo_L   = L_side/(2.*np.cos(q_cube0))  #[m] Half of the diagonal length of a cube
        # Material properties
        # self.k_sp_Lin  = k_sp_Lin
        # self.k_sp_Rot  = k_sp_Rot
        self.A_const   = A_const
        self.alpha     = alpha
        self.r1_morse  = r1_morse
        self.r2_morse  = -r1_morse
        
        super().__init__(L_side, q0_Lin, chain_config,
                         k_Lin1, k_Rot1, A_const, alpha, r1_morse)
        self.q_minima   = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)
        self.angle0,self.height0 = self.InitialConfiguration()


    def Hamiltonian_mono(self, k, mass1, r_inrt1, k_Lin1, k_Rot1):
        Hmat = np.zeros((2,2),dtype=complex)
        exp_p = cmath.exp(1j*k*self.height0)
        exp_m = cmath.exp(-1j*k*self.height0)

        # Coefficient for Morse potential function
        wrk1 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(2.*self.alpha*(2.*self.angle0-2.*self.r1_morse)) \
                 - np.exp(   self.alpha*(2.*self.angle0-2.*self.r1_morse)) )
        wrk2 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(-2.*self.alpha*(2.*self.angle0-2.*self.r2_morse)) \
                 - np.exp(   -self.alpha*(2.*self.angle0-2.*self.r2_morse)) )
        T_slope = wrk1 + wrk2
        # alpha_uu
        Auu = k_Lin1*(exp_p+exp_m-2.)/mass1
        # alpha_up
        Aup = k_Lin1*self.geo_L*np.sin(self.angle0)*(exp_p-exp_m)/mass1
        # alpha_pu
        Apu = k_Lin1*self.geo_L*np.sin(self.angle0)*(-exp_p+exp_m)/r_inrt1
        # alpha_pp
        a1 = (k_Lin1*self.geo_L**2*np.cos(2.*self.angle0) \
              - k_Rot1 - T_slope)*(exp_p+exp_m)
        
#        a2 = -2.*self.k_sp_Lin*self.geo_L**2*(1.+np.sin(self.angle0)**2)
        a2 = -2.*k_Lin1*self.geo_L**2
        a3 = -4.*k_Rot1
        a4 = -4.*T_slope
        App = (a1+a2+a3+a4)/r_inrt1
        
        Hmat[0,0] = Auu
        Hmat[0,1] = Aup
        Hmat[1,0] = Apu
        Hmat[1,1] = App
        return(Hmat)
    
    # =========================================================================
    # Hang, this function needs to be modified (please use k_Lin2 and k_Rot2)
    # =========================================================================
    def Hamiltonian_dimer(self, k, mass1, r_inrt1, mass2, r_inrt2, 
                                   k_Lin1, k_Rot1, k_Lin2, k_Rot2):
        H0   = 2.*self.height0
        Hmat = np.zeros((4,4),dtype=complex)
        exp_p = cmath.exp(1j*k*H0)
        exp_m = cmath.exp(-1j*k*H0)
        
        # Coefficient for Morse potential function
        wrk1 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(2.*self.alpha*(2.*self.angle0-2.*self.r1_morse)) \
                 - np.exp(   self.alpha*(2.*self.angle0-2.*self.r1_morse)) )
        wrk2 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(-2.*self.alpha*(2.*self.angle0-2.*self.r2_morse)) \
                 - np.exp(   -self.alpha*(2.*self.angle0-2.*self.r2_morse)) )
        T_slope = wrk1 + wrk2
        
        Aup  = k_Lin1*self.geo_L*np.sin(self.angle0)
        App1 = -(2.*k_Lin1*self.geo_L**2 + 4.*k_Rot1 +4.*T_slope)
        App2 = k_Lin1*self.geo_L**2*np.cos(2.*self.angle0) - k_Rot1 - T_slope
        
        # Matrix form =====================================================
        Hmat[0,0] = -2.*k_Lin1/mass1
        Hmat[0,1] = 0.
        Hmat[0,2] = k_Lin1*(1.+exp_m)/mass1
        Hmat[0,3] = Aup*(1.-exp_m)/mass1

        Hmat[1,0] = 0.
        Hmat[1,1] = App1/r_inrt1
        Hmat[1,2] = -Aup*(1.-exp_m)/r_inrt1
        Hmat[1,3] = App2*(1.+exp_m)/r_inrt1

        Hmat[2,0] = k_Lin1*(1.+exp_p)/mass2
        Hmat[2,1] = -Aup*(1.-exp_p)/mass2
        Hmat[2,2] = -2.*k_Lin1/mass2
        Hmat[2,3] = 0.
#
        Hmat[3,0] = Aup*(1.-exp_p)/r_inrt2
        Hmat[3,1] = App2*(1.+exp_p)/r_inrt2
        Hmat[3,2] = 0.
        Hmat[3,3] = App1/r_inrt2

        return(Hmat)

    def Calculate_Pu(self, k_vec, mass1, r_inrt1, k_Lin1, k_Rot1):
        Hmat = self.Hamiltonian_mono(k_vec, mass1, r_inrt1, k_Lin1, k_Rot1)
        la,v = np.linalg.eig(-Hmat)
        Eig_sort = np.argsort(la)
        i_lower  = Eig_sort[0]
        i_upper  = Eig_sort[1]
        omega0 = cmath.sqrt(la[i_lower])
        omega1 = cmath.sqrt(la[i_upper])
        # Polarization
        Pu0 = np.linalg.norm(v[0,i_lower])/(np.linalg.norm(v[0,i_lower])+np.linalg.norm(v[1,i_lower]))
        Pu1 = np.linalg.norm(v[0,i_upper])/(np.linalg.norm(v[0,i_upper])+np.linalg.norm(v[1,i_upper]))
        # Eigen vectors
        # v_eig1[:,i] = v[:,i_lower]
        # v_eig2[:,i] = v[:,i_upper]
        return(omega0, omega1, Pu0, Pu1)
    
    def Analyze_DispersionRelation_Monomer(self, mass1, r_inrt1, k_Lin1, k_Rot1,
                                           k_step=401, v_Pu_min=1e-4, fig_flag=True):
        k_vec = np.linspace(-np.pi/self.height0, np.pi/self.height0, k_step)
        omega = np.zeros((2,k_step),dtype=complex)
        Pu    = np.zeros((2,k_step))
        v_eig1 = np.zeros((2,k_step),dtype=complex)
        v_eig2 = np.zeros((2,k_step),dtype=complex)
        # Explicit form
        # eig_value = np.zeros((2,k_step),dtype=complex)
        # eig_value[0,:],eig_value[1,:],v_eig1,v_eig2 = self.Eigenvalue_homogeneous(k_vec, mass1, r_inrt1, k_Lin1, k_Rot1)
        
        for i in range(k_step):
            Hmat = self.Hamiltonian_mono(k_vec[i], mass1, r_inrt1, k_Lin1, k_Rot1)
            la,v = np.linalg.eig(-Hmat)
            Eig_sort = np.argsort(la)
            i_lower  = Eig_sort[0]
            i_upper  = Eig_sort[1]
            omega[0,i] = cmath.sqrt(la[i_lower])
            omega[1,i] = cmath.sqrt(la[i_upper])
            # Polarization
            Pu[0,i] = np.linalg.norm(v[0,i_lower])/(np.linalg.norm(v[0,i_lower])+np.linalg.norm(v[1,i_lower]))
            Pu[1,i] = np.linalg.norm(v[0,i_upper])/(np.linalg.norm(v[0,i_upper])+np.linalg.norm(v[1,i_upper]))
            # Eigen vectors
            v_eig1[:,i] = v[:,i_lower]
            v_eig2[:,i] = v[:,i_upper]
#            print(np.linalg.norm(v[0,i_upper]))
#            print(np.linalg.norm(np.linalg.norm(v[0,i_lower]) + np.linalg.norm(v[1,i_lower])))
#            print('{0},{1}'.format(vu_1, vp_1))
        w0 = np.sqrt(k_Lin1/mass1)
    
    
        
        # Plot ----------------------------------------------------------------
        if fig_flag==True:
            plt.figure('Dispersion_Homo')
#            plt.title('$\\theta_0=%d^\circ$'%np.degrees(self.angle0[0]))
#                plt.grid(True)
            # Numerically calculated eigenvalues
            plt.plot(k_vec,omega[0,:]/(2.*np.pi),'r')
            plt.plot(k_vec,omega[1,:]/(2.*np.pi),'b')
            # Explicit form
            # plt.plot(k_vec,eig_value[0,:]/(2.*np.pi),'m--')
            # plt.plot(k_vec,eig_value[1,:]/(2.*np.pi),'c--')
            plt.xlabel('Wave number')
#                ylabel('Frequency $\omega$')
            plt.ylabel('Frequency $f$ [Hz]')
            plt.xticks([-np.pi/self.height0,0.,np.pi/self.height0],['$-\\frac{\pi}{h_0}$','$0$', '$\\frac{\pi}{h_0}$'])
            plt.xlim(-np.pi/self.height0,np.pi/self.height0)
            plt.ylim(ymin=0)
            
            # Berry phase calculation ---------------------------------------------
#            Berry, An1, An2  = self.BerryPhase(mass1, r_inrt1)
#            # Setting up a colormap
#            phase_map = plt.cm.get_cmap('jet')
#            cm = plt.cm.ScalarMappable(cmap=phase_map, norm=plt.Normalize(vmin=0, vmax=np.pi))
##                cm.set_array(Berry.real)
#            cm.set_array(np.linspace(0., np.pi,100))
#
#            plt.figure('Dispersion_BerryPhase')
##            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
##                plt.grid(True)
#            # Draw band gap
#            for i_branch in range(1):
#                w1_max = max(omega[i_branch,:])
#                w2_min = min(omega[i_branch+1,:])
#                if w1_max < w2_min:
#                    plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
#            plt.plot(k_vec, omega[0,:]/(2.*np.pi), color=phase_map(Berry[0].real/np.pi))
#            plt.plot(k_vec, omega[1,:]/(2.*np.pi), color=phase_map(Berry[1].real/np.pi))
#            cbar = plt.colorbar(cm)
#            cbar.solids.set_edgecolor("face")
#            cbar.set_label('Berry phase')
#            cbar.set_ticks([0.,np.pi])
#            cbar.set_ticklabels(['0','$\pi$'])
#            
#            plt.xlabel('Wave number')
##                plt.ylabel('Frequency $\omega$')
#            plt.ylabel('Frequency $f$ [Hz]')
#            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
##                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
#            plt.xlim(-np.pi/self.height0, np.pi/self.height0)
#            plt.ylim(ymin=0)
            
            # Polarization ----------------------------------------------------
            plt.figure('Polarization')
            plt.plot(k_vec, Pu[0,:],'r', label='Lower')
            plt.plot(k_vec, Pu[1,:],'b--', label='upper')
            plt.legend(loc='best')
            plt.xlabel('Wave number')
            plt.ylabel('Polarization, $P_u$')
            
            # Impose polarization factor
            v_Pu_min = 1e-4
            plt.figure('Dispersion_Polarization')
#            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
#                plt.grid(True)
            # Draw band gap
            for i_branch in range(1):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max < w2_min:
                    plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
            # Setting up a colormap
            pu_map = plt.cm.get_cmap('jet')
            cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm2.set_array([])
            for i in range(k_step):
                plt.plot(k_vec[i], omega[0,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[0,i]) )
                plt.plot(k_vec[i], omega[1,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[1,i]) )
            cbar2 = plt.colorbar(cm2)
            cbar2.solids.set_edgecolor("face")
            cbar2.set_label('Polarization index, $P_u$')
#            cbar2.set_ticks([0.,v_Pu_max])
#            cbar2.set_ticklabels(['0','{}'.format(v_Pu_max)])
            cbar2.set_ticks([1e-4,1e-2,1])
            cbar2.set_ticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            plt.xlabel('Wave number')
#                plt.ylabel('Frequency $\omega$')
            plt.ylabel('Frequency $f$ [Hz]')
            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
#                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
            plt.xlim(-np.pi/self.height0, np.pi/self.height0)
            plt.ylim(ymin=0)
            
            
            
            v_Pu_min = 1e-4
            plt.figure('Dispersion_Polarization(Homo)')
#            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
#                plt.grid(True)
            # Draw band gap
            for i_branch in range(1):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max < w2_min:
                    plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
            # Setting up a colormap
            pu_map = plt.cm.get_cmap('jet')
            cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm2.set_array([])
            for i in range(k_step):
                plt.plot(k_vec[i], omega[0,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[0,i]) )
                plt.plot(k_vec[i], omega[1,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[1,i]) )
            # cbar2 = plt.colorbar(cm2)
            # cbar2.solids.set_edgecolor("face")
            # cbar2.set_label('Polarization index, $P_u$')
#            cbar2.set_ticks([0.,v_Pu_max])
#            cbar2.set_ticklabels(['0','{}'.format(v_Pu_max)])
            # cbar2.set_ticks([1e-4,1e-2,1])
            # cbar2.set_ticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            plt.xlabel('Wave number')
#                plt.ylabel('Frequency $\omega$')
            plt.ylabel('Frequency $f$ [Hz]')
            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{h_0}$','$0$', '$\\frac{\pi}{h_0}$'])
#                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
            plt.xlim(-np.pi/self.height0, np.pi/self.height0)
            plt.ylim(ymin=0)
            
            
            
            
            
            # Polarization ----------------------------------------------------
            
            # Impose polarization factor
            fig1 = plt.figure('Polarization (Normalized)', figsize=(9,7))
            fig1.subplots_adjust(top=0.75,bottom=0.2,left=0.2,right=0.75)
            ax1 = fig1.gca()
#            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
#                plt.grid(True)
            # Draw band gap
            for i_branch in range(1):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max < w2_min:
                    ax1.fill_between(k_vec, w1_max/w0, w2_min/w0, where=None, facecolor='gray', alpha=0.6)
            # Setting up a colormap
            pu_map = plt.cm.get_cmap('jet')
            cm3 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm3.set_array([])
            for i in range(k_step):
                ax1.plot(k_vec[i], omega[0,i]/w0, '.', markersize=14, color=cm3.to_rgba(Pu[0,i]) )
                ax1.plot(k_vec[i], omega[1,i]/w0, '.', markersize=14, color=cm3.to_rgba(Pu[1,i]) )
            ax_divider = make_axes_locatable(ax1)
            cax4 = ax_divider.append_axes("top", size="7%", pad="5%")
            cbar4 = plt.colorbar(cm3,cax=cax4,orientation="horizontal")
            cbar4.ax.minorticks_on()
            cbar4.ax.set_xticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            cbar4.ax.set_xticks(minor=True)
            cax4.xaxis.set_ticks([1e-4,1e-2,1], minor=True)
            cax4.xaxis.set_ticks_position("top")
            cax4.xaxis.set_label_position("top")
            cbar4.solids.set_edgecolor("face")
            cbar4.set_label('Polarization index, $P_u$')
            cbar4.ax.minorticks_on()
            
#            cbar3 = plt.colorbar(cm3)
#            cbar3.solids.set_edgecolor("face")
#            cbar3.set_label('Polarization, $P_u$')
##            cbar3.set_ticks([0.,v_Pu_max])
##            cbar3.set_ticklabels(['{}'.format(v_Pu_min),'1'])
#            cbar3.set_ticks([1e-4,1e-2,1])
#            cbar3.set_ticklabels(['$<10^{-4}$','$10^{2}$', '$10^0$'])
#            plt.xlabel('Wave number')
##                plt.ylabel('Frequency $\omega$')
#            plt.ylabel('Frequency $\omega/\omega_0$')
##            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{a}$','$0$', '$\\frac{\pi}{a}$'])
#            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\pi/a$','$0$', '$\pi/a$'])
##                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
#            plt.xlim(-np.pi/self.height0, np.pi/self.height0)
#            plt.ylim(ymin=0)
            
            ax1.set_xlabel('Wave number')
            ax1.set_ylabel('Frequency $\omega/\omega_0$')
            ax1.set_xlim(-np.pi/self.height0, np.pi/self.height0)
            ax1.set_ylim(ymin=0)
            ax1.set_xticks([-np.pi/self.height0, 0., np.pi/self.height0])
#            ax1.set_xticklabels(['$-\pi/a$','$0$', '$\pi/a$'])
            ax1.set_xticklabels(['$-\\frac{\pi}{h_0}$','$0$', '$\\frac{\pi}{h_0}$'])
        return(k_vec,omega,w0,Pu,v_eig1,v_eig2)
    

    def BerryPhase(self, mass1, r_inrt1):
        kx = np.linspace(-np.pi/self.height0, np.pi/self.height0,201)
        Eigvec1 = np.zeros((2,len(kx)+1),dtype=complex)
        Eigvec2 = np.zeros((2,len(kx)+1),dtype=complex)
        
        # Calculate eivenvectors ===========================================
        for i in range(len(kx)+1):
            if i==len(kx):
                H = self.Hamiltonian_mono(kx[-1], mass1, r_inrt1)
            else:
                H = self.Hamiltonian_mono(kx[i], mass1, r_inrt1)
            la,v = np.linalg.eig(H)
            Eig_sort = np.argsort(la)
            # Eigenvectors
            Eigvec1[:,i] = v[:,Eig_sort[0]]
            Eigvec1[:,i] = Eigvec1[:,i]/Eigvec1[0,i]/np.linalg.norm(Eigvec1[:,i]/Eigvec1[0,i])
            Eigvec2[:,i] = v[:,Eig_sort[1]]
            Eigvec2[:,i] = Eigvec2[:,i]/Eigvec2[0,i]/np.linalg.norm(Eigvec2[:,i]/Eigvec2[0,i])
        # Calculate Berry connection =======================================
        An1 = np.zeros(len(kx),dtype=complex)
        An2 = np.zeros(len(kx),dtype=complex)
        for i in range(len(kx)):
            if i==len(kx)-1:
                ket1 = (Eigvec1[:,0] - Eigvec1[:,i])/(kx[1]-kx[0])
                ket2 = (Eigvec2[:,0] - Eigvec2[:,i])/(kx[1]-kx[0])
            else:
                ket1 = (Eigvec1[:,i+1] - Eigvec1[:,i])/(kx[1]-kx[0])
                ket2 = (Eigvec2[:,i+1] - Eigvec2[:,i])/(kx[1]-kx[0])
            An1[i] = 1j*np.dot(Eigvec1[:,i].conjugate(),ket1)
            An2[i] = 1j*np.dot(Eigvec2[:,i].conjugate(),ket2)
        # Calculate Berry phase ============================================
        Berry    = np.zeros(2,dtype=complex)
        Berry[0] = integrate.simps(An1,kx)
        Berry[1] = integrate.simps(An2,kx)
        # 0 or pi
        Berry = np.arccos(np.cos(Berry))
        return(Berry,An1,An2)
    
    def Analyze_DispersionRelation_Dimer(self, mass1, r_inrt1, mass2, r_inrt2,
                                               k_Lin1, k_Rot1, k_Lin2, k_Rot2,
                                         n_cal=201, v_Pu_min=1e-4, fig_flag=True):
        H0    = 2.*self.height0 # Lattice parameter for a supercell composed of two units
        k_vec = np.linspace(-2.*np.pi/H0, 2.*np.pi/H0, n_cal)
        omega = np.zeros((4,len(k_vec)),dtype=complex)
        Pu    = np.zeros((4,len(k_vec)))
        P_TTL = np.zeros(len(k_vec))
        for i in range(len(k_vec)):
            Hmat = self.Hamiltonian_dimer(k_vec[i], mass1, r_inrt1, mass2, r_inrt2, k_Lin1, k_Rot1, k_Lin2, k_Rot2)
            la,v = np.linalg.eig(-Hmat)
            Eig_sort = np.argsort(la)
            i_0  = Eig_sort[0]
            i_1  = Eig_sort[1]
            i_2  = Eig_sort[2]
            i_3  = Eig_sort[3]
            omega[0,i] = cmath.sqrt(la[i_0])
            omega[1,i] = cmath.sqrt(la[i_1])
            omega[2,i] = cmath.sqrt(la[i_2])
            omega[3,i] = cmath.sqrt(la[i_3])
            # Polarization
            vu_1 = v[0,i_0]
            vp_1 = v[1,i_0]
            vu_2 = v[0,i_1]
            vp_2 = v[1,i_1]
            # P_TTL[i] = np.linalg.norm(v[0,i_lower])+np.linalg.norm(v[1,i_lower])
            den0    = np.linalg.norm(v[0,i_0]) + np.linalg.norm(v[1,i_0]) + np.linalg.norm(v[2,i_0]) + np.linalg.norm(v[3,i_0])
            Pu[0,i] = (np.linalg.norm(v[0,i_0]) + np.linalg.norm(v[2,i_0])) / den0
            # Second branch
            den1    = np.linalg.norm(v[0,i_1]) + np.linalg.norm(v[1,i_1]) + np.linalg.norm(v[2,i_1]) + np.linalg.norm(v[3,i_1])
            Pu[1,i] = (np.linalg.norm(v[0,i_1]) + np.linalg.norm(v[2,i_1])) / den1
            # Third branch
            den2    = np.linalg.norm(v[0,i_2]) + np.linalg.norm(v[1,i_2]) + np.linalg.norm(v[2,i_2]) + np.linalg.norm(v[3,i_2])
            Pu[2,i] = (np.linalg.norm(v[0,i_2]) + np.linalg.norm(v[2,i_2])) / den2
            # Fourth branch
            den3    = np.linalg.norm(v[0,i_3]) + np.linalg.norm(v[1,i_3]) + np.linalg.norm(v[2,i_3]) + np.linalg.norm(v[3,i_3])
            Pu[3,i] = (np.linalg.norm(v[0,i_3]) + np.linalg.norm(v[2,i_3])) / den3
            
            
            # Pu[0,i] = np.linalg.norm(v[0,i_0])/(np.linalg.norm(v[0,i_0])+np.linalg.norm(v[1,i_0]))
            # Pu[1,i] = np.linalg.norm(v[0,i_1])/(np.linalg.norm(v[0,i_1])+np.linalg.norm(v[1,i_1]))
#            print(np.linalg.norm(v[0,i_upper]))
#            print(np.linalg.norm(np.linalg.norm(v[0,i_lower]) + np.linalg.norm(v[1,i_lower])))
#            print('{0},{1}'.format(vu_1, vp_1))
        w0 = np.sqrt(k_Lin1/mass1)
    
        # Plot ----------------------------------------------------------------
        if fig_flag==True:
            plt.figure('Dispersion_Dimer')
#            plt.title('$\\theta_0=%d^\circ$'%np.degrees(self.angle0[0]))
#                plt.grid(True)
            # Draw band gap
            gp_offset = 6
            for i_branch in range(3):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max+gp_offset < w2_min:
                    plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
            plt.plot(k_vec, omega[0,:]/(2.*np.pi),'r')
            plt.plot(k_vec, omega[1,:]/(2.*np.pi),'b')
            plt.plot(k_vec, omega[2,:]/(2.*np.pi),'r')
            plt.plot(k_vec, omega[3,:]/(2.*np.pi),'b')
            plt.xlabel('Wave number')
#                ylabel('Frequency $\omega$')
            plt.ylabel('Frequency $f$ [Hz]')
            plt.xticks([-np.pi/H0, 0., np.pi/H0],['$-\\frac{\pi}{2h_0}$','$0$', '$\\frac{\pi}{2h_0}$'])
            plt.xlim(-np.pi/H0, np.pi/H0)
            plt.ylim(ymin=0)
            
            # Polarization ----------------------------------------------------
            # Impose polarization factor
            v_Pu_min = 1e-4
            plt.figure('Dispersion_Polarization')
#            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
#                plt.grid(True)
            # Draw band gap
            for i_branch in range(3):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max+gp_offset < w2_min:
                    plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
            # Setting up a colormap
            pu_map = plt.cm.get_cmap('jet')
            cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm2.set_array([])
            for i in range(len(k_vec)):
                plt.plot(k_vec[i], omega[0,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[0,i]) )
                plt.plot(k_vec[i], omega[1,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[1,i]) )
                plt.plot(k_vec[i], omega[2,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[2,i]) )
                plt.plot(k_vec[i], omega[3,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[3,i]) )
            cbar2 = plt.colorbar(cm2)
            cbar2.solids.set_edgecolor("face")
            cbar2.set_label('Polarization index, $P_u$')
#            cbar2.set_ticks([0.,v_Pu_max])
#            cbar2.set_ticklabels(['0','{}'.format(v_Pu_max)])
            cbar2.set_ticks([1e-4,1e-2,1])
            cbar2.set_ticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            plt.xlabel('Wave number')
#                plt.ylabel('Frequency $\omega$')
            plt.ylabel('Frequency $f$ [Hz]')
            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
#                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
            plt.xlim(-np.pi/self.height0, np.pi/self.height0)
            plt.ylim(ymin=0)
            
            
            # Polarization ----------------------------------------------------
            # Impose polarization factor
            v_Pu_min = 1e-4
            plt.figure('Dispersion_Polarization2')
#            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
#                plt.grid(True)
            # Draw band gap
            for i_branch in range(3):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max+gp_offset < w2_min:
                    plt.fill_between(k_vec, w1_max/(2.*np.pi), w2_min/(2.*np.pi), where=None, facecolor='gray', alpha=0.6)
            # Setting up a colormap
            pu_map = plt.cm.get_cmap('jet')
            cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm2.set_array([])
            for i in range(len(k_vec)):
                plt.plot(k_vec[i], omega[0,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[0,i]) )
                plt.plot(k_vec[i], omega[1,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[1,i]) )
                plt.plot(k_vec[i], omega[2,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[2,i]) )
                plt.plot(k_vec[i], omega[3,i]/(2.*np.pi), '.', markersize=14, color=cm2.to_rgba(Pu[3,i]) )
#             cbar2 = plt.colorbar(cm2)
#             cbar2.solids.set_edgecolor("face")
#             cbar2.set_label('Polarization index, $P_u$')
# #            cbar2.set_ticks([0.,v_Pu_max])
# #            cbar2.set_ticklabels(['0','{}'.format(v_Pu_max)])
#             cbar2.set_ticks([1e-4,1e-2,1])
#             cbar2.set_ticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            plt.xlabel('Wave number')
#                plt.ylabel('Frequency $\omega$')
            plt.ylabel('Frequency $f$ [Hz]')
            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{H_0}$','$0$', '$\\frac{\pi}{H_0}$'])
#                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
            plt.xlim(-np.pi/self.height0, np.pi/self.height0)
            plt.ylim(ymin=0)
            
            # Polarization (Normalized) ----------------------------
            fig1 = plt.figure('Polarization (Normalized)', figsize=(9,7))
            fig1.subplots_adjust(top=0.75,bottom=0.2,left=0.2,right=0.75)
            ax1 = fig1.gca()
#            plt.title('$\\theta_0^{(1)}=%d^\circ$ and $\\theta_0^{(2)}=%d^\circ$'%(np.degrees(self.angle0[0]),np.degrees(self.angle0[1])), fontsize=14)
#                plt.grid(True)
            # Draw band gap
            for i_branch in range(1):
                w1_max = max(omega[i_branch,:])
                w2_min = min(omega[i_branch+1,:])
                if w1_max < w2_min:
                    ax1.fill_between(k_vec, w1_max/w0, w2_min/w0, where=None, facecolor='gray', alpha=0.6)
            # Setting up a colormap
            pu_map = plt.cm.get_cmap('jet')
            cm3 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
            cm3.set_array([])
            for i in range(len(k_vec)):
                plt.plot(k_vec[i], omega[0,i]/w0, '.', markersize=14, color=cm2.to_rgba(Pu[0,i]) )
                plt.plot(k_vec[i], omega[1,i]/w0, '.', markersize=14, color=cm2.to_rgba(Pu[1,i]) )
                plt.plot(k_vec[i], omega[2,i]/w0, '.', markersize=14, color=cm2.to_rgba(Pu[2,i]) )
                plt.plot(k_vec[i], omega[3,i]/w0, '.', markersize=14, color=cm2.to_rgba(Pu[3,i]) )
            # for i in range(len(k_vec)):
            #     ax1.plot(k_vec[i], omega[0,i]/w0, '.', markersize=14, color=cm3.to_rgba(Pu[0,i]) )
            #     ax1.plot(k_vec[i], omega[1,i]/w0, '.', markersize=14, color=cm3.to_rgba(Pu[1,i]) )
            ax_divider = make_axes_locatable(ax1)
            cax4 = ax_divider.append_axes("top", size="7%", pad="5%")
            cbar4 = plt.colorbar(cm3,cax=cax4,orientation="horizontal")
            cbar4.ax.minorticks_on()
            cbar4.ax.set_xticklabels(['$<10^{-4}$','$10^{-2}$', '$10^0$'])
            cbar4.ax.set_xticks(minor=True)
            cax4.xaxis.set_ticks([1e-4,1e-2,1], minor=True)
            cax4.xaxis.set_ticks_position("top")
            cax4.xaxis.set_label_position("top")
            cbar4.solids.set_edgecolor("face")
            cbar4.set_label('Polarization index, $P_u$')
            cbar4.ax.minorticks_on()
            
#            cbar3 = plt.colorbar(cm3)
#            cbar3.solids.set_edgecolor("face")
#            cbar3.set_label('Polarization, $P_u$')
##            cbar3.set_ticks([0.,v_Pu_max])
##            cbar3.set_ticklabels(['{}'.format(v_Pu_min),'1'])
#            cbar3.set_ticks([1e-4,1e-2,1])
#            cbar3.set_ticklabels(['$<10^{-4}$','$10^{2}$', '$10^0$'])
#            plt.xlabel('Wave number')
##                plt.ylabel('Frequency $\omega$')
#            plt.ylabel('Frequency $\omega/\omega_0$')
##            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\\frac{\pi}{a}$','$0$', '$\\frac{\pi}{a}$'])
#            plt.xticks([-np.pi/self.height0, 0., np.pi/self.height0],['$-\pi/a$','$0$', '$\pi/a$'])
##                plt.xlim(-np.pi/self.h0,np.pi/self.h0)
#            plt.xlim(-np.pi/self.height0, np.pi/self.height0)
#            plt.ylim(ymin=0)
            
            ax1.set_xlabel('Wave number')
            ax1.set_ylabel('Frequency $\omega/\omega_0$')
            ax1.set_xlim(-np.pi/self.height0, np.pi/self.height0)
            ax1.set_ylim(ymin=0)
            ax1.set_xticks([-np.pi/self.height0, 0., np.pi/self.height0])
#            ax1.set_xticklabels(['$-\pi/a$','$0$', '$\pi/a$'])
            ax1.set_xticklabels(['$-\\frac{\pi}{h_0}$','$0$', '$\\frac{\pi}{h_0}$'])
        return(k_vec,omega,w0,Pu)
    
    def DispersionRelationChange_Monomer(self, q0_Lin_range, mass1, r_inrt1, k_Lin1, k_Rot1,
                                         v_Pu_min=1e-4, k_step=71, 
                                         fname_paraview='Paraview_code/Paraview_Dispersion_Monomer_original.py'):
        n_step = len(q0_Lin_range)
        w1_data = np.zeros((n_step,k_step))
        w2_data = np.zeros((n_step,k_step))
        Pu1_data = np.zeros((n_step,k_step))
        Pu2_data = np.zeros((n_step,k_step))
        w0       = np.sqrt(k_Lin1/mass1)
        # Save data for paraview---
        # Delete previous data
        dir_save = 'Dispersion_Monomer_VTK'
        if os.path.exists(dir_save):
            shutil.rmtree(dir_save)
        os.makedirs(dir_save)
        fname_branch = "{}/DispersionRelationship_Monomer".format(dir_save)
        f1 = open('{}_Branch1.csv'.format(fname_branch),'w')
        f2 = open('{}_Branch2.csv'.format(fname_branch),'w')
        # f3 = open('{}_Branch3.csv'.format(fname_branch),'w')
        # f4 = open('{}_Branch4.csv'.format(fname_branch),'w')
        f1.write("Wave number (k), q0 angle (deg), Normalized frequency (omega), Polarization index\n")
        f2.write("Wave number (k), q0 angle (deg), Normalized frequency (omega), Polarization index \n")
        # f3.write("Wave number (k), Alpha, Frequency, Zak phase \n")
        # f4.write("Wave number (k), Alpha, Frequency, Zak phase \n")
        
        for i in range(n_step):
            # Set the initial angle
            self.q0_Lin   = q0_Lin_range[i]
            self.q_minima = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)
            self.angle0,self.height0 = self.InitialConfiguration()
            # Eigenvalue analysis
            k_vec,omega,w0,Pu = self.Analyze_DispersionRelation_Monomer(mass1, r_inrt1, k_Lin1, k_Rot1, fig_flag=False)
            w1_data[i,:]  = omega[0,:]
            w2_data[i,:]  = omega[1,:]
            Pu1_data[i,:] = Pu[0,:]
            Pu2_data[i,:] = Pu[1,:]
            # Create VTK file for the surface plot -------
            for ik in range(len(k_vec)):
                f1.write("{}, {}, {}, {}\n".format(k_vec[ik]*self.height0, np.degrees(q0_Lin_range[i]), omega[0,ik].real/w0, Pu[0,ik].real) )
                f2.write("{}, {}, {}, {}\n".format(k_vec[ik]*self.height0, np.degrees(q0_Lin_range[i]), omega[1,ik].real/w0, Pu[1,ik].real) )
        # Convert data to VTK
        f1.close()
        f2.close()
        self.csv2vtk(filename = fname_branch+'_Branch1.csv')
        self.csv2vtk(filename = fname_branch+'_Branch2.csv')
        # Create Python code for Paraview
        self.CreateParaviewScript4Dispersion(fname_paraview, fname_branch)
        
        # Setting up a colormap
        pu_map = plt.cm.get_cmap('jet')
        cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
        cm2.set_array([])
        plt.figure('Dispersion change (Monomer)')
        for i in range(n_step):
            for ik in range(len(k_vec)):
                plt.plot(np.degrees(q0_Lin_range[i]), w1_data[i,ik]/(2.*np.pi), 'o', color=cm2.to_rgba(Pu1_data[i,ik]) )
                plt.plot(np.degrees(q0_Lin_range[i]), w2_data[i,ik]/(2.*np.pi), 'o', color=cm2.to_rgba(Pu2_data[i,ik]) )
        plt.xlabel('Initial angle $\\theta_0$ ($^\circ$)')
        plt.ylabel('Frequency $f$ (Hz)')
        # plt.ylabel('Frequency $\omega/\omega_0$')
        return()

    def DispersionRelationChange_Dimer(self, q0_Lin_range, mass1, r_inrt1, mass2, r_inrt2, 
                                       k_Lin1, k_Rot1, k_Lin2, k_Rot2,
                                       v_Pu_min=1e-4, n_cal=71, 
                                       fname_paraview = 'Paraview_code/Paraview_Dispersion_Dimer_original.py'):
        q0_Lin_range = np.linspace(-np.radians(40), np.radians(40), 81)
        n_step = len(q0_Lin_range)
        w1_data = np.zeros((n_step,n_cal))
        w2_data = np.zeros((n_step,n_cal))
        w3_data = np.zeros((n_step,n_cal))
        w4_data = np.zeros((n_step,n_cal))
        Pu1_data = np.zeros((n_step,n_cal))
        Pu2_data = np.zeros((n_step,n_cal))
        Pu3_data = np.zeros((n_step,n_cal))
        Pu4_data = np.zeros((n_step,n_cal))
        
        # Save data for paraview---
        # Delete previous data
        dir_save = 'Dispersion_Dimer_VTK'
        if os.path.exists(dir_save):
            shutil.rmtree(dir_save)
        os.makedirs(dir_save)
        fname_branch = "{}/DispersionRelationship_Monomer".format(dir_save)
        f1 = open('{}_Branch1.csv'.format(fname_branch),'w')
        f2 = open('{}_Branch2.csv'.format(fname_branch),'w')
        f3 = open('{}_Branch3.csv'.format(fname_branch),'w')
        f4 = open('{}_Branch4.csv'.format(fname_branch),'w')
        f1.write("Wave number (k), q0 angle (deg), Normalized frequency (omega), Polarization index\n")
        f2.write("Wave number (k), q0 angle (deg), Normalized frequency (omega), Polarization index \n")
        f3.write("Wave number (k), q0 angle (deg), Normalized frequency (omega), Polarization index\n")
        f4.write("Wave number (k), q0 angle (deg), Normalized frequency (omega), Polarization index \n")
        
        for i in range(n_step):
            # Set the initial angle
            self.q0_Lin   = q0_Lin_range[i]
            self.q_minima = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)
            self.angle0,self.height0 = self.InitialConfiguration()
            # Eigenvalue analysis
            # k_vec,omega,w0,Pu = self.Analyze_DispersionRelation_Monomer(mass1, r_inrt1, fig_flag=False)
            k_vec,omega,w0,Pu = self.Analyze_DispersionRelation_Dimer(mass1, r_inrt1, mass2, r_inrt2, 
                                                                      k_Lin1, k_Rot1, k_Lin2, k_Rot2, n_cal=n_cal, fig_flag=False)
            w1_data[i,:]  = omega[0,:]
            w2_data[i,:]  = omega[1,:]
            w3_data[i,:]  = omega[2,:]
            w4_data[i,:]  = omega[3,:]
            Pu1_data[i,:] = Pu[0,:]
            Pu2_data[i,:] = Pu[1,:]
            Pu3_data[i,:] = Pu[2,:]
            Pu4_data[i,:] = Pu[3,:]
            # Create VTK file for the surface plot -------
            for ik in range(len(k_vec)):
                f1.write("{}, {}, {}, {}\n".format(k_vec[ik]*self.height0, np.degrees(q0_Lin_range[i]), omega[0,ik].real/w0, Pu[0,ik].real) )
                f2.write("{}, {}, {}, {}\n".format(k_vec[ik]*self.height0, np.degrees(q0_Lin_range[i]), omega[1,ik].real/w0, Pu[1,ik].real) )
                f3.write("{}, {}, {}, {}\n".format(k_vec[ik]*self.height0, np.degrees(q0_Lin_range[i]), omega[2,ik].real/w0, Pu[2,ik].real) )
                f4.write("{}, {}, {}, {}\n".format(k_vec[ik]*self.height0, np.degrees(q0_Lin_range[i]), omega[3,ik].real/w0, Pu[3,ik].real) )
        # Convert data to VTK
        f1.close()
        f2.close()
        f3.close()
        f4.close()
        self.csv2vtk(filename = fname_branch+'_Branch1.csv')
        self.csv2vtk(filename = fname_branch+'_Branch2.csv')
        self.csv2vtk(filename = fname_branch+'_Branch3.csv')
        self.csv2vtk(filename = fname_branch+'_Branch4.csv')
        # Create Python code for Paraview
        self.CreateParaviewScript4Dispersion(fname_paraview, fname_branch)
        
        # Setting up a colormap
        pu_map = plt.cm.get_cmap('jet')
        cm2 = plt.cm.ScalarMappable(cmap=pu_map, norm=colors.LogNorm(vmin=v_Pu_min, vmax=1))
        cm2.set_array([])
        plt.figure('Dispersion change (Dimer)')
        for i in range(n_step):
            for ik in range(len(k_vec)):
                plt.plot(np.degrees(q0_Lin_range[i]), w1_data[i,ik]/(2.*np.pi), 'o', color=cm2.to_rgba(Pu1_data[i,ik]) )
                plt.plot(np.degrees(q0_Lin_range[i]), w2_data[i,ik]/(2.*np.pi), 'o', color=cm2.to_rgba(Pu2_data[i,ik]) )
                plt.plot(np.degrees(q0_Lin_range[i]), w3_data[i,ik]/(2.*np.pi), 'o', color=cm2.to_rgba(Pu3_data[i,ik]) )
                plt.plot(np.degrees(q0_Lin_range[i]), w4_data[i,ik]/(2.*np.pi), 'o', color=cm2.to_rgba(Pu4_data[i,ik]) )
        plt.xlabel('Initial angle $\\theta_0$ ($^\circ$)')
        plt.ylabel('Frequency $f$ (Hz)')
        # plt.ylabel('Frequency $\omega/\omega_0$')
        return()
    
    def csv2vtk(self, filename):
        with open(filename,'r') as f:
            f.readline()
            
            x = []
            y = []
            z = []
            s_val = []
            for line0 in f:
                line = line0[0:len(line0)-1]
                item = line.split(",")
                x.append(item[0])
                y.append(item[1])
                z.append(item[2])
                s_val.append(item[3])
            f.close()
            # output
            basename = os.path.splitext(filename)[0]
            f = open(basename + ".vtk", "w")
            f.write("# vtk DataFile Version 2.0\n")
            f.write(basename + "\n")
            f.write("ASCII\n")
            f.write("DATASET UNSTRUCTURED_GRID\n")
            f.write("POINTS %d float\n" % len(x))
            num = len(x)
            for i in range(0, num):
                f.write(x[i] + " " + y[i] + " 0\n")
            f.write("CELLS %d %d\n" % (num, 2*num))
            for i in range(0, num):
                f.write("1 %d\n" % i)
            f.write("CELL_TYPES %d\n" % num)
            for i in range(0, num):
                f.write("1\n")
            f.write("POINT_DATA %d\n" % num)
            f.write("SCALARS point_scalars float\n")
            f.write("LOOKUP_TABLE default\n")
            for i in range(0, num):
                f.write(z[i] + "\n")
            f.write("SCALARS scalar_val float\n")
            f.write("LOOKUP_TABLE default\n")
            for i in range(0, num):
                f.write(s_val[i] + "\n")
        return()
    
    def CreateParaviewScript4Dispersion(self, fname_paraview, fname_branch):
        print(fname_paraview)
        txt_data = []
        with open(fname_paraview,'r') as f:
            f_list = f.readlines()
            i_Line = 0
            for line in f_list: # Read number of step
                
                # Detect vertex
                i_variable = line.find('#PY_VARIABLE')
                if i_variable >= 0:
                    if line.find('fname_branch1') >=0:
                        line_wrk = "fname_branch1 = '{}/{}_Branch1.vtk'\n".format(os.getcwd(), fname_branch)
                    elif line.find('fname_branch2') >=0:
                        line_wrk = "fname_branch2 = '{}/{}_Branch2.vtk'\n".format(os.getcwd(), fname_branch)
                    elif line.find('fname_branch3') >=0:
                        line_wrk = "fname_branch3 = '{}/{}_Branch3.vtk'\n".format(os.getcwd(), fname_branch)
                    elif line.find('fname_branch4') >=0:
                        line_wrk = "fname_branch4 = '{}/{}_Branch4.vtk'\n".format(os.getcwd(), fname_branch)
                    else:
                        print('Unkown variable !!!!!!!!!!!!!!!')
                else:
                    line_wrk = line
                # Store the line
                txt_data.append(line_wrk)
                # Renew
                i_Line = i_Line + 1
        
        # Write data into txt file
        fscript_save = 'Paraview_3D_Dispersion-rev.py'
        with open(fscript_save,'w') as f:
            for i in range(len(txt_data)):
                f.write(txt_data[i])
        return()
    
    def Determinant_mono(self, k_ratio, k, mass1, r_inrt1):
        exp_p = cmath.exp( 1j*k*self.height0)
        exp_m = cmath.exp(-1j*k*self.height0)

        # Coefficient for Morse potential function
        wrk1 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(2.*self.alpha*(2.*self.angle0-2.*self.r1_morse)) \
                 - np.exp(   self.alpha*(2.*self.angle0-2.*self.r1_morse)) )
        wrk2 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(-2.*self.alpha*(2.*self.angle0-2.*self.r2_morse)) \
                 - np.exp(   -self.alpha*(2.*self.angle0-2.*self.r2_morse)) )
        T_slope = wrk1 + wrk2
        # alpha_uu
        Auu = (exp_p+exp_m-2.)/mass1
        # alpha_up
        Aup = self.geo_L*np.sin(self.angle0)*(exp_p-exp_m)/mass1
        # alpha_pu
        Apu = self.geo_L*np.sin(self.angle0)*(-exp_p+exp_m)/r_inrt1
        # alpha_pp
        a1 = (self.geo_L**2*np.cos(2.*self.angle0) \
              - k_ratio - T_slope)*(exp_p+exp_m)        
        a2 = -2.*self.geo_L**2
        # a3 = -4.*k_Rot1/k_Lin1
        a3 = -4.*k_ratio
        a4 = -4.*T_slope
        App = (a1+a2+a3+a4)/r_inrt1
        
        det = Auu*App - Aup*Apu
        return(det)
    
    def ZeroDeterminant(self, mass1, r_inrt1, k_Lin1, k_Rot1):
        k = 0.01*np.pi/self.height0
        k_step = 101
        k_vec = np.linspace(-np.pi/self.height0, np.pi/self.height0, k_step)
        det   = np.zeros(k_step)
        k_ratio = 0.1
        for ik in range(k_step):
            det[ik] = self.Determinant_mono(k_ratio, k_vec[ik], mass1, r_inrt1)
        k_Lin_sol  = 0.5*opt.newton(self.Determinant_mono, k_Lin1,
                                          args=(k, mass1, r_inrt1),
                                          # tol=1e-8, maxiter=int(1e6)
                                          )
        print('Initial guess = {}'.format(k_Lin1))
        print('Solution      = {}'.format(k_Lin_sol))
        res = self.Determinant_mono(k_Lin_sol, k, mass1, r_inrt1)
        print('res = {}'.format(res))
        
        plt.figure('Check')
        plt.plot(k_vec, det, 'b-')
        return()
    
    def Eigenvalue_homogeneous(self, k, mass1, r_inrt1, k_Lin1, k_Rot1):
        exp_p = np.exp(1j*k*self.height0, dtype=complex)
        exp_m = np.exp(-1j*k*self.height0, dtype=complex)

        # Coefficient for Morse potential function
        wrk1 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(2.*self.alpha*(2.*self.angle0-2.*self.r1_morse)) \
                 - np.exp(   self.alpha*(2.*self.angle0-2.*self.r1_morse)) )
        wrk2 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(-2.*self.alpha*(2.*self.angle0-2.*self.r2_morse)) \
                 - np.exp(   -self.alpha*(2.*self.angle0-2.*self.r2_morse)) )
        T_slope = wrk1 + wrk2
        # alpha_uu
        Auu = -k_Lin1*(exp_p+exp_m-2.)/mass1
        # alpha_up
        Aup = -k_Lin1*self.geo_L*np.sin(self.angle0)*(exp_p-exp_m)/mass1
        # alpha_pu
        Apu = -k_Lin1*self.geo_L*np.sin(self.angle0)*(-exp_p+exp_m)/r_inrt1
        # alpha_pp
        a1 = (k_Lin1*self.geo_L**2*np.cos(2.*self.angle0) \
              - k_Rot1 - T_slope)*(exp_p+exp_m)
        
#        a2 = -2.*self.k_sp_Lin*self.geo_L**2*(1.+np.sin(self.angle0)**2)
        a2 = -2.*k_Lin1*self.geo_L**2
        a3 = -4.*k_Rot1
        a4 = -4.*T_slope
        App = -(a1+a2+a3+a4)/r_inrt1
        
        temp1 = Auu + App
        temp2 = (Auu + App)**2
        temp3 = 4.*(Auu*App - Aup*Apu)
        
        # eig_value = np.zeros(2,dtype=complex)
        # eig1 = np.sqrt(0.5*(temp1 - np.sqrt(temp2 - temp3, dtype=complex) ), dtype=complex)
        # eig2 = np.sqrt(0.5*(temp1 + np.sqrt(temp2 - temp3, dtype=complex) ), dtype=complex)
        eig1 = np.sqrt(0.5*(temp1 - np.sqrt(temp2 - temp3, dtype=complex) ), dtype=complex)
        eig2 = np.sqrt(0.5*(temp1 + np.sqrt(temp2 - temp3, dtype=complex) ), dtype=complex)
        
        v1   = np.array([ Aup, App-eig1 ], dtype=complex)
        v2   = np.array([ App-eig2, Apu ], dtype=complex)
        return(eig1, eig2, v1, v2)
    
    def Explicit_Dispersion_Homo(self, mass1, r_inrt1, k_Lin1, k_Rot1,
                               k_step=201, v_Pu_min=1e-4, fig_flag=True):
        k_vec = np.linspace(-np.pi/self.height0, np.pi/self.height0, k_step)
        omega = np.zeros((2,k_step),dtype=complex)
        omega[0,:],omega[1,:],v_eig1,v_eig2 = self.Eigenvalue_homogeneous(k_vec, mass1, r_inrt1, k_Lin1, k_Rot1)
        
        w0 = np.sqrt(k_Lin1/mass1)
        
        # Check
        k_sin = 0.4*np.pi
        omega0, omega1, Pu0, Pu1 = self.Calculate_Pu(k_sin/self.height0, mass1, r_inrt1, k_Lin1, k_Rot1)
        dw       = np.gradient(omega[0,:]/(2.*np.pi), k_vec)
        dw_intrp = interp1d(k_vec, dw, kind='cubic') #, kind='cubic'
        dw_sin   = dw_intrp(k_sin/self.height0)
        # v_front  = dw_sin*t_sim
        w_slope  = dw_sin*(k_vec-k_sin/self.height0) + omega0.real/(2.*np.pi)
        plt.figure('Check')
        plt.plot(k_vec, dw_intrp(k_vec), 'bo-')
        
        # Plot ----------------------------------------------------------------
        if fig_flag==True:
            plt.figure('Dispersion_Homogeneous')
#            plt.title('$\\theta_0=%d^\circ$'%np.degrees(self.angle0[0]))
#                plt.grid(True)
            plt.plot(k_vec,omega[0,:]/(2.*np.pi),'r')
            plt.plot(k_vec,omega[1,:]/(2.*np.pi),'b')
            # Check
            # Input wave number ------------------------
            plt.axvline(x=k_sin/self.height0, color='red', linestyle='--', linewidth=2, alpha=0.7)
            # Check slope
            plt.plot(k_vec, w_slope, 'k--')
            
            
            plt.xlabel('Wave number')
#                ylabel('Frequency $\omega$')
            plt.ylabel('Frequency $f$ [Hz]')
            plt.xticks([-np.pi/self.height0,0.,np.pi/self.height0],['$-\\frac{\pi}{h_0}$','$0$', '$\\frac{\pi}{h_0}$'])
            plt.xlim(-np.pi/self.height0,np.pi/self.height0)
            plt.ylim(ymin=0)
        return(omega, v_eig1, v_eig2)
           
        
if __name__ == "__main__":
    #==============================================================================
    # Geometry
    #==============================================================================
    L_side = 12.*1e-3  #[m] Side length of the cube
    H_cube = 10.*1e-3  # [m] Height of the cube
    
    # Initial configuration
    q0_Lin  = np.radians(5.)
    # Configuration of the chain
    # 'mono_C', 'mono_L', 'mono_R', 'dime_LR', 'dime_RL', 'dime_CL', 'dime_CR'
    chain_config = 'mono_C' 
    #==============================================================================
    # Material propeties
    #==============================================================================
    # Spring constant for linear spring --------------
    # Config1
    k_Lin1 = 670.8
    k_Rot1 = 0.00044325837185769376  # for t_hinge=0.75*1e-3 [m]
    # Config2
    k_Lin2 = k_Lin1
    k_Rot2 = k_Rot1
    # Parameters for the Morse potential function -----
    A_const  = 0. #0.0005477163091763655
    alpha    = 4.966064409064443
    r1_morse = 0.74422634 # for t_hinge=0.75*1e-3 [m]
    # # Material property for the disk
    # weight = 1.9888*1e-3 #[kg] including joints, screws, and bearing
    # r_inrt = 46.608*1e-9 #[kgm^2]
    # Material property -----------------------
    # mass1 = 1.9888*1e-3 #[kg] (Measured)
    # r_inrt1 = 46.608*1e-9 #[kgm^2] (Measured)
    
    # Config 1
    dens1 = 1400. #[kg/m^3]
    mass1 = dens1*L_side*L_side*H_cube
    r_inrt1 = (1./12.)*mass1*L_side*L_side
    # Config 2
    dens2 = 2.*dens1 #[kg/m^3]
    mass2 = dens2*L_side*L_side*H_cube
    r_inrt2 = (1./12.)*mass2*L_side*L_side

    rotsq_eig = RotSq_Eigenvalue_unit(L_side, q0_Lin, chain_config, k_Lin1, k_Rot1, A_const, alpha, r1_morse)

    # =========================================================================
    # Analyze frequency band structure
    # =========================================================================
    
    # Dispersion for homogeneous configuration ------------------------
    # Numerically calculated eigenvalues
    # k_vec,omega,w0,Pu,v_eig1,v_eig2 = rotsq_eig.Analyze_DispersionRelation_Monomer(mass1, r_inrt1, k_Lin1, k_Rot1)
    # Explicit form
    omega, v_eig1, v_eig2 = rotsq_eig.Explicit_Dispersion_Homo(mass1, r_inrt1, k_Lin1, k_Rot1)
    
    # Dispersion for dimer configuration ------------------------
    # k_vec,omega,w0,Pu = rotsq_eig.Analyze_DispersionRelation_Dimer(mass1, r_inrt1, mass2, r_inrt2,
    #                                                                 k_Lin1, k_Rot1, k_Lin2, k_Rot2)
    
    
    # Sweep the initial angle ------------------------
    q0_Lin_range = np.linspace(-np.radians(40), np.radians(40), 81)
    # Homogeneous
    # rotsq_eig.DispersionRelationChange_Monomer(q0_Lin_range, mass1, r_inrt1, k_Lin1, k_Rot1)
    # Dimer
    # rotsq_eig.DispersionRelationChange_Dimer(q0_Lin_range, mass1, r_inrt1, mass2, r_inrt2,
    #                                          k_Lin1, k_Rot1, k_Lin2, k_Rot2)
    
    # Calculate the determinant
    # rotsq_eig.ZeroDeterminant(mass1, r_inrt1, k_Lin1, k_Rot1)