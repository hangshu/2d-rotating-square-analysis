import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.colorbar import Colorbar
import cmath
from scipy import integrate
from scipy import fftpack
import scipy.optimize as opt
from scipy import signal
import os
import sys
import shutil   #High level file operation
sys.path.append('..')
from RotSquare_Energy import RotSquare_EnergyAnalysis
from IMS_VTK4Animation import CubicModule_VTKanimation
# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 24        # Original fornt size is 8, multipy by above number
#plt.rcParams['text.usetex']     = True
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
#plt.rcParams['text.latex.preamble'] = '\usepackage{sfmath}'
plt.rcParams['lines.linewidth'] = 4.   
plt.rcParams['lines.markersize'] = 8. 
plt.rcParams['legend.fontsize'] = 20        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in' 
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300

#==============================================================================
# Eigenvalue analysis on a finite chain
#==============================================================================
class RotSq_Eigenvalue_FiniteChain(RotSquare_EnergyAnalysis):
    def __init__(self,n_unit,L_side,q0_Lin,chain_config,
                 k_sp_Lin,k_sp_Rot,A_const,alpha,r1_morse,weight,r_inrt, q_cube0=np.radians(45)):
        # Dimension
        self.n_unit = n_unit
        self.L_side  = L_side
        self.q0_Lin  = q0_Lin
        self.chain_config = chain_config
        self.geo_L   = L_side/(2.*np.cos(q_cube0))  #[m] Half of the diagonal length of a cube
        print('Length L = {}'.format(self.geo_L*1e3))
#        self.height0   = 2.*self.geo_L*np.cos(q0_Lin)
        # Material properties
        # Spring properties
        self.k_sp_Lin  = k_sp_Lin
        self.k_sp_Rot  = k_sp_Rot
        self.A_const   = A_const
        self.alpha     = alpha
        self.r1_morse  = r1_morse
        self.r2_morse  = -r1_morse
        self.weight = weight
        self.r_inrt = r_inrt
        super().__init__(L_side,q0_Lin,chain_config,
                 k_sp_Lin,k_sp_Rot,A_const,alpha,r1_morse)
        self.q_minima   = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)

    def InitialConfiguration_Chain(self):
        rot_angl0 = np.zeros(self.n_unit)
        if len(self.q_minima)==1:
            rot_angl0[:] = self.q_minima[0]
        elif len(self.q_minima)==2:
            if self.chain_config=='mono_L': # Uniform pre-compression/tension
                rot_angl0[:] = self.q_minima[0]
            elif self.chain_config=='mono_R': # Uniform pre-compression/tension
                rot_angl0[:] = self.q_minima[1]
            elif self.chain_config=='mono_C': # Uniform pre-compression/tension
                rot_angl0[:] = self.q_minima[1]
        else:
            if self.chain_config=='mono_C': # Uniform pre-compression/tension
                rot_angl0[:] = self.q_minima[1]
            elif self.chain_config=='mono_L': # Uniform pre-compression/tension
                rot_angl0[:] = self.q_minima[0]
            elif self.chain_config=='mono_R': # Uniform pre-compression/tension
                rot_angl0[:] = self.q_minima[2]
            elif self.chain_config=='PB_1': # Uniform pre-compression/tension
                rot_angl0[0:int(self.n_unit/2)-1] = self.q_minima[0]
                rot_angl0[int(self.n_unit/2)-1]   = self.q_minima[0]*0.96
                rot_angl0[int(self.n_unit/2)]     = self.q_minima[1]
                rot_angl0[int(self.n_unit/2)+1]   = self.q_minima[2]*0.96
                rot_angl0[int(self.n_unit/2)+2:]  = self.q_minima[2]
        height0   = 2.*self.geo_L*np.cos(rot_angl0)  #[m]
        return(rot_angl0, height0)
    
    def Coefficients_Linear(self, i_unit):
        # Coefficient for Morse potential function
        wrk1 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(2.*self.alpha*(2.*self.q0_chain[i_unit]-2.*self.r1_morse)) \
                 - np.exp(   self.alpha*(2.*self.q0_chain[i_unit]-2.*self.r1_morse)) )
        wrk2 = 2.*self.alpha**2*self.A_const*( \
                2.*np.exp(-2.*self.alpha*(2.*self.q0_chain[i_unit]-2.*self.r2_morse)) \
                 - np.exp(   -self.alpha*(2.*self.q0_chain[i_unit]-2.*self.r2_morse)) )
        T_slope = wrk1 + wrk2
        # Coefficients for a linearized finite chain
        Auu = self.k_sp_Lin
        Auq = self.k_sp_Lin*self.geo_L*np.sin(self.q0_chain[i_unit])
        Aqu = self.k_sp_Lin*self.geo_L*np.sin(self.q0_chain[i_unit])
#        Aqq1 = -2.*self.k_sp_Lin*self.geo_L**2*(1.+np.sin(self.q0_Lin)**2) -4.*self.k_sp_Rot
        Aqq1 = -2.*self.k_sp_Lin*self.geo_L**2 -4.*self.k_sp_Rot - 4.*T_slope
        Aqq2 = self.k_sp_Lin*self.geo_L**2*np.cos(2.*self.q0_chain[i_unit]) - self.k_sp_Rot - T_slope
#        Aqq1p = -self.k_sp_Lin*self.geo_L**2*(1.+2.*np.sin(self.q0_Lin)**2) - 3.*self.k_sp_Rot
        Aqq1p = -self.k_sp_Lin*self.geo_L**2 - 3.*self.k_sp_Rot - 3.*T_slope
        return(Auu,Auq,Aqu,Aqq1,Aqq2,Aqq1p,T_slope)
    
    def Finite_chain(self, L_end, mass_config):
        Mmat = np.zeros((2*self.n_unit+2,2*self.n_unit+2))
        Bmat = np.zeros((2*self.n_unit+2,2*self.n_unit+2))
        
        for i in range(2*self.n_unit+2):
            for j in range(2*self.n_unit+2):
                # First unit ---------------------------------------
                # Coefficients
                A1uu,A1uq,A1qu,A1qq1,A1qq2,A1qq1p,T1_slope = self.Coefficients_Linear(i_unit=0)
                if i<2:
                    # Translational
                    if i%2==0:
                        if i==j:
                            Bmat[i,j] = -A1uu
                            Mmat[i,j] = mass_config[int(i/2)]
                        elif i==j-1:
                            Bmat[i,j] = A1uq
                        elif i==j-2:
                            Bmat[i,j] = A1uu
                        elif i==j-3:
                            Bmat[i,j] = A1uq
                    # Rotational
                    else:
                        if i==j+1:
                            Bmat[i,j] = A1qu
                        elif i==j:
                            Bmat[i,j] = A1qq1p
                            Mmat[i,j] = self.r_inrt
                        elif i==j-1:
                            Bmat[i,j] = -A1qu
                        elif i==j-2:
                            Bmat[i,j] = A1qq2
                # Last unit ----------------------------------------
                elif i>2*self.n_unit-1:
                    # Coefficients
                    A1uu,A1uq,A1qu,A1qq1,A1qq2,A1qq1p,T1_slope = self.Coefficients_Linear(i_unit=int(i/2-1))
                    if i%2==0: # Translational
                        if i==j+2:
                            Bmat[i,j] = A1uu
                        elif i==j+1:
                            Bmat[i,j] = -A1uq
                        elif i==j:
                            Bmat[i,j] = -A1uu
                            Mmat[i,j] = mass_config[int(i/2)]
                        elif i==j-1:
                            Bmat[i,j] = -A1uq
                    else: # Rotational
                        if i==j+3:
                            Bmat[i,j] = A1qu
                        elif i==j+2:
                            Bmat[i,j] = A1qq2
                        elif i==j+1:
                            Bmat[i,j] = -A1qu
                        elif i==j:
                            Bmat[i,j] = A1qq1p
                            Mmat[i,j] = self.r_inrt
                # Rest of the chain ----------------------------------------
                else:
                    # Coefficients
                    A1uu,A1uq,A1qu,A1qq1,A1qq2,A1qq1p,T1_slope = self.Coefficients_Linear(i_unit=int(i/2-1))
                    A2uu,A2uq,A2qu,A2qq1,A2qq2,A2qq1p,T2_slope = self.Coefficients_Linear(i_unit=int(i/2))
                    if i%2==0: # Translational ---------
                        if i==j+2:
                            Bmat[i,j] = A2uu
                        elif i==j+1:
                            Bmat[i,j] = -A2uq
                        elif i==j:
                            Bmat[i,j] = -A1uu -A2uu
                            Mmat[i,j] = mass_config[int(i/2)]
                        elif i==j-1:
                            Bmat[i,j] = -A2uq + A1uq
                        elif i==j-2:
                            Bmat[i,j] = A1uu
                        elif i==j-3:
                            Bmat[i,j] = A1uq
                    else: # Rotational --------------
                        if i==j+3:
                            Bmat[i,j] = A2qu
                        elif i==j+2:
                            Bmat[i,j] = A2qq2
                        elif i==j+1:
                            Bmat[i,j] = -A2qu + A1qu
                        elif i==j:
                            Bmat[i,j] = A1qq1 + T1_slope - T2_slope
                            Mmat[i,j] = self.r_inrt
                        elif i==j-1:
                            Bmat[i,j] = -A1qu
                        elif i==j-2:
                            Bmat[i,j] = A1qq2
        return(Mmat,Bmat)

#
    def EigenvalueAnalysis(self, i_mode, v_adjust, mass_config,
                           fig_flag=True, L_end='free', eigmode_scale=0.8):
        if i_mode > 2*self.n_unit+2:
            i_mode= 1
        # Calculate the initial configurations ----------------------
        self.q0_chain, self.h0_chain = self.InitialConfiguration_Chain()
        # Calculate coefficients
        Mmat,Bmat = self.Finite_chain(L_end, mass_config)

        # Calculate eigenvalues
        Lmat = np.dot(np.linalg.inv(Mmat),Bmat)
        la,v = np.linalg.eig(Lmat)
        omega = np.zeros(len(la))
        for i in range(len(la)):
            aa = cmath.sqrt(-la[i])
            omega[i] = aa.real
#            if aa.imag != 0:
#                print 'Complex eigenvalue:',aa
#        omega = sort(sqrt(-la))
#        omega = sort(temp)
        i_omega = np.argsort(omega)
        eig_vec = v[:,i_omega[:]]
        mode = np.arange(2*self.n_unit+3)

        def Find_k(v, i_omega, i, mode, fig_flag=False, color='r'):
            N  = len(v[mode::2, i_omega[i]])     # Number of data points
            dx = self.h0_chain[0]           # Sampling distance
            # FFT and Normalization
            yf = 2.*fftpack.fft(v[mode::2, i_omega[i]])/N
            # Frequency scale
            k_temp = 2.*np.pi*fftpack.fftfreq(N, dx)
    #                k_shft = fftpack.fftshift( k_temp )
            # Express magnitude by using dB
            magnitude = abs(yf)
            dbs       = 20*np.log10(magnitude)
#            i_max = np.argmax(dbs)
            maxid = signal.argrelmax(dbs, order=1)
            if len(maxid[0])==0:
                k   = 0.
                amp = -1e5
                gap = 0.
            else:
                i_k = np.argsort(k_temp[maxid[0]])
#                k   = abs(k_temp[maxid[0][i_k[-1]]])
#                amp = dbs[maxid[0][i_k[-1]]]
                k   = abs(k_temp[np.argmax(dbs)])
                amp = dbs[np.argmax(dbs)]
                if len(maxid[0])>2:
                    gap = dbs[maxid[0][i_k[-1]]] - dbs[maxid[0][i_k[-2]]]
                else:
                    gap = 0.
            if fig_flag==True:
#                print(k_temp[maxid[0][i_k]])
                plt.figure('Check dbs')
                plt.plot(k_temp, dbs, color=color)
                plt.axvline(x=k, color=color)
            return(k,amp,dbs,gap)
        
#        i_la = np.argsort(la)
        
        k_vec_fft = np.zeros(len(la))
        for i in range(len(k_vec_fft)):
            k_disp,amp_disp,_,gap_disp = Find_k(v_adjust*v, i_omega, i, mode=0)
            k_rot,amp_rot,_,gap_rot  = Find_k(v, i_omega, i, mode=1)
            if amp_disp>amp_rot:
                k_vec_fft[i] = k_disp
            else:
                k_vec_fft[i] = k_rot
#            if gap_disp>gap_rot:
#                k_vec_fft[i] = k_disp
#            else:
#                k_vec_fft[i] = k_rot


        # =====================================================================
        # Animation: Eigenmodes
        # =====================================================================
        # Create VTK files -----------------------------
        N_step = 100
        dir_cwd = os.getcwd()
        scale_disp = eigmode_scale*np.sin(np.linspace(0,2.*np.pi,N_step))
        scale_angl = eigmode_scale*np.sin(np.linspace(0,2.*np.pi,N_step))
        # Lower mode ----------
        fname   ='RotSquare_eigenmode'
        ims_vtk = CubicModule_VTKanimation(self.n_unit, self.h0_chain, -self.q0_chain, self.geo_L, fname, dir_cwd)
        for i_step in range(N_step):
            ims_vtk.CreateVTK(disp_x=v[::2, i_omega[i_mode-1]].real*scale_disp[i_step],
                              angl_z=v[1::2,i_omega[i_mode-1]].real*scale_angl[i_step], i_step=i_step)
        
#        k_disp,amp_disp,dbs_disp,gap_disp = Find_k(v_adjust*v, i_omega, i_mode-1, mode=0, fig_flag=fig_flag, color='r')
        k_rot,amp_rot,dbs_rot,gap_rot  = Find_k(v, i_omega, i_mode-1, mode=1, fig_flag=fig_flag, color='b')

        #==============================================================================
        # Plot
        #==============================================================================
        w0 = np.sqrt(self.k_sp_Lin/self.weight)
        if fig_flag==True:
            
            plt.figure('Eigenvalue Analysis')
#            plt.grid(True)
            for i in range(len(i_omega)):
                plt.plot(mode[i+1], omega[i_omega[i]]/(2.*np.pi),'ro')
                # Check locarization
                v_local = np.max(abs(eig_vec[0,i])) - np.max(abs(eig_vec[-1,i]))
                if v_local > 0.5*np.max(abs(eig_vec[0,i])):
                    plt.plot(mode[i+1], omega[i_omega[i]]/(2.*np.pi),'go')
            plt.plot(i_mode, omega[i_omega[i_mode-1]]/(2.*np.pi),'bx')
            #plot(k_ana,w_ana)
            plt.xlabel('Mode number')
            plt.ylabel('Frequency $f$ [Hz]')
    #        xticks([mode[0],mode[-1]],['$0$', '$\pi$'])
            plt.xlim(mode[0],mode[-1])
            plt.ylim(ymin=0)
            
            plt.figure('Eigenvalue Analysis (normalized)')
#            plt.grid(True)
            for i in range(len(i_omega)):
                plt.plot(mode[i+1], omega[i_omega[i]]/w0,'ro')
                # Check locarization
                v_local = abs(eig_vec[-2,i]) - abs(eig_vec[int(self.n_unit/2),i])
                if v_local > 0.7*np.max(abs(eig_vec[0::2,i])):
                    plt.plot(mode[i+1], omega[i_omega[i]]/w0,'go')
            plt.plot(i_mode, omega[i_omega[i_mode-1]]/w0,'bx')
            #plot(k_ana,w_ana)
            plt.xlabel('Mode number')
            plt.ylabel('Frequency $\omega/\omega_0$')
    #        xticks([mode[0],mode[-1]],['$0$', '$\pi$'])
            plt.xlim(mode[0],mode[-1])
            plt.ylim(ymin=0)
            
            
            plt.figure('Eigenvalue Analysis 2 (normalized)')
#            plt.grid(True)
            for i in range(len(i_omega)):
                plt.plot(k_vec_fft[i], omega[i_omega[i]]/w0,'ro')
            plt.plot(k_vec_fft[i_mode-1], omega[i_omega[i_mode-1]]/w0,'bx')
            plt.xlabel('Wave number, $k$')
            plt.ylabel('Frequency $\omega/\omega_0$')
            plt.xticks([0., np.pi/self.h0_chain[0]],['$0$', '$\\frac{\pi}{H_0}$'])
            plt.xlim(0, np.pi/self.h0_chain[0])
    #        xticks([mode[0],mode[-1]],['$0$', '$\pi$'])
#            plt.xlim(mode[0],mode[-1])
            plt.ylim(ymin=0)

            
            # Mode shapes ---------------------------------------------------------
            plt.figure('Mode_shape2')
            plt.subplot(211)
            plt.title('Mode=%d, $f$=%1.2f'%(i_mode,omega[i_omega[i_mode-1]]/(2.*np.pi)))
            plt.plot(np.arange(1,len(v[::2,i_omega[i_mode-1]])+1),v[::2,i_omega[i_mode-1]],'ro-')
    #        for i in range(len(v[:,i_omega[i_mode-1]])):
    #            if i%2==0:
    #                plot(i+1,v[i,i_omega[i_mode-1]],'ro', markersize=9)
            plt.ylabel('Modal $u_n$')
            plt.ylim(-max(abs(v[::2,i_omega[i_mode-1]])),max(abs(v[::2,i_omega[i_mode-1]])))
#            plt.yticks([0],['0'])
            plt.subplot(212)
            plt.plot(np.arange(1,len(v[1::2,i_omega[i_mode-1]])+1),v[1::2,i_omega[i_mode-1]],'ro-')
    #        for i in range(len(v[:,i_omega[i_mode-1]])):
    #            if i%2==1:
    #                plot(i+1,v[i,i_omega[i_mode-1]],'bo', markersize=9)
            plt.ylabel('Mode $\\theta_n$')
            plt.xlabel('Unit index, $n$')
            plt.ylim(-1.3*max(abs(v[1::2,i_omega[i_mode-1]])), 1.3*max(abs(v[1::2,i_omega[i_mode-1]])))
#            plt.ylim(-max(abs(v[::2,i_omega[i_mode-1]])),max(abs(v[::2,i_omega[i_mode-1]])))
#            plt.yticks([0],['0'])
        return(k_vec_fft, omega[i_omega[:]],w0)
#   
    
if __name__ == "__main__":
    #==============================================================================
    # Geometry
    #==============================================================================
    n_unit = 100
    L_side = 12.*1e-3  #[m] Side length of the cube

    # Initial configuration
    q0_Lin  = np.radians(1.)
    q_cube0 = np.radians(45)
    # Configuration of the chain
    # 'mono_C', 'mono_L', 'mono_R', 'PB_1', 'PB_2'
    chain_config = 'mono_C' 
    #==============================================================================
    # Material propeties
    #==============================================================================
    # Spring constant for linear spring
    k_sp_Lin = 670.8
    k_sp_Rot = 0.00044325837185769376  # for t_hinge=0.75*1e-3 [m]
    A_const  = 0.0005477163091763655
    alpha    = 4.966064409064443
    r1_morse = 0.74422634 # for t_hinge=0.75*1e-3 [m]
    # Material property for the disk
    weight = 1.9888*1e-3     #[kg] including joints, screws, and bearing
    r_inrt = 46.608*1e-9 #[kgm^2]
    mass_config = weight*np.ones(n_unit+1)
#    mass_config[1::2] = 2*weight

    rotsq_eig = RotSq_Eigenvalue_FiniteChain(n_unit,L_side,q0_Lin,chain_config,
                            k_sp_Lin,k_sp_Rot,A_const,alpha,r1_morse,weight,r_inrt)
#    Auu,Auq,Aqu,Aqq1,Aqq2,Aqq1p,T_slope = rotsq_eig.Coefficients_Linear(i_unit=49)
    # =========================================================================
    # Analyze frequency band structure
    # =========================================================================
#    # Calculate the initial configurations ----------------------
#    q0_chain, h0_chain = rotsq_eig.InitialConfiguration_Chain()
    # Dispersion for single configuration ------------------------
    k_vec_fft, omega, w0 = rotsq_eig.EigenvalueAnalysis(i_mode=50, v_adjust = 200.,
                                                     mass_config=mass_config, fig_flag=True, eigmode_scale=0.8)
    
   
    
    
    
    
    
    
    