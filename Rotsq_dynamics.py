#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 30 23:36:45 2021

@author: hangshu
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.colorbar import Colorbar
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import cmath
import scipy.optimize as opt
from scipy import signal
from scipy import integrate
from scipy import linalg
from scipy.interpolate import interp1d
import os
import sys
import shutil   #High level file operation
import time
import warnings
from scipy import fftpack
from scipy import signal
#from IMS_VTK4Animation import CubicModule_VTKanimation
sys.path.append('../2d-rotating-square-analysis/linear-analysis/')
from RotSquare_Energy import RotSquare_EnergyAnalysis
# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 18       # Original fornt size is 8, multipy by above number
plt.rcParams['text.usetex']     = False
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['lines.linewidth'] = 3.  
plt.rcParams['lines.markersize'] = 10.
plt.rcParams['legend.fontsize'] = 16        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 16        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 16        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300

#==============================================================================
# compute dynamics of Rotating square chain
#==============================================================================
class RotSq_Dynamics_unit(RotSquare_EnergyAnalysis):
    def __init__(self, L_side, q0_Lin, chain_config, k_Linh, k_Linv, k_Rot1,
                 A_const, alpha, r1_morse, cells, damp_tra, damp_rot, mass1, 
                                      r_inrt1, row_x, row_y, q_cube0=np.radians(45)):
        # Dimension
        self.L_side = L_side
        self.q0_Lin  = q0_Lin
        self.chain_config = chain_config
        self.geo_L   = L_side/(2.*np.cos(q_cube0))  #[m] Half of the diagonal length of a cube
        super().__init__(L_side, q0_Lin, chain_config,
                         k_Lin1, k_Rot1, A_const, alpha, r1_morse)
        self.q_minima   = self.Find_LocalMinima(dQ_angle=2.*np.radians(np.linspace(-50., 50, 301)), fig_flag=False)
        self.angle0,self.height0 = self.InitialConfiguration()

        self.h0_chain = 2*self.geo_L*np.cos(self.q0_Lin)
        # self.height0= 2*self.geo_L*np.cos(self.q0_Lin)
        # self.angle0 = q0_Lin
        # Spring properties
        self.k_u  = k_Linh
        self.k_v  = k_Linv
        self.k_s  = k_Rot1
        
        # Material properties        
        self.A_const   = A_const
        self.alpha     = alpha
        self.r1_morse  = r1_morse
        self.r2_morse  = -r1_morse
        self.n_unit = cells

        #Hinge properties 
        self.damp_tra = damp_tra
        self.damp_rot = damp_rot

        #Dimensions 2d
        self.row_x = row_x
        self.row_y = row_y
        self.n_unit = cells
        
        #mass
        self.mass1 = mass1
        self.r_inrt1 = r_inrt1
##################current sample is for 2by2 matrix testing
    def dynamic_solver(self, u0, t0, tf, tstep, tsave, fig_flag=True ):
       
        ###computes a 6 by N matrix for each time frame    
        def EOM(sol_temp, t):
            
            ddu = np.zeros(self.n_unit)
            ddv = np    
    t_elapse = time.perf_counter()-tic
    
    print("Simulation elapse time : {:.4f}s".format(t_elapse))
    
    print("Start FFT and energy analysis:").zeros(self.n_unit)
            ddw = np.zeros(self.n_unit)
            
            u_lin = self.k_u*self.geo_L
            v_lin = self.k_v*self.geo_L
            w_lin = self.k_s*self.geo_L
            
            u = sol_temp[0:self.n_unit]
            v = sol_temp[self.n_unit:2*self.n_unit]        
            w = sol_temp[2*self.n_unit:3*self.n_unit]   
            du = sol_temp[3*self.n_unit:4*self.n_unit]   
            dv = sol_temp[4*self.n_unit:5*self.n_unit]    
            dw = sol_temp[5*self.n_unit:6*self.n_unit]     
            
            wrk1 = 2.*self.alpha**2*self.A_const*( \
                    2.*np.exp(2.*self.alpha*(2.*self.angle0-2.*self.r1_morse)) \
                     - np.exp(   self.alpha*(2.*self.angle0-2.*self.r1_morse)) )
            wrk2 = 2.*self.alpha**2*self.A_const*( \
                    2.*np.exp(-2.*self.alpha*(2.*self.angle0-2.*self.r2_morse)) \
                     - np.exp(   -self.alpha*(2.*self.angle0-2.*self.r2_morse)) )
               
            T_slope = wrk1 + wrk2
      
            ################construct equations of motion#####################
            #[2, 3] cell numberings 2by2
            #[0, 1]
            #############ddu###############################
            for i in range(self.n_unit):
      
                w_sin = np.sin(w[i]+self.angle0)
                w_cos = np.cos(w[i]+self.angle0)

                
                n = int(divmod(i,row_x)[1]+1)
                m = int(divmod(i,row_x)[0]+1)
                if n == 1:
                    w_sin_np =  np.sin(w[i+1]+self.angle0)
                    w_cos_np =  np.cos(w[i+1]+self.angle0)
                    if m == 1:

                        w_sin_mp =  np.sin(w[i+self.row_x]+self.angle0)
                        w_cos_mp =  np.cos(w[i+self.row_x]+self.angle0)

                        ddu[i] =  (self.k_u*(u[i+1]-2*u[i])+self.k_v*(u[i+self.row_x]-2*u[i]) \
                                    +u_lin*(-w_cos_np)+v_lin*w_sin_mp-du[i]*self.damp_tra)/self.mass1
                                
                                
                        ddv[i] =  (self.k_v*(v[i+1]-2*v[i])+self.k_u*(v[i+self.row_x]-2*v[i]) \
                                   +u_lin*(-w_sin_mp)+v_lin*(-w_cos_np)-dv[i]*self.damp_tra)/self.mass1  
                               
                               
                               
                        ddw[i] = (-self.k_s*(4*w[i]+w[i+1]+w[i+self.row_x]+8*(self.angle0-q0_Lin)) \
                                     -u_lin*w_sin*(u[i+1]-u[i]-self.geo_L*w_cos_np-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(u[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i+self.row_x]-u[i]+pow(-1,n+m)*self.geo_L*w_sin_mp+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i]+pow(-1,n+m)*self.geo_L*w_sin) \
                                     -u_lin*w_sin*(v[i+self.row_x]-v[i]-self.geo_L*w_cos_mp-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(v[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i+1]-v[i]-pow(-1,n+m)*self.geo_L*w_sin_np-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-pow(-1,n+m-1)*self.geo_L*w_sin)  \
                                     - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
                            
                    elif m == row_y:
                        w_sin_mm =  np.sin(w[i-self.row_x]+self.angle0)
                        w_cos_mm =  np.cos(w[i-self.row_x]+self.angle0)
                        ddu[i] =  (self.k_u*(u[i+1]-2*u[i])+self.k_v*(u[i-self.row_x]-2*u[i]) \
                                    +u_lin*(-w_cos_np)-v_lin*pow(-1,n+m)*w_sin_mm-du[i]*self.damp_tra)/self.mass1
                                
                                
                        ddv[i] =  (self.k_u*(v[i+1]-2*v[i])+self.k_v*(v[i-self.row_x]-2*v[i]) \
                                   +v_lin*(w_cos_mm)+pow(-1,n+m)*u_lin*(-w_sin_np)-dv[i]*self.damp_tra)/self.mass1  
                               
                               
                               
                        ddw[i] = (-self.k_s*(4*w[i]+w[i+1]+w[i-self.row_x]+8*(self.angle0-q0_Lin)) \
                                     -u_lin*w_sin*(u[i+1]-u[i]-self.geo_L*w_cos_np-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(u[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(-u[i]+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i]-u[i-self.row_x]+pow(-1,n+m-1)*self.geo_L*w_sin+pow(-1,n+m-2)*self.geo_L*w_sin_mm) \
                                     -u_lin*w_sin*(-v[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(v[i]-v[i-self.row_x]-self.geo_L*w_cos-self.geo_L*w_cos_mm+2*self.geo_L*np.cos(self.angle0)) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i+1]-v[i]-pow(-1,n+m)*self.geo_L*w_sin_np-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
                            
                    else:    
                        w_sin_mp =  np.sin(w[i+self.row_x]+self.angle0)
                        w_sin_mm =  np.sin(w[i-self.row_x]+self.angle0)
                        w_cos_mp =  np.cos(w[i+self.row_x]+self.angle0)
                        w_cos_mm =  np.cos(w[i-self.row_x]+self.angle0) 
    
                                
                        ddu[i] =  (self.k_u*(u[i+1]-2*u[i])+self.k_v*(u[i-self.row_x]+u[i+self.row_x]-2*u[i]) \
                                    +u_lin*(-w_cos_np)+v_lin*pow(-1,n+m)*(w_sin_mp-w_sin_mm)-du[i]*self.damp_tra)/self.mass1
                                
                                
                        ddv[i] =  (self.k_u*(v[i+1]-2*v[i])+self.k_v*(v[i-self.row_x]+v[i+self.row_x]-2*v[i]) \
                                   +v_lin*(w_cos_mm-w_cos_mp)+pow(-1,n+m)*v_lin*(-w_sin_np)-dv[i]*self.damp_tra)/self.mass1  
                           
                           
                           
                        ddw[i] = (-self.k_s*(4*w[i]+w[i+1]+w[i-self.row_x]+w[i+self.row_x]+8*(self.angle0-q0_Lin)) \
                                     -u_lin*w_sin*(u[i+1]-u[i]-self.geo_L*w_cos_np-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(u[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i+self.row_x]-u[i]+pow(-1,n+m)*self.geo_L*w_sin_mp+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i]-u[i-self.row_x]+pow(-1,n+m-1)*self.geo_L*w_sin+pow(-1,n+m-2)*self.geo_L*w_sin_mm) \
                                     -u_lin*w_sin*(v[i+self.row_x]-v[i]-self.geo_L*w_cos_mp-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(v[i]-v[i-self.row_x]-self.geo_L*w_cos-self.geo_L*w_cos_mm+2*self.geo_L*np.cos(self.angle0)) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i+1]-v[i]-pow(-1,n+m)*self.geo_L*w_sin_np-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
                                 
                                     
           
                elif n == self.row_x:
                    w_sin_nm =  np.sin(w[i-1]+self.angle0)
                    w_cos_nm =  np.cos(w[i-1]+self.angle0)
                    if m == 1:
                        w_sin_mp =  np.sin(w[i+self.row_x]+self.angle0)
                        w_cos_mp =  np.cos(w[i+self.row_x]+self.angle0)
                        ddu[i] =  (self.k_u*(u[i-1]-2*u[i])+self.k_v*(u[i+self.row_x]-2*u[i]) \
                                    +u_lin*(w_cos_nm)+pow(-1,n+m)*v_lin*w_sin_mp-du[i]*self.damp_tra)/self.mass1
                                    
                                    
                        ddv[i] =  (self.k_u*(v[i-1]-2*v[i])+self.k_v*(v[i+self.row_x]-2*v[i]) \
                                   +v_lin*(-w_cos_mp)+pow(-1,n+m)*u_lin*(w_sin_nm)-dv[i]*self.damp_tra)/self.mass1        
                                    
                                   
                        ddw[i] = (-self.k_s*(4*w[i]+w[i-1]+w[i+self.row_x]+8*(self.angle0-q0_Lin)) \
                                     -u_lin*w_sin*(-u[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(u[i]-u[i-1]-self.geo_L*w_cos-self.geo_L*w_cos_nm+2*self.geo_L*np.cos(self.angle0)) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i+self.row_x]-u[i]+pow(-1,n+m)*self.geo_L*w_sin_mp+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i]+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     -u_lin*w_sin*(v[i+self.row_x]-v[i]-self.geo_L*w_cos_mp-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(v[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(-v[i]-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-v[i-1]-pow(-1,n+m-1)*self.geo_L*w_sin-pow(-1,n+m-2)*self.geo_L*w_sin_nm) \
                                     - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
                            
                    elif m == self.row_y:
                        w_sin_mm =  np.sin(w[i-self.row_x]+self.angle0)
                        w_cos_mm =  np.cos(w[i-self.row_x]+self.angle0)
                        ddu[i] =  (self.k_u*(u[i-1]-2*u[i])+self.k_v*(u[i-self.row_x]-2*u[i]) \
                                    +u_lin*(w_cos_nm)+v_lin*pow(-1,n+m)*(-w_sin_mm)-du[i]*self.damp_tra)/self.mass1
                                
                                
                        ddv[i] =  (self.k_u*(v[i-1]-2*v[i])+self.k_v*(v[i-self.row_x]-2*v[i]) \
                                   +v_lin*(w_cos_mm)+pow(-1,n+m)*u_lin*(w_sin_nm)-dv[i]*self.damp_tra)/self.mass1 
                               
                               
                               
                        ddw[i] = (-self.k_s*(4*w[i]+w[i-self.row_x]+w[i-1]+8*(self.angle0-q0_Lin)) \
                                     -u_lin*w_sin*(-u[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(u[i]-u[i-1]-self.geo_L*w_cos-self.geo_L*w_cos_nm+2*self.geo_L*np.cos(self.angle0)) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(-u[i]+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i]-u[i-self.row_x]+pow(-1,n+m-1)*self.geo_L*w_sin+pow(-1,n+m-2)*self.geo_L*w_sin_mm) \
                                     -u_lin*w_sin*(-v[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(v[i]-v[i-self.row_x]-self.geo_L*w_cos-self.geo_L*w_cos_mm+2*self.geo_L*np.cos(self.angle0)) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(-v[i]-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-v[i-1]-pow(-1,n+m-1)*self.geo_L*w_sin-pow(-1,n+m-2)*self.geo_L*w_sin_nm) \
                                     - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
                        
                    else:        
                        w_sin_mp =  np.sin(w[i+self.row_x]+self.angle0)
                        w_sin_mm =  np.sin(w[i-self.row_x]+self.angle0)
                        w_cos_mp =  np.cos(w[i+self.row_x]+self.angle0)
                        w_cos_mm =  np.cos(w[i-self.row_x]+self.angle0)  
  
    
                        ddu[i] =  (self.k_u*(u[i-1]-2*u[i])+self.k_v*(u[i-self.row_x]+u[i+self.row_x]-2*u[i]) \
                                +u_lin*(w_cos_nm)+v_lin*pow(-1,n+m)*(w_sin_mp-w_sin_mm)-du[i]*self.damp_tra)/self.mass1
                        
                                
                        ddv[i] =  (self.k_u*(v[i-1]-2*v[i])+self.k_v*(v[i-self.row_x]+v[i+self.row_x]-2*v[i]) \
                                   +u_lin*(w_cos_mm-w_cos_mp)+pow(-1,n+m)*v_lin*(w_sin_nm)-dv[i]*self.damp_tra)/self.mass1  
                               
                               
                               
                        ddw[i] = (-self.k_s*(4*w[i]+w[i-self.row_x]+w[i-1]+w[i+self.row_x]+8*(self.angle0-q0_Lin)) \
                                     -u_lin*w_sin*(-u[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(u[i]-u[i-1]-self.geo_L*w_cos-self.geo_L*w_cos_nm+2*self.geo_L*np.cos(self.angle0)) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i+self.row_x]-u[i]+pow(-1,n+m)*self.geo_L*w_sin_mp+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     -pow(-1,n+m-1)*v_lin*w_cos*(u[i]-u[i-self.row_x]+pow(-1,n+m-1)*self.geo_L*w_sin+pow(-1,n+m-2)*self.geo_L*w_sin_mm) \
                                     -u_lin*w_sin*(v[i+self.row_x]-v[i]-self.geo_L*w_cos_mp-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                     -u_lin*w_sin*(v[i]-v[i-self.row_x]-self.geo_L*w_cos-self.geo_L*w_cos_mm+2*self.geo_L*np.cos(self.angle0)) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(-v[i]-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                     +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-v[i-1]-pow(-1,n+m-1)*self.geo_L*w_sin-pow(-1,n+m-2)*self.geo_L*w_sin_nm) \
                                     - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1             
    

                              
                    ##################################
                elif m == 1:
                    w_sin_np =  np.sin(w[i+1]+self.angle0)
                    w_sin_nm =  np.sin(w[i-1]+self.angle0)
                    w_cos_np =  np.cos(w[i+1]+self.angle0)
                    w_cos_nm =  np.cos(w[i-1]+self.angle0)
                    w_sin_mp =  np.sin(w[i+self.row_x]+self.angle0)
                    w_cos_mp =  np.cos(w[i+self.row_x]+self.angle0)
                    ddu[i] =  (self.k_u*(u[i-1]+u[i+1]-2*u[i])+self.k_v*(u[i+self.row_x]-2*u[i]) \
                                +u_lin*(w_cos_nm-w_cos_np)+pow(-1,n+m)*v_lin*w_sin_mp-du[i]*self.damp_tra)/self.mass1
                                
                                
                    ddv[i] =  (self.k_u*(v[i-1]+v[i+1]-2*v[i])+self.k_v*(v[i+self.row_x]-2*v[i]) \
                               +v_lin*(-w_cos_mp)+pow(-1,n+m)*u_lin*(-w_sin_np+w_sin_nm)-dv[i]*self.damp_tra)/self.mass1 
                               
                    ddw[i] = (-self.k_s*(4*w[i]+w[i+1]+w[i-1]+w[i+self.row_x]+8*(self.angle0-q0_Lin)) \
                                -u_lin*w_sin*(u[i+1]-u[i]-self.geo_L*w_cos_np-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                -u_lin*w_sin*(u[i]-u[i-1]-self.geo_L*w_cos-self.geo_L*w_cos_nm+2*self.geo_L*np.cos(self.angle0)) \
                                -pow(-1,n+m-1)*v_lin*w_cos*(u[i+self.row_x]-u[i]+pow(-1,n+m)*self.geo_L*w_sin_mp+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                -pow(-1,n+m-1)*v_lin*w_cos*(u[i]+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                -u_lin*w_sin*(v[i+self.row_x]-v[i]-self.geo_L*w_cos_mp-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                -u_lin*w_sin*(v[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                +pow(-1,n+m-1)*v_lin*w_cos*(v[i+1]-v[i]-pow(-1,n+m)*self.geo_L*w_sin_np-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-v[i-1]-pow(-1,n+m-1)*self.geo_L*w_sin-pow(-1,n+m-2)*self.geo_L*w_sin_nm) \
                                - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
             
                                 
     
                elif m == self.row_y:
                    w_sin_mm =  np.sin(w[i-self.row_x]+self.angle0)
                    w_cos_mm =  np.cos(w[i-self.row_x]+self.angle0)  
                    w_sin_np =  np.sin(w[i+1]+self.angle0)
                    w_cos_np =  np.cos(w[i+1]+self.angle0)
 
    
                    ddu[i] =  (self.k_u*(u[i+1]+u[i-1]-2*u[i])+self.k_v*(u[i-self.row_x]-2*u[i]) \
                                +u_lin*(w_cos_nm-w_cos_np)+v_lin*pow(-1,n+m)*(-w_sin_mm)-du[i]*self.damp_tra)/self.mass1
                            
                            
                    ddv[i] =  (self.k_u*(v[i+1]+v[i-1]-2*v[i])+self.k_v*(v[i-self.row_x]-2*v[i]) \
                               +v_lin*(w_cos_mm)+pow(-1,n+m)*u_lin*(w_sin_nm-w_sin_np)-dv[i]*self.damp_tra)/self.r_inrt1  
                           
                           
                           
                    ddw[i] = (-self.k_s*(4*w[i]+w[i+1]+w[i-self.row_x]+w[i-1]+8*(self.angle0-q0_Lin)) \
                                 -u_lin*w_sin*(u[i+1]-u[i]-self.geo_L*w_cos_np-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                 -u_lin*w_sin*(u[i]-u[i-1]-self.geo_L*w_cos-self.geo_L*w_cos_nm+2*self.geo_L*np.cos(self.angle0)) \
                                 -pow(-1,n+m-1)*v_lin*w_cos*(-u[i]+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                 -pow(-1,n+m-1)*v_lin*w_cos*(u[i]-u[i-self.row_x]+pow(-1,n+m-1)*self.geo_L*w_sin+pow(-1,n+m-2)*self.geo_L*w_sin_mm) \
                                 -u_lin*w_sin*(-v[i]-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                 -u_lin*w_sin*(v[i]-v[i-self.row_x]-self.geo_L*w_cos-self.geo_L*w_cos_mm+2*self.geo_L*np.cos(self.angle0)) \
                                 +pow(-1,n+m-1)*v_lin*w_cos*(v[i+1]-v[i]-pow(-1,n+m)*self.geo_L*w_sin_np-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                 +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-v[i-1]-pow(-1,n+m-1)*self.geo_L*w_sin-pow(-1,n+m-2)*self.geo_L*w_sin_nm)\
                                 - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
             
                
                                       



             
             
                else: 
                    w_sin_np =  np.sin(w[i+1]+self.angle0)
                    w_sin_nm =  np.sin(w[i-1]+self.angle0)
                    w_cos_np =  np.cos(w[i+1]+self.angle0)
                    w_cos_nm =  np.cos(w[i-1]+self.angle0)
                    w_sin_mp =  np.sin(w[i+self.row_x]+self.angle0)
                    w_sin_mm =  np.sin(w[i-self.row_x]+self.angle0)
                    w_cos_mp =  np.cos(w[i+self.row_x]+self.angle0)
                    w_cos_mm =  np.cos(w[i-self.row_x]+self.angle0)  
    
                    ddu[i] =  (self.k_u*(u[i+1]+u[i-1]-2*u[i])+self.k_v*(u[i-self.row_x]+u[i+self.row_x]-2*u[i]) \
                                +u_lin*(w_cos_nm-w_cos_np)+v_lin*pow(-1,n+m)*(w_sin_mp-w_sin_mm)-du[i]*self.damp_tra)/self.mass1
                            
                            
                    ddv[i] =  (self.k_v*(v[i+1]+v[i-1]-2*v[i])+self.k_u*(v[i-self.row_x]+v[i+self.row_x]-2*v[i]) \
                               +u_lin*(w_cos_mm-w_cos_mp)+pow(-1,n+m)*v_lin*(w_sin_nm-w_sin_np)-dv[i]*self.damp_tra)/self.mass1  
                           
                           
                           
                    ddw[i] = (-self.k_s*(4*w[i]+w[i+1]+w[i-self.row_x]+w[i-1]+w[i+self.row_x]+8*(self.angle0-q0_Lin)) \
                                 -u_lin*w_sin*(u[i+1]-u[i]-self.geo_L*w_cos_np-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                 -u_lin*w_sin*(u[i]-u[i-1]-self.geo_L*w_cos-self.geo_L*w_cos_nm+2*self.geo_L*np.cos(self.angle0)) \
                                 -pow(-1,n+m-1)*v_lin*w_cos*(u[i+self.row_x]-u[i]+pow(-1,n+m)*self.geo_L*w_sin_mp+pow(-1,n+m-1)*self.geo_L*w_sin) \
                                 -pow(-1,n+m-1)*v_lin*w_cos*(u[i]-u[i-self.row_x]+pow(-1,n+m-1)*self.geo_L*w_sin+pow(-1,n+m-2)*self.geo_L*w_sin_mm) \
                                 -u_lin*w_sin*(v[i+self.row_x]-v[i]-self.geo_L*w_cos_mp-self.geo_L*w_cos+2*self.geo_L*np.cos(self.angle0)) \
                                 -u_lin*w_sin*(v[i]-v[i-self.row_x]-self.geo_L*w_cos-self.geo_L*w_cos_mm+2*self.geo_L*np.cos(self.angle0)) \
                                 +pow(-1,n+m-1)*v_lin*w_cos*(v[i+1]-v[i]-pow(-1,n+m)*self.geo_L*w_sin_np-pow(-1,n+m-1)*self.geo_L*w_sin) \
                                 +pow(-1,n+m-1)*v_lin*w_cos*(v[i]-v[i-1]-pow(-1,n+m-1)*self.geo_L*w_sin-pow(-1,n+m-2)*self.geo_L*w_sin_nm) \
                             - 4*T_slope-dw[i]*self.damp_rot)/self.r_inrt1
                                
            
     


           
      
            return(np.concatenate((du,dv,dw,ddu,ddv,ddw),axis = 0))
       
        def rk4(f, t0, tf, u0, tstep, tsave):
            frame = int(tsave/tstep)
            n = int(tf/tsave)
            t = np.linspace(t0, tf, n+1)
            
            sol = np.array((n+1)*[u0])
            iteration = int(tf/tstep)
            t_array = np.arange(t0, tf, tstep)
            sol_temp = u0
     
            # for i in range(iteration):
            #     k1 = tstep * f(sol_temp, t_array[i])
            #     k2 = tstep * f(sol_temp + 0.5 * k1, t_array[i] + 0.5*tstep)
            #     k3 = tstep * f(sol_temp + 0.5 * k2, t_array[i] + 0.5*tstep)
            #     k4 = tstep * f(sol_temp + 0.5 * k3, t_array[i] + 0.5*tstep)     
            #     sol_temp = sol_temp +(k1+2*(k2+k3) +k4)/6
                
            #     if i%frame == 0:
            #         sol[int(i/frame),:] = sol_temp
            #         if (i/frame)%(n/10) == 0: 
            #             print('Simulation process  = {:.1f}%'.format(i*100/iteration))
            # print('Runge-Kutta 4th finished calculation, importing data')
            sol = integrate.odeint(EOM,u0, t)
            return(sol,t)     
        # sol = integrate.solve_ivp(EOM, [t0, tf], u0, method = 'RK45', t_eval = t)
        #print('Runge-Kutta 4th finished calculation, importing data')
       
        sol, t = rk4(EOM, t0, tf, u0, tstep, tsave)
        disp = sol[:, 0:3*self.n_unit]
        vlct = sol[:, 3*self.n_unit:6*self.n_unit]
        
        return(sol,disp,vlct,t)
    
    
    def fft_analysis(self, disp, vlct, t):
        dt = t[1]-t[0]
        ###########2D FFT DISP_U######################
        F_u = fftpack.fft2(disp[0:self.n_unit,:])
        w = 2.*np.pi*fftpack.fftfreq(len(t),dt)
        frequency = fftpack.fftshift( w )
        k = fftpack.fftfreq(self.n_unit, self.h0_chain)
        n_wave = -2.*np.pi*fftpack.fftshift( k )
        # To put the major ticks at the middle of each cell
        dk  = n_wave[0] - n_wave[1]
#        dw  = frequency[1] - frequency[0] 
        df  = frequency[1]/(2.*np.pi) - frequency[0]/(2.*np.pi)
        extent_Dispersion=[n_wave[0]+0.5*dk,n_wave[-1]-0.5*dk,frequency[0]/(2.*np.pi)-0.5*df,frequency[-1]/(2.*np.pi)+0.5*df]
        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        Fshift_u = fftpack.fftshift( F_u )
        # Calculate a 2D power spectrum
        psd2D = abs( Fshift_u )**2
        Log_psd2D_u = 10*np.log10( psd2D )
        
        
        
        # 2D FFT disp_v -----------------------------------------------
        F_v = fftpack.fft2(disp[self.n_unit:2*self.n_unit,:])
        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        Fshift_v = fftpack.fftshift( F_v )
        # Calculate a 2D power spectrum
        psd2D = abs( Fshift_v )**2
        Log_psd2D_v = 10*np.log10( psd2D )
        
        # 2D FFT disp_w -----------------------------------------------
        F_w = fftpack.fft2(disp[2*self.n_unit::,:])
        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        Fshift_w = fftpack.fftshift( F_w )
        # Calculate a 2D power spectrum
        psd2D = abs( Fshift_w )**2
        Log_psd2D_w = 10*np.log10( psd2D )
        
        # 2D FFT vlct_u -----------------------------------------------
        F_v = fftpack.fft2(vlct[0:self.n_unit,:])
        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        Fshift_v = fftpack.fftshift( F_v )
        # Calculate a 2D power spectrum
        psd2D = abs( Fshift_v )**2
        Log_psd2D_du = 10*np.log10( psd2D )
        
        # 2D FFT vlct_v -----------------------------------------------
        F_v = fftpack.fft2(vlct[self.n_unit:2*self.n_unit,:])
        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        Fshift_v = fftpack.fftshift( F_v )
        # Calculate a 2D power spectrum
        psd2D = abs( Fshift_v )**2
        Log_psd2D_dv = 10*np.log10( psd2D )
        
        # 2D FFT (Angle change) -----------------------------------------------
        F_w = fftpack.fft2(vlct[2*self.n_unit::,:])
        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        Fshift_w = fftpack.fftshift( F_w )
        # Calculate a 2D power spectrum
        psd2D = abs( Fshift_w )**2
        Log_psd2D_dw = 10*np.log10( psd2D )
        
        psd2D_data = {'n_wave':n_wave, 'extent_Dispersion': extent_Dispersion,
                      'Log_psd2D_disu': Log_psd2D_u, 'Log_psd2D_disv': Log_psd2D_v, 
                      'Log_psd2D_w': Log_psd2D_w, 'Log_psd2D_vlctu': Log_psd2D_du, 
                      'Log_psd2D_vlctv': Log_psd2D_dv, 
                      'Log_psd2D_vlctw': Log_psd2D_dw}
        
            
        
        return(psd2D_data, Log_psd2D_u, Log_psd2D_v, Log_psd2D_w, Log_psd2D_du,Log_psd2D_dv,Log_psd2D_dw  )
    
    def energy_analysis(self,vlct,disp, t):
        
        #======================================================================
        # Energy analysis
        #======================================================================
        # Kinetic energy
        vlct_u = vlct[:,0:self.n_unit]
        vlct_v = vlct[:,self.n_unit:self.n_unit*2]
        vlct_w = vlct[:,self.n_unit*2:self.n_unit*3]
        energy_KineTrans = np.zeros((len(t),self.n_unit))
        energy_KineRotat = np.zeros((len(t),self.n_unit))
        
        #potential energy
        disp_u = disp[:,0:self.n_unit]
        disp_v = disp[:,self.n_unit:self.n_unit*2]
        disp_w = disp[:,self.n_unit*2:self.n_unit*3]
        energy_PotTrans = np.zeros((len(t),self.n_unit))
        energy_PotRotat = np.zeros((len(t),self.n_unit))
        for i in range(self.n_unit):
            # energy_Kinet = 0.5*self.v_mass*(vlct_dis**2) + 0.5*self.r_inrt*(vlct_ang**2)
            energy_KineTrans[:,i] = 0.5*self.mass1*(vlct_u[:,i]**2+vlct_v[:,i]**2)
            energy_KineRotat[:,i] = 0.5*self.r_inrt1*vlct_w[:,i]**2
            energy_PotTrans[:,i] = 0.5*self.k_v*(disp_u[:,i]**2+disp_v[:,i]**2)
            energy_PotRotat[:,i] = 0.5*self.k_s*disp_w[:,i]**2
        energy_sum = energy_KineTrans + energy_KineRotat
       
        return(energy_sum)
    
    
    def driver(self, u0, t0, tf, tstep, tsave, fig_flag = True):
        
        
        sol,disp,vlct, t = self.dynamic_solver(u0, t0, tf, tstep, tsave)
        
        psd2D_data, Log_psd2D_u, Log_psd2D_v, Log_psd2D_w, Log_psd2D_du,Log_psd2D_dv, \
            Log_psd2D_dw = self.fft_analysis(disp, vlct, t)
        
        energy_sum = self.energy_analysis(vlct,disp, t)
    
        if fig_flag == True:
            ##############disp and vlct u##################
            plt.figure('Horizontal direction data')
            plt.subplot(121)
            for i in range(0,row_x):
                plt.plot(t*1000,disp[:,i]*1000, color='red',alpha=0.9-i/row_x, label = "$u_{}$".format(i))

            plt.xlabel('Time [ms]')
            plt.ylabel('Displacement [mm]')
            #plt.legend()
            plt.tight_layout()
            plt.subplot(122)
            for i in range(0,row_x):
                plt.plot(t*1000,vlct[:,i], color='blue',alpha=0.9-i/row_x, label = "$v_{}$".format(i))

            plt.xlabel('Time [ms]')
            plt.ylabel('Velocity [m/s]')
            #plt.legend()
            plt.tight_layout()
            ###################disp and vlct v##################
            plt.figure('Vertical direction data')
            plt.subplot(121)
            for i in range(0,row_x):
                plt.plot(t*1000,disp[:,i+self.n_unit]*1000, color='red',alpha=0.9-i/row_x, label = "$u_{}$".format(i))

            plt.xlabel('Time [ms]')
            plt.ylabel('Displacement [mm]')
            #plt.legend()
            
            plt.subplot(122)
            for i in range(0,row_x):
                plt.plot(t*1000,vlct[:,i+self.n_unit], color='blue',alpha=0.9-i/row_x, label = "$v_{}$".format(i))

            plt.xlabel('Time [ms]')
            plt.ylabel('Velocity [m/s]')
            #plt.legend()
            plt.tight_layout()
            ###################rot and vlct w##################
            plt.figure('Rotation')
            plt.subplot(121)
            for i in range(0,row_x):
                plt.plot(t*1000,np.degrees(disp[:,i+2*self.n_unit]), color='red',alpha=0.1+i/row_x, label = "$\u03B8 _{}$".format(i))

            plt.xlabel('Time [ms]')
            plt.ylabel('Displacement [deg]')
            #plt.legend()
           
            plt.subplot(122)
            for i in range(0,row_x):
                plt.plot(t*1000,np.degrees(vlct[:,i+2*self.n_unit]), color='blue',alpha=0.1+i/row_x, label = "$\omega_{}$".format(i))

            plt.xlabel('Time [ms]')
            plt.ylabel('Velocity [deg/s]')
            #plt.legend()
            plt.tight_layout()
            
            
            #########################2D contour plots for disp and vlc###############
            #############Looking at first row of rotating squares##################
 
            # disp ={'disp_u':disp_u, 'disp_v':disp_v, 'disp_rot':disp_w }
            # vlct ={'vlct_u':vlct_u, 'vlct_v':vlct_v, 'vlct_rot':vlct_w }
            ###########################contour plots for disp and vlct data#################
            fig = plt.figure('Wave propogation 2D cell displacement horizontal')
            ax = fig.add_subplot(1,2,1)
            cmap = plt.get_cmap('jet')
            sm = ax.imshow(disp[:,0:self.n_unit], aspect='auto', origin = 'lower', interpolation ='spline16', cmap =cmap,
                         vmin=np.min(disp[:,0:self.n_unit]), vmax=np.max(disp[:,0:self.n_unit]), 
                         extent=[0,self.n_unit,0,tf*1000])
            ax.set_title('Displacement u')
            ax.set_ylabel('Time [ms]')
            ax.set_xlabel('Square index')
            sm = mpl.cm.ScalarMappable(cmap=cmap)
            norm = mpl.colors.Normalize(vmin=np.min(disp[:,0:self.n_unit]), 
                                         vmax=np.max(disp[:,0:self.n_unit]))
            cbar = fig.colorbar(sm,cmap=cmap, norm = norm, orientation='horizontal')
            cbar.set_label('Normalized displacement')

            ax1 = fig.add_subplot(1,2,2)
            cmap = plt.get_cmap('jet')
            sm1 = ax1.imshow(vlct[:,0:self.n_unit], aspect='auto', origin = 'lower', interpolation ='spline16', cmap =cmap, 
                         vmin=np.min(vlct[:,0:self.n_unit]), 
                         vmax=np.max(vlct[:,0:self.n_unit]), 
                         extent=[0, self.n_unit, 0,tf*1000])
            ax1.set_title('Velocity u')
            ax1.set_ylabel('Time [ms]')
            ax1.set_xlabel('Square index')
            sm = mpl.cm.ScalarMappable(cmap=cmap)
            norm1 = mpl.colors.Normalize(vmin=np.min(vlct[:,0:self.n_unit]), 
                                          vmax=np.max(vlct[:,0:self.n_unit]))
            cbar1 = fig.colorbar(sm,cmap=cmap, norm = norm1, orientation='horizontal')
            cbar1.set_label('Normalized velocity')
            plt.tight_layout()
            
            
            
            fig1 = plt.figure('Wave propogation 2D cell displacement vertical')
            ax2 = fig1.add_subplot(1,2,1)
            cmap = plt.get_cmap('jet')
            sm2 = ax2.imshow(disp[:,self.n_unit:2*self.n_unit], aspect='auto', 
                              origin = 'lower', interpolation ='spline16', cmap =cmap,
                         vmin=np.min(disp[:,self.n_unit:2*self.n_unit]), 
                         vmax=np.max(disp[:,self.n_unit:2*self.n_unit]), extent=[0,self.n_unit, 0,tf*1000])
            ax2.set_title('Displacement v')
            ax2.set_ylabel('Time [ms]')
            ax2.set_xlabel('Square index')
            sm = mpl.cm.ScalarMappable(cmap=cmap)
            norm2 = mpl.colors.Normalize( vmin=np.min(disp[:,self.n_unit:2*self.n_unit]), 
                                          vmax=np.max(disp[:,self.n_unit:2*self.n_unit]))
            cbar2 = fig1.colorbar(sm,cmap=cmap, norm = norm2, orientation='horizontal')
            cbar2.set_label('Normalized displacement')

            ax3 = fig1.add_subplot(1,2,2)
            cmap = plt.get_cmap('jet')
            sm3 = ax3.imshow(vlct[:,self.n_unit:2*self.n_unit], aspect='auto', origin = 'lower', interpolation ='spline16', cmap =cmap, 
                         vmin=np.min(vlct[:,self.n_unit:2*self.n_unit]), 
                         vmax=np.max(vlct[:,self.n_unit:2*self.n_unit]), 
                         extent=[0,self.n_unit, 0,tf*1000])
            ax3.set_title('Velocity v')
            ax3.set_ylabel('Time [ms]')
            ax3.set_xlabel('Square index')
            sm = mpl.cm.ScalarMappable(cmap=cmap)
            norm3 = mpl.colors.Normalize( vmin=np.min(vlct[:,self.n_unit:2*self.n_unit]), 
                                          vmax=np.max(vlct[:,self.n_unit:2*self.n_unit]))
            cbar3 = fig1.colorbar(sm,cmap=cmap, norm = norm3, orientation='horizontal')
            cbar3.set_label('Normalized velocity')
            plt.tight_layout()
            
            
            
             ###########rotation ##############3

            fig2 = plt.figure('Wave propogation 2D cell rotation')
            ax4 = fig2.add_subplot(1,2,1)
            cmap = plt.get_cmap('jet')
            sm4 = ax4.imshow(disp[:,2*self.n_unit:3*self.n_unit], aspect='auto', origin = 'lower', interpolation ='spline16', cmap =cmap, 
                         vmin=np.min(disp[:,2*self.n_unit:3*self.n_unit]), 
                         vmax = np.max(disp[:,2*self.n_unit:3*self.n_unit]),
                         extent=[0,self.n_unit, 0,tf*1000])
            ax4.set_title('Rotation $\u03B8 _1$')            
            ax4.set_ylabel('Time [ms]')
            ax4.set_xlabel('Square index')
            sm = mpl.cm.ScalarMappable(cmap=cmap)
            norm4 = mpl.colors.Normalize(vmin=np.min(disp[:,2*self.n_unit:3*self.n_unit]), 
                                          vmax = np.max(disp[:,2*self.n_unit:3*self.n_unit]))
            cbar4 = fig2.colorbar(sm,cmap=cmap, norm= norm4, orientation='horizontal')
            cbar4.set_label('Normalized rotation')


            ax5 = fig2.add_subplot(1,2,2)
            cmap = plt.get_cmap('jet')
            sm5 = ax5.imshow(vlct[:,2*self.n_unit:], aspect='auto', origin = 'lower', interpolation ='spline16', cmap =cmap, 
                         vmin=np.min(vlct[:,2*self.n_unit:]), 
                         vmax = np.max(vlct[:,2*self.n_unit:]),
                         extent=[0,self.n_unit, 0,tf*1000])
            ax5.set_title('Angular velocity $\omega$')              
            ax5.set_ylabel('Time [ms]')
            ax5.set_xlabel('Square index')
            # sm = mpl.cm.ScalarMappable(cmap=cmap)
            norm5 = mpl.colors.Normalize(vmin=np.min(vlct[:,2*self.n_unit:]), 
                                          vmax = np.max(vlct[:,2*self.n_unit:]))
            cbar5 = fig2.colorbar(sm,cmap=cmap, norm = norm5, orientation='horizontal')
            cbar5.set_label('Normalized angular velocity')
            plt.tight_layout()
            #############create energy plot##################
            plt.figure('Energy')
            #plt.plot(t*1000, np.sum(energy_KineTrans,1), 'b', alpha= 0.7, label = 'Trans')
            plt.plot(t*1000, np.sum(energy_sum,1), 'r', alpha= 0.7, label = 'Rot')
            plt.xlabel('Time [ms]')
            plt.ylabel('Energy $U$')
            #plt.legend()
            plt.tight_layout()
            
            #############plot fft#############
           #  fig3 = plt.figure('FFT 2D cell displacement horizontal')
           #  ax11 = fig3.add_subplot(1,2,1)
           #  cmap = plt.get_cmap('jet')
           #  sm = ax11.imshow(Log_psd2D_u, aspect='auto', origin = 'lower', interpolation ='none', cmap =cmap,
           #              vmin=np.min(Log_psd2D_u), vmax=np.max(Log_psd2D_u), 
           #              extent=[0,self.n_unit,0,tf*1000])
           #  ax11.set_title('Displacement u')
           #  ax11.set_ylabel('Time [ms]')
           #  ax11.set_xlabel('Square index')
           #  sm = mpl.cm.ScalarMappable(cmap=cmap)
           #  norm11 = mpl.colors.Normalize(vmin=np.min(Log_psd2D_u), 
           #                              vmax=np.max(Log_psd2D_u))
           #  cbar11 = fig3.colorbar(sm,cmap=cmap, norm = norm11, orientation='horizontal')
           #  cbar11.set_label('Normalized displacement')

           #  ax22 = fig3.add_subplot(1,2,2)
           #  cmap = plt.get_cmap('jet')
           #  sm1 = ax22.imshow(Log_psd2D_du, aspect='auto', origin = 'lower', interpolation ='none', cmap =cmap, 
           #              vmin=np.min(Log_psd2D_du), 
           #              vmax=np.max(Log_psd2D_du), 
           #              extent=[0, self.n_unit, 0,tf*1000])
           #  ax22.set_title('Velocity u')
           #  ax22.set_ylabel('Time [ms]')
           #  ax22.set_xlabel('Square index')
           #  sm = mpl.cm.ScalarMappable(cmap=cmap)
           #  norm22 = mpl.colors.Normalize(vmin=np.min(Log_psd2D_du), 
           #                               vmax=np.max(Log_psd2D_du))
           #  cbar22 = fig3.colorbar(sm,cmap=cmap, norm = norm22, orientation='horizontal')
           #  cbar22.set_label('Normalized velocity ')
           #  plt.tight_layout()
            
            
            
           #  fig4 = plt.figure('FFT 2D cell displacement vertical')
           #  ax33 = fig4.add_subplot(1,2,1)
           #  cmap = plt.get_cmap('jet')
           #  sm2 = ax33.imshow(Log_psd2D_v, aspect='auto', 
           #                   origin = 'lower', interpolation ='none', cmap =cmap,
           #              vmin=np.min(Log_psd2D_v), 
           #              vmax=np.max(Log_psd2D_v), extent=[0,self.n_unit, 0,tf*1000])
           #  ax33.set_title('Displacement v')
           #  ax33.set_ylabel('Time [ms]')
           #  ax33.set_xlabel('Square index')
           #  sm = mpl.cm.ScalarMappable(cmap=cmap)
           #  norm33 = mpl.colors.Normalize( vmin=np.min(Log_psd2D_v), 
           #                               vmax=np.max(Log_psd2D_v))
           #  cbar33 = fig4.colorbar(sm,cmap=cmap, norm = norm33, orientation='horizontal')
           #  cbar33.set_label('Normalized displacement')

           #  ax44 = fig4.add_subplot(1,2,2)
           #  cmap = plt.get_cmap('jet')
           #  sm3 = ax44.imshow(Log_psd2D_dv, aspect='auto', origin = 'lower', interpolation ='none', cmap =cmap, 
           #              vmin=np.min(Log_psd2D_dv), 
           #              vmax=np.max(Log_psd2D_dv), 
           #              extent=[0,self.n_unit, 0,tf*1000])
           #  ax44.set_title('Velocity v')
           #  ax44.set_ylabel('Time [ms]')
           #  ax44.set_xlabel('Square index')
           #  sm = mpl.cm.ScalarMappable(cmap=cmap)
           #  norm44 = mpl.colors.Normalize( vmin=np.min(Log_psd2D_dv), 
           #                               vmax=np.max(Log_psd2D_dv))
           #  cbar44 = fig4.colorbar(sm,cmap=cmap, norm = norm44, orientation='horizontal')
           #  cbar44.set_label('Normalized velocity')
           #  plt.tight_layout()
            
            
            
           #  ###########rotation ##############3

           #  fig5 = plt.figure('FFT 2D cell rotation')
           #  ax55 = fig5.add_subplot(1,2,1)
           #  cmap = plt.get_cmap('jet')
           #  sm4 = ax55.imshow(Log_psd2D_w, aspect='auto', origin = 'lower', interpolation ='none', cmap =cmap, 
           #              vmin=np.min(Log_psd2D_w), 
           #              vmax = np.max(Log_psd2D_w),
           #              extent=[0,self.n_unit, 0,tf*1000])
           #  ax55.set_title('Rotation $\u03B8 _1$')            
           #  ax55.set_ylabel('Time [ms]')
           #  ax55.set_xlabel('Square index')
           #  sm = mpl.cm.ScalarMappable(cmap=cmap)
           #  norm55 = mpl.colors.Normalize(vmin=np.min(Log_psd2D_w), 
           #                               vmax = np.max(Log_psd2D_w))
           #  cbar55 = fig5.colorbar(sm,cmap=cmap, norm= norm55, orientation='horizontal')
           #  cbar55.set_label('Normalized rotation')


           #  ax66 = fig5.add_subplot(1,2,2)
           #  cmap = plt.get_cmap('jet')
           #  sm66 = ax66.imshow(Log_psd2D_dw, aspect='auto', origin = 'lower', interpolation ='none', cmap =cmap, 
           #              vmin=np.min(Log_psd2D_dw), 
           #              vmax = np.max(Log_psd2D_dw),
           #              extent=[0,self.n_unit, 0,tf*1000])
           #  ax66.set_title('Angular velocity $\omega$')              
           #  ax66.set_ylabel('Time [ms]')
           #  ax66.set_xlabel('Square index')
           # # sm = mpl.cm.ScalarMappable(cmap=cmap)
           #  norm66 = mpl.colors.Normalize(vmin=np.min(Log_psd2D_dw), 
           #                               vmax = np.max(Log_psd2D_dw))
           #  cbar66 = fig5.colorbar(sm,cmap=cmap, norm = norm66, orientation='horizontal')
           #  cbar66.set_label('Normalized angular velocity')
           #  plt.tight_layout()
        return(sol,t,energy_sum)
    
    
    
  
if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    #==============================================================================
    # Geometry
    #==============================================================================
    L_side = 12.*1e-3  #[m] Side length of the cube
    H_cube = 10.*1e-3  # [m] Height of the cube
   
    # Initial configuration
    q0_Lin  = np.radians(0)
    # Configuration of the 2D map
    row_x = 2
    row_y = 2
    cells = row_x*row_y

    #==============================================================================
    # Material propeties
    chain_config = 'mono_L'
    #==============================================================================
    # Spring constant for linear spring --------------
    # Config1
    k_Lin1 = 670.8
    k_Rot1 = 0.00044325837185769376  # for t_hinge=0.75*1e-3 [m]
    # Config2
    k_Lin2 = k_Lin1
    k_Rot2 = k_Rot1
    # Parameters for the Morse potential function -----
    A_const  = 0. #0.0005477163091763655
    alpha    = 4.966064409064443
    r1_morse = 0.74422634 # for t_hinge=0.75*1e-3 [m]
    
    damp_tra = 0
    damp_rot = 0
    # damp_tra = 3.00097565e-8#(Measured)
    # damp_rot = 136.66992188e-8 #(Measured)
    # # Material property for the disk
    # weight = 1.9888*1e-3 #[kg] including joints, screws, and bearing
    # r_inrt = 46.608*1e-9 #[kgm^2]
    # Material property -----------------------
    # mass1 = 1.9888*1e-3 #[kg] (Measured)
    # r_inrt1 = 46.608*1e-9 #[kgm^2] (Measured)
    k_Linh = k_Lin1
    k_Linv = k_Lin1
    # Config 1
    dens1 = 1400. #[kg/m^3]
    mass1 = dens1*L_side*L_side*H_cube
    r_inrt1 = (1./12.)*mass1*L_side*L_side
    # Config 2
    dens2 = 2.*dens1 #[kg/m^3]
    mass2 = dens2*L_side*L_side*H_cube
    r_inrt2 = (1./12.)*mass2*L_side*L_side
   
   
    # numerical solver parameters
    t0 = 0
    tf = 0.1
    tstep =1e-5
    tsave = 1e-5
    u0 =np.zeros(row_x*row_y*6)
    u0[2*cells:3*cells] =  q0_Lin #u,v,w, du, dv, dw[m,m/s]
    u0[3*cells] = 1

    ##############################excecution########################################3
    print('Dynamic simulation for {:d} by {:d} rotating square sample:'.format(row_x, row_y))
    rotsq_dym = RotSq_Dynamics_unit(L_side, q0_Lin, chain_config, k_Linh, k_Linv, k_Rot1,
                 A_const, alpha, r1_morse, cells, damp_tra, damp_rot, mass1, 
                                      r_inrt1, row_x, row_y)
    tic = time.perf_counter()
    print("Simulation starting:")
    sol,t,energy_sum= rotsq_dym.driver(u0, t0, tf, tstep, tsave)
    
    t_elapse = time.perf_counter()-tic
    
    print("Simulation elapse time : {:.4f}s".format(t_elapse))
    
    print("Start FFT and energy analysis:")
    