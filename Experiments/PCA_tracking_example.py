import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from numpy import linalg
from scipy import optimize
from scipy.integrate import ode
from scipy import integrate
from matplotlib import animation
from ctypes import (CDLL, POINTER, ARRAY, c_void_p,
                    c_int, byref,c_double, c_char,
                    c_char_p, create_string_buffer)
from numpy.ctypeslib import ndpointer
import os
import shutil   #High level file operation
import glob     #Get file list by using wildcard
import cv2
from scipy import fftpack
from scipy import signal
import cmath
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.colorbar import Colorbar
# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 24        # Original fornt size is 8, multipy by above number
#plt.rcParams['text.usetex']     = True
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
#plt.rcParams['text.latex.preamble'] = '\usepackage{sfmath}'
plt.rcParams['lines.linewidth'] = 3.   
plt.rcParams['lines.markersize'] = 8. 
plt.rcParams['legend.fontsize'] = 21        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 24        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in' 
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300




class MotionTrack_RectangleDetect:
    def __init__(self, fname_video, dir_cwd, fname_save, area_min, area_max, thrsh_min, thrsh_max):
        self.dir_cwd    = dir_cwd
        self.fname_save = fname_save
        # Display window
        cv2.namedWindow("motion")
        # Callback of mouse event
#        cv2.setMouseCallback("motion", self.onMouse)
        # video
        self.video = cv2.VideoCapture(fname_video)
        # FPS
        self.fps  = self.video.get(5)
        # Inverval (1000/frame rate)
        self.INTERVAL_Play = 30
        self.INTERVAL = self.INTERVAL_Play
        # Current frame (color)
        self.frame = None
        # Current frame (gray)
        self.gray_next = None
        # Previous frame (gray)
        self.gray_prev = None
        # Features
        self.features = None
        # Status of features
        self.status = None
        # Cooridante of features
        self.position = []
        # Time
        self.time = []
        # "esc" key
        self.ESC_KEY = 0x1b
        # "s" key
        self.S_KEY = 0x73
        # "r" key
        self.R_KEY = 0x72
        # Maximum number of features
        self.MAX_FEATURE_NUM = 500
        # Terminate criteria for Loop argorithm
        self.CRITERIA = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03)
        
        # Video data
        #VIDEO_DATA = 'Experiment.mov'
        
        # Size factor for display
        self.dis_fac = 2.
        # Font in the image
        self.font = cv2.FONT_HERSHEY_PLAIN
        self.font_size = 1.4
        
        # Rhombus marker detect ---------
        self.area_min   = area_min
        self.area_max   = area_max
        self.thrsh_min  = thrsh_min
        self.thrsh_max  = thrsh_max
        self.roi_radius = 400
#        # Blob detect ---------------------------------------------------------
#        self.mrk_radius = 5
#        self.params = cv2.SimpleBlobDetector_Params()
#        self.params.filterByArea = True
#        self.params.minArea = np.pi*(0.4*self.mrk_radius)**2
#        self.params.maxArea = np.pi*(1.2*self.mrk_radius)**2
#        # Filter by Circularity
#        self.params.filterByCircularity = True
#        self.params.minCircularity = 0.5
#        # Filter by Convexity
#        self.params.filterByConvexity = True
#        self.params.minConvexity = 0.77
#        # Filter by Inertia
#        self.params.filterByInertia = True
#        self.params.minInertiaRatio = 0.5
    
    def Resize_image(self, frame):
        height = frame.shape[0]
        width  = frame.shape[1]
        frame_rev = cv2.resize(frame, (int(width/self.dis_fac), int(height/self.dis_fac)) )
        return(frame_rev)

    # Main Loop
    def DIC_tracking(self, import_feature=False):
        # Read 1st frame
        end_flag, self.frame = self.video.read()
#        # Resize the image for display
#        height = self.frame.shape[0]
#        width = self.frame.shape[1]
#        self.frame = cv2.resize(self.frame, (int(width/self.dis_fac), int(height/self.dis_fac)) )
        # Save analyzed movie
        fourcc   = cv2.VideoWriter_fourcc(*"MJPG") 
        self.out = cv2.VideoWriter('DIC_measurement.avi',fourcc, int(self.fps),
                                   (int(self.frame.shape[1]/self.dis_fac), int(self.frame.shape[0]/self.dis_fac)) )
        # Convert to gray scale
        self.gray_prev = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        self.gray_next = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        
        # Analyze the first frame ---------------------------------------------
        # Detect rhombus marker
        mrk_center,mrk_angle,mrk_area,gray = self.Detect_RhombusMarker()
        n_mrk = len(mrk_center)
        print(mrk_center)
        print('Total number of features = {}'.format(n_mrk))
        mrk_prev  = mrk_center
        mrkR_prev = mrk_angle
#        mrk_prev = 
#        mrk_area = np.array(mrk_area)
        print('Area of each marker')
        print(mrk_area)
        print('Average area of the detected markers = {}'.format(np.mean(mrk_area)))
        # Display
        # Resize the image for display
        frame_rev = self.Resize_image(self.frame)
        gray_rev = self.Resize_image(gray)
        cv2.putText(frame_rev,'Press "esc" to close the window',(0, 40), self.font, self.font_size,(255,0,0))
        cv2.imshow('output', frame_rev)
        cv2.imshow('Gray scale', gray_rev)
        cv2.waitKey(0)
        # End process
        cv2.destroyAllWindows()
        
        mrk_position = []
        mrk_rotation = []
        time_video   = []
        while end_flag: # Start while -------------------------------------------
            end_flag, self.frame = self.video.read()
            if( self.frame is None ):
                break
            # Detect rhombus marker
            mrk_center,mrk_angle,mrk_area,gray = self.Detect_RhombusMarker()
            mrk_next  = mrk_center
            mrkR_next = mrk_angle
#            print(mrk_center)
            mrk_prev,mrkR_prev = self.MarkerPosition(self.frame, mrk_prev, mrk_next, mrkR_prev, mrkR_next, n_mrk)
            mrk_position.append(mrk_prev)
            mrk_rotation.append(mrkR_prev)
            # Display ------------------------------
            # Resize the image for display
            frame_rev = self.Resize_image(self.frame)
            # Put text in window (Camera 1)
            n_frame = self.video.get(1)
            # Save time
            time_video.append(n_frame/self.fps)
            text1 = 'Frame rate   = %d'%(int(round(self.fps)))
            text2 = 'Current frame = %d'%(n_frame)
#            text2 = 'Current time = %1.3f [sec]'%((n1_frame-n0_frame1)/self.fps)
#                text3 = 'Press any key to end this program'
            cv2.putText(frame_rev,text1,(40,40), self.font, self.font_size,(255,0,0))
            cv2.putText(frame_rev,text2,(40,80), self.font, self.font_size,(255,0,0))
            
            cv2.imshow("motion", frame_rev)
            self.out.write(frame_rev)
            # Prepare for next Loop
            self.gray_prev = self.gray_next
#            end_flag, self.frame = self.video.read()
            
            # Interval
            key = cv2.waitKey(self.INTERVAL)
            # End the process if "esc" is pressed
            if key == self.ESC_KEY:
                break
            # Pose if "s" is pressed
            elif key == self.S_KEY:
                self.INTERVAL = 0
            elif key == self.R_KEY:
                self.INTERVAL = self.INTERVAL_play
        # End while --------------------------------------------------------
        time_video   = np.array(time_video)
        mrk_position = np.array(mrk_position)
        mrk_rotation = np.array(mrk_rotation)
        # End process
        cv2.destroyAllWindows()
        self.video.release()
        self.out.release()
        
        # Save the data ----------------------------------------------------
        # Delete previous data
        if os.path.exists(self.dir_cwd+'/'+self.fname_save):
            shutil.rmtree(self.dir_cwd+'/'+self.fname_save)
        os.makedirs(self.dir_cwd+'/'+self.fname_save)
        
        
        Rotation = np.zeros((len(time_video), n_mrk))
        for i_mrk in range(n_mrk):
            Rotation[:,i_mrk] = np.degrees(mrk_rotation[:,i_mrk]-mrk_rotation[0,i_mrk])
        
        with open('{}/DIC_measurement_RawData.csv'.format(self.fname_save), 'w') as f:
            f.write("Time, x (pixel), y (pixel), w (deg)\n")
            for i_t in range(len(time_video)):
                f.write("{0}".format(time_video[i_t]))
                for i_mrk in range(n_mrk):
                    f.write(", {0}, {1}, {2} ".format(mrk_position[i_t,i_mrk,0], 
                                                mrk_position[i_t,i_mrk,1],
                                                Rotation[i_t,i_mrk]))
                f.write("\n")
        
        plt.figure('Trajectory')
        for i_mrk in range(n_mrk):
            plt.plot(mrk_position[:,i_mrk,0],mrk_position[:,i_mrk,1])
        
        

        plt.figure('Angle change')
        for i_mrk in range(0,n_mrk,4):
           
            plt.plot(time_video, np.degrees(mrk_rotation[:,i_mrk]-mrk_rotation[0,i_mrk]))
        plt.xlabel('Time (sec)')
        plt.ylabel('Rotational angle (deg)')
        return(mrk_rotation, Rotation)
    
    def MarkerPosition(self, frame, mrk_prev, mrk_next, mrkR_prev, mrkR_next, n_mrk, flag_arrow=False):
        # Analyze previous and current frame
        n_mrk1  = np.size(mrk_next,0)
        pos_wrk = []
        rot_wrk = []
        # Calculate distance matrix
#        temp = distance.cdist(mrk_prev, mrk_next)
        i3 = 0
        amplitude = []
        for i in range(n_mrk):
            dist = np.zeros((2,n_mrk1))
            for j in range(n_mrk1):
                dist[:,j] = mrk_prev[i,0:2] - mrk_next[j,0:2]
            Tdist = np.sqrt(dist[0,:]**2 + dist[1,:]**2)
            i_min = np.argmin(Tdist)
            if Tdist[i_min] < self.roi_radius:
                # Renew the value
                mrk_prev[i,0:2] = mrk_next[i_min,0:2]
                # Check the rotation angle
                def_angle = mrkR_prev[i] - mrkR_next[i_min]
                if abs(def_angle) > 0.8*np.pi:
                    mrkR_prev[i] = mrkR_next[i_min] + def_angle/abs(def_angle)*np.pi
                else:
                    mrkR_prev[i] = mrkR_next[i_min]
                # Add arros in the movie
                amplitude.append(Tdist[i_min])
                if np.mod(i,3)==1:
                    amplitude = max(amplitude)
#                    if flag_arrow==True:
#                        if amplitude>0.5:
#                            dirct = np.sign(dist[1,i_min])
#                            self.drow_arrow(frame, (int(mrk_next[0,3*i3]),  int(mrk_next[1,3*i3])), Tdist[i_min],dirct)
#                            self.drow_arrow(frame, (int(mrk_next[0,3*i3+1]),int(mrk_next[1,3*i3+1])), Tdist[i_min],dirct)
#                            self.drow_arrow(frame, (int(mrk_next[0,3*i3+2]),int(mrk_next[1,3*i3+2])), Tdist[i_min],dirct)
                    i3 = i3 + 1
                    amplitude = []
            else:
                print('!!!!!!!!!!!!!!!!! Lost a marker !!!!!!!!!!!!!!!!!')
#            print mrk_prev[:,i]
            cv2.circle(frame,(int(mrk_prev[i,0]), int(mrk_prev[i,1])), 2,(0,0,255),2)
            # Analyze the position of the features
            pos_wrk.append([mrk_prev[i,0],mrk_prev[i,1]])
            rot_wrk.append(mrkR_prev[i])
        pos_wrk = np.array(pos_wrk)
        rot_wrk = np.array(rot_wrk)
        return(pos_wrk,rot_wrk)
    
    

    def Detect_RhombusMarker(self):
        # Convert the image to gray scale
        gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.medianBlur(gray,5)
        # Binary
#        retval, bw = cv2.threshold(gray, self.thrsh_min, self.thrsh_max, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        retval, bw = cv2.threshold(gray, self.thrsh_min, self.thrsh_max, cv2.THRESH_BINARY)
#        bw = gray
        
        # Extract the boundaries
        #   contours : [Area][Point No][0][x=0, y=1]
        #   cv2.CHAIN_APPROX_NONE: All boundary points are stored
        #   cv2.CHAIN_APPROX_SIMPLE: Store only the corner points
        img = self.frame
        contours, hierarchy = cv2.findContours(bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    
        
        # Detect rhombus markers
        mrk_center = []
        mrk_angle  = []
        mrk_area   = []
        for i, cnt in enumerate(contours):
            # Calculate the area
            area = cv2.contourArea(contours[i])
            # Exclude noise (too small regions) and too big areas
            if area < self.area_min or area > self.area_max:
                continue
            # Select only rectable shape -------------------
            # Calculate the length of the outer edges
            arclen = cv2.arcLength(cnt, True)
            approx_cnt = cv2.approxPolyDP(cnt, epsilon=0.01* arclen, closed=True)
#            print(len(approx_cnt))
#            cv2.drawContours(self.frame, [approx_cnt], -1, (0, 0, area), 2)
#            if len(approx_cnt) > 6:
#                continue
            mrk_area.append(area)
            # Draw boundaries
            cv2.drawContours(self.frame, contours, i, (0, 0, 255), 2, 8, hierarchy, 0)
            # Store the boundary data
            X = np.array(contours[i], dtype=np.float).reshape((contours[i].shape[0], contours[i].shape[2]))
            # PCA (1D)
            mean, eigenvectors = cv2.PCACompute(X, mean=np.array([], dtype=np.float), maxComponents=1)
            # Draw principal axis
            mrk_center.append([mean[0][0], mean[0][1]])
            vec = (eigenvectors[0][0], eigenvectors[0][1])
            angle = self.drawAxis(self.frame, mrk_center[-1], vec, (255, 255, 0), 150)
            mrk_angle.append(angle)
        return(np.array(mrk_center),np.array(mrk_angle),mrk_area,bw)
    
    


    # Draw vector
    def drawAxis(self, img, start_pt, vec, colour, length):
        # Anti-aliasing
        CV_AA = 16
        # End point
        end_pt  = (int(start_pt[0] + length * vec[0]), int(start_pt[1] + length * vec[1]))
        end_pt2 = (int(start_pt[0] - length * vec[0]), int(start_pt[1] - length * vec[1]))
        # Draw center point
        cv2.circle(img, (int(start_pt[0]), int(start_pt[1])), 5, colour, 1)
    
        # Draw axis
#        cv2.line(img, (int(start_pt[0]), int(start_pt[1])), end_pt, colour, 1, CV_AA);
        cv2.line(img, end_pt2, end_pt, colour, 1, CV_AA);
        
        # Draw arrow
        angle = np.arctan2(vec[1], vec[0])
#        print(np.degrees(angle))
#    
#        qx0 = int(end_pt[0] - 9 * np.cos(angle + np.pi / 4))
#        qy0 = int(end_pt[1] - 9 * np.sin(angle + np.pi / 4))
#        cv2.line(img, end_pt, (qx0, qy0), colour, 1, CV_AA)
#    
#        qx1 = int(end_pt[0] - 9 * np.cos(angle - np.pi / 4))
#        qy1 = int(end_pt[1] - 9 * np.sin(angle - np.pi / 4))
#        cv2.line(img, end_pt, (qx1, qy1), colour, 1, CV_AA)
        return(angle)

    def contourArea(self, mrk_position, t_range, fps, Rotation):
        tf = np.shape(mrk_position)[0]/fps
        cell_max = np.shape(mrk_position)[1]
        plt.figure('Contour map')
        cmap = plt.get_cmap('jet')
        contour = plt.imshow(mrk_position[:,1::8], aspect='auto', origin = 'lower', 
                              extent = [0, 21, 0, 1 ], interpolation ='spline16', cmap =cmap, 
             vmin=np.min(mrk_position), 
             vmax = np.max(mrk_position))
        plt.xlabel('Cell index')
        plt.ylabel('Time [s]')
        plt.colorbar()
        
        
        plt.figure('Contour map Rotation')
        cmap = plt.get_cmap('jet')
        contour = plt.imshow(abs(Rotation[:,0::8]), aspect='auto', origin = 'lower', 
                              extent = [0, 21, 0, 1], interpolation ='spline16', cmap =cmap, 
             vmin=np.min(mrk_position), 
             vmax = np.max(mrk_position))
        plt.xlabel('Cell index')
        plt.ylabel('Time [s]')
        plt.colorbar()
        return()
        
if __name__ == '__main__':
    fname_video = '2D_transition_2000fps_0.543s_trial1_20210428_150758_C001H001S0001.avi'
    fps = 2000
    t_range = 186/24 
    dir_cwd     = os.getcwd()
    fname_save  = 'Result_DIC'
    area_min    = 1.5*1e2
    area_max    = 2.5*1e2
    thrsh_min =  200
    thrsh_max   = 255# 255
    track = MotionTrack_RectangleDetect(fname_video, dir_cwd, fname_save,
                                        area_min, area_max, thrsh_min, thrsh_max)
    
    # DIC analysis
    mrk_position, Rotation = track.DIC_tracking(import_feature=False)

    track.contourArea(mrk_position, t_range, fps, Rotation)
    