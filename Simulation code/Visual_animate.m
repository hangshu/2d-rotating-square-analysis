function Visual_animate(t,x,Nx,CO, opt,df, filename,Theta0,Defect_loc, L, CNT)
spacing=round(length(t)/50);

for p=1:spacing:length(t)
    figure(20);clf
for n=1:length(CO(:,1))
    q=1+mod(floor((n-1)/Nx),2)+n-Nx*floor((n-1)/Nx)-1;
    if abs(x(p,2*df*n-3))>pi/4
        r=1;
    else
    r=abs(x(p,2*df*n-3))/(pi/4);
    end
Xe=[CO(n,1)+x(p,2*df*n-5)+L*cos(Theta0+x(p,2*df*n-3)) ...
    CO(n,1)+x(p,2*df*n-5)+(-1)^q*L*sin(Theta0+x(p,2*df*n-3)) ...
    CO(n,1)+x(p,2*df*n-5)-L*cos(Theta0+x(p,2*df*n-3)) ...
    CO(n,1)+x(p,2*df*n-5)-(-1)^q*L*sin(Theta0+x(p,2*df*n-3))];
Ye=[CO(n,2)+x(p,2*df*n-4)+(-1)^q*L*sin(Theta0+x(p,2*df*n-3)) ...
    CO(n,2)+x(p,2*df*n-4)-L*cos(Theta0+x(p,2*df*n-3)) ...
    CO(n,2)+x(p,2*df*n-4)-(-1)^q*L*sin(Theta0+x(p,2*df*n-3)) ...
    CO(n,2)+x(p,2*df*n-4)+L*cos(Theta0+x(p,2*df*n-3))];

if x(p,2*df*n-3)>=0
    if opt==1 && n == Defect_loc(1)
        fill(Xe,Ye,'k');hold on 
    elseif opt==1 && n == Defect_loc(2)
        fill(Xe,Ye,'k');hold on 
    else
        fill(Xe,Ye,[r 1-r 0]);hold on
    end 
    axis([min(CO(:,1))-0.1*max(CO(:,1)) 1.1*max(CO(:,1)) ...
        min(CO(:,2))-0.1*max(CO(:,2)) 1.1*max(CO(:,2))])
    axis equal
    grid off 
elseif x(p,2*df*n-3)<0
    if opt==1 && n == Defect_loc(1)
        fill(Xe,Ye,'k');hold on 
    elseif opt==1 && n == Defect_loc(2)
        fill(Xe,Ye,'k');hold on 
    else
        fill(Xe,Ye,[0 1-r r]);hold on
    end 

    axis([min(CO(:,1))-0.1*max(CO(:,1)) 1.1*max(CO(:,1)) ...
        min(CO(:,2))-0.1*max(CO(:,2)) 1.1*max(CO(:,2))])
    axis equal
    grid off 
    
end
end
pause (0.1)
%     axis image
%      axis off
    frame = getframe; 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256);
    if p == 1
        if CNT==1
        imwrite(imind,cm,filename,'gif','WriteMode','overwrite', 'Loopcount',inf);
        %imwrite(imind,cm,filename,'png','WriteMode','overwrite');
        else
        
        imwrite(imind,cm,filename,'gif','WriteMode','append', 'DelayTime',0.05);
        end
    else
        imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.05);
    end
end
end