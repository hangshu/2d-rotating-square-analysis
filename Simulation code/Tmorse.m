function y=Tmorse(A,alpha,Delta_Theta,ThetaLin,Theta_morse)
Tmorse1=2*alpha*A*(exp(2*alpha*(Delta_Theta+2*ThetaLin-2*Theta_morse))-...
    exp(alpha*(Delta_Theta+2*ThetaLin-2*Theta_morse)));
Tmorse2=2*alpha*A*(exp(-2*alpha*(Delta_Theta+2*ThetaLin+2*Theta_morse))-...
    exp(-alpha*(Delta_Theta+2*ThetaLin+2*Theta_morse)));
y=1*(Tmorse1-Tmorse2);

end