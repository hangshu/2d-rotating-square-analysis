%Code to extrac info from PFA motion analysis 
%Implemented by: Hang Shu, 030921
%=========================================================================
function PFA_extraction(Nx, Ny)
path= '/home/hangshu/Documents/2d-rotating-square-analysis/Experiments/';
%%system parameters 
dp = 0.388; %[mm]
trial = 4;
options = '_5points.csv'; %'_impactsignal'
fileId  = [path, '2D_transition_x',num2str(Nx),'_y',num2str(Ny),'_030921_trial',...
    num2str(trial),options];
Data = csvread(fileId,3);
N_cells = sscanf(options, '_%d');
row_x = [18, 10, 1, 6, 18];
row_y = [18, 18, 18, 14, 14];
for ii = 1:N_cells
    %row_x = sscanf(names{(ii-1)*9+2},'x%d');row_y = sscanf(names{(ii-1)*9+2},'y%d');
    N(ii) = (row_y(ii)-1)*Nx+row_x(ii); 
end 

t_exp = Data(:,1)-Data(1,1);
% x = [Data(:,10), Data(:,19), Data(:,20), Data(:,37), Data(:,46)]*dp;
% y = [Data(:,9), Data(:,18), Data(:,21), Data(:,36), Data(:,45)]*dp;
x_exp = [Data(:,2), Data(:,11), Data(:,20), Data(:,29), Data(:,46)]*dp;
y_exp = [Data(:,3), Data(:,12), Data(:,21), Data(:,30), Data(:,45)]*dp;
%%extraction experimental data
for i = 1:size(x_exp,2)
    x_exp(:,i) = x_exp(:,i)-x_exp(1,i);
    x_exp(:,i) = abs(x_exp(:,i))/max(abs(x_exp(:,i)));
    y_exp(:,i) = y_exp(:,i)-y_exp(1,i);
    y_exp(:,i) = abs(y_exp(:,i))/max(abs(y_exp(:,i)));
end 

%%extract numerical data
% 
% figure;
% colormap(jet)
% hold on 
% contourf(dismesh, tmesh, u', 2000,'LineColor','None')
% %plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
% %plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
% hold off 
% %xlim([0, max(dis_vec)])
% xticks([0,90, 180])
% xticklabels({'1','9','18'})
% %yticks([0, max(t)/4,max(t)/2,3*max(t)/4,max(t)] )
% ylim([0, max(t)])
% title('Displacement U_x')
% % title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
% xlabel('Cell colu% [t,x]=ode45(@(t,x) Governing_eqns_2D(t,x,CO,CM,Nx,Ny, df,Kl,Ks,Kj,M,J,L,Theta0,...
%     ThetaLin,Theta_morse,A,alpha,Defect_loc,opt, damp_flag),timeINT1,initCOND1,opts);
% filename = ['2D_Rotating_Square_motion_','x', num2str(Nx),'_y',num2str(Ny),...
%     '_opt',num2str(opt),'_Impact',num2str(initCOND1(6*(ex_loc1)-2)),'_t',...
%     num2str(timeINT1(end)),'.gif'];

% ylabel('Time [s]')
% cc = colorbar();
% cc.Label.String = 'U_x [mm]';
% set(gca, 'Linewidth',2,'fontsize',18)
% % set(gcf, 'position',[730,200,640, 560])
% 
% figure;
% colormap(jet)
% hold on 
% contourf(dismesh, tmesh, v', 2000,'LineColor','None')
% %plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
% %plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
% hold off 
% xticks([0,90, 180])
% xticklabels({'1','9','18'})
% %yticks([0, max(t)/4,max(t)/2,3*max(t)/4,max(t)] )
% ylim([0, max(t)])
% title('Displacement U_y')
% % title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
% xlabel('Cell column index')
% ylabel('Time [s]')
% cc = colorbar();
% cc.Label.String = 'U_y [mm]';
% set(gca, 'Linewidth',2,'fontsize',18)
% % set(gcf, 'position',[730,200,640, 560])


figure;
colormap(jet)
hold on 
contourf(x_exp(:,1:3), 2000,'LineColor','None')
%plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
%plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
yticks([1, length(t_exp)/4, length(t_exp)/2, 3*length(t_exp)/4, length(t_exp)])
yticklabels({num2str(0), num2str(round(t_exp(end)/4,2)), num2str(round(t_exp(end)/2,2)), ...
    num2str(round(3*t_exp(end)/4,2)), num2str(round(t_exp(end),2))})
 xticks([1, 2, 3])
xticklabels({'1', '9', '18'})
xlabel('Cell column index')
ylabel('Time [s]')
title('Exp displacement U_x')
cc = colorbar();
cc.Label.String = 'Normalized U_x';
set(gca, 'Linewidth',2,'fontsize',18)
% set(gcf, 'position',[730,200,640, 560])


figure;
colormap(jet)
hold on 
contourf(y_exp(:,1:3), 2000,'LineColor','None')
%plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
%plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
yticks([1, length(t_exp)/4, length(t_exp)/2, 3*length(t_exp)/4, length(t_exp)])
yticklabels({num2str(0), num2str(round(t_exp(end)/4,2)), num2str(round(t_exp(end)/2,2)), ...
    num2str(round(3*t_exp(end)/4,2)), num2str(round(t_exp(end),2))})
 xticks([1, 2, 3])
xticklabels({'1', '9', '18'})
title('Exp displacement U_y')
xlabel('Cell column index')
ylabel('Time [s]')
cc = colorbar();
cc.Label.String = 'Normalized U_y';
set(gca, 'Linewidth',2,'fontsize',18)
end 

% yticks([1, length(t_exp)/4, length(t_exp)/2, 3*length(t_exp)/4, length(t_exp)])
% yticklabels({num2str(0), num2str(round(t_exp(end)/4,2)), num2str(round(t_exp(end)/2,2)), ...
%     num2str(round(3*t_exp(end)/4,2)), num2str(round(t_exp(end),2))})
%  xticks([1, 2, 3])
% xticklabels({'1', '9', '18'})
% xlabel('Cell column index')
% ylabel('Time [s]')