%=====================2D Rotating Square Simulation========================%
cd '/home/hangshu/Documents/2d-rotating-square-analysis/Results/';
%change cd to .../hangshu/... for home computers 
close all;clc; clf; clear all
df=3;                  %the number of freedoms on each node.
Ne=2;                  %the number of node on each element.
Kl=1*670.8;Kj=1*0.4*0.00044325837185769376;Ks=1*Kl;M=1*2.49*1e-3;
L=12*1e-3/(2*cos(pi/4));Theta0=pi*(0/180);ThetaLin=pi*(0/180);Fexe=0*1e-7;
J=(1/12)*M*L*L;
Theta_morse=0.5*0.74422634;A=0.0005477163091763655;alpha=4.966064409064443;
damp_flag = 1; %damping 
% cl = 0.34; cj = 13.67;
%=====================GEOMETRY AND MESH GENERATION========================%
Nx      = 21;       %number of nodes along x
Ny      = 4;       %number of nodes along y
Num_link=(Nx-1)*Ny+(Ny-1)*Nx;
Lx=1*2*L;Ly=1*2*L;

%system modifications 

opt=0 ;%opt=1 if defects are assigned, otherwise, no defect
di1=5;dj1=4; 
Defect_loc(1)=di1+(dj1-1)*Nx;
di2=17;dj2=1 ; 
Defect_loc(2)=di2+(dj2-1)*Nx;
% di3=18;dj3=18;
% Defect_loc(3)=di3+(dj3-1)*Nx;

%simulation parameters 
ex_loc1=1;
ex_loc2= Nx*Ny;

%save file
CNT=1;
count1=0;
%=========================================================================%

for jj=1:Ny
    for ii=1:Nx
        count1=count1+1;
        CO(count1,:)=[(ii-1)*Lx (jj-1)*Ly];
    end
    
end% xticks([0,50, 100,  150, 200])
% xticklabels({'1','5','10','15','20'})

count1=0;
for nn=1:Nx
    for mm=1:Ny-1
    aa=nn+(mm-1)*Nx;bb=nn+(mm-1)*Nx+Nx;
    count1=count1+1;
        CM(count1,:)=[aa bb];
    end
end
for nn=1:Ny
    for mm=1:Nx-1
    cc=mm+(nn-1)*Nx;
    dd=mm+(nn-1)*Nx+1;
    count1=count1+1;
        CM(count1,:)=[cc dd];
    end
end

%=========================================================================
%time and intial conditions 
timeINT1=linspace(0,0.4,2000);timeINT2=linspace(0,0.2,1000);
initCOND1=zeros(2*df*length(CO(:,1)),1); p=2.4;
initCOND1(6*(ex_loc1)-2)= 2*p;%initCOND1(6*(ex_loc1)-3)= pi ;
%initCOND1(6*(ex_loc2)-2)= -p; %initCOND1(6*(ex_loc2)-2)= -0.75*p ;

% %visualize initial mesh
% for n=1:length(CO(:,1))
%     q=1+mod(floor((n-1)/Nx),2)+n-Nx*floor((n-1)/Nx)-1;
% figure(1);hold on
% Xe=[CO(n,1)+L*cos(Theta0) CO(n,1)+(-1)^q*L*sin(Theta0) ...
%     CO(n,1)-L*cos(Theta0) CO(n,1)-(-1)^q*L*sin(Theta0)];
% Ye=[CO(n,2)+(-1)^q*L*sin(Theta0) CO(n,2)-L*cos(Theta0) ...
%     CO(n,2)-(-1)^q*L*sin(Theta0) CO(n,2)+L*cos(Theta0)];
%     if n== Defect_loc(1) || n == Defect_loc(2)
%     fill(Xe,Ye,'k')
%   
%     else 
%     fill(Xe,Ye,'g')
%     end
% set(gca,'visible','off')
% axis equal
% end

opts = odeset('RelTol',1e-5,'AbsTol',1e-5);
[t,x]=ode45(@(t,x) Governing_eqns_2D(t,x,CO,CM,Nx,Ny, df,Kl,Ks,Kj,M,J,L,Theta0,...
    ThetaLin,Theta_morse,A,alpha,Defect_loc,opt, damp_flag),timeINT1,initCOND1,opts);
    
filename = ['2D_Rotating_Square_motion_','x', num2str(Nx),'_y',num2str(Ny),...
     '_opt',num2str(opt),'_Impact',num2str(initCOND1(6*(ex_loc1)-2)),'_t',...
     num2str(timeINT1(end)),'.gif'];
%Visual_animate(t,x,Nx,CO, opt,df, filename,Theta0, Defect_loc, L,CNT)
[v_wave, Y_fmax] = contour_plot(t,x, Nx,L, Theta0, df, Nx);
%[w_t, dw_t, t_w] = surfplot(t,x, Nx,Ny,L, Theta0, df,opt);

initCOND2 = zeros(2*df*length(CO(:,1)),1); 
for i = 1: Nx*Ny
    initCOND2(1:6:end) = x(end,1:6:end); initCOND2(2:6:end) = x(end,2:6:end); 
    initCOND2(3:6:end) = x(end,3:6:end); 
end 
%Visual_animate(t,x,Nx,CO, opt,df, filename,Theta0, Defect_loc, L,CNT)
initCOND2(4) = -2.5*p; 
initCOND2(5) = -2*p; 

% damp_flag = 2;
% [t1,x1]=ode45(@(t1,x1) Governing_eqns_2D(t1,x1,CO,CM,Nx,Ny, df,Kl,Ks,Kj,M,J,L,Theta0,...
%     ThetaLin,Theta_morse,A,alpha,Defect_loc,opt, damp_flag),timeINT2,initCOND2,opts);
% for j = 1:Nx*Ny
%     x1(j,:) = x1(j,:)-initCOND2';
% end 
% extract = 50;
%Visual_animate(t1,x1,Nx,CO, opt,df, filename,Theta0, Defect_loc, L,CNT)
% %remove defect
% initCOND3=x(end,:);
% opt = 1;
% di1=2;dj1=4;
% Defect_loc(1)=di1+(dj1-1)*Nx;
% [t,x]=ode45(@(t,x) Governing_eqns_2D,300,400, 500(t,x,CO,CM,Nx,df,Kl,Ks,Kj,M,J,L,Theta0,...
%     ThetaLin,Theta_morse,A,alpha,Defect_loc,opt),timeINT1,initCOND3,opts);
% %Visual_animate(t,x,Nx,CO, opt,df, filename,Theta0,di1, dj1, L,CNT)
% Total energy
% Visulization
% establish mesh
% 
% [Etot] = Energy_calc(t,x, CO, CM, Nx, df, Theta0, L, alpha, ThetaLin, Theta_morse, Kl, Ks, Kj, A,...
%                         M,J);
% PFA_extraction(Nx, Ny);