function [v_wave, Y_fmax] = contour_plot(t,x, Nx,L, Theta0, df, extract)
spacing=round(length(t)/100);
t_vec = t(1:spacing:end);
x(isnan(x))=0;
dis_vec = linspace(0, sqrt(2)*Nx*L*cos(Theta0)*1000, Nx); %unit mm
[tmesh, dismesh] = meshgrid(t_vec,dis_vec);
u = zeros(length(t(1:spacing:end)),Nx);
v = zeros(length(t(1:spacing:end)),Nx);
w = zeros(length(t(1:spacing:end)),Nx);
du = zeros(length(t(1:spacing:end)),Nx);
dv = zeros(length(t(1:spacing:end)),Nx);
dw = zeros(length(t(1:spacing:end)),Nx);
for i = 1:Nx
    
    u(:,i) = (abs(x(1:spacing:end,(i-1)*df*2+1))-abs(x(1,(i-1)*df*2+1)))/max(abs(x(1:spacing:end,(i-1)*df*2+1)));
    v(:,i) = (abs(x(1:spacing:end,(i-1)*df*2+2))-abs(x(1,(i-1)*df*2+2)))/max(abs(x(1:spacing:end,(i-1)*df*2+2)));
    w(:,i) = (abs(x(1:spacing:end,(i-1)*df*2+3))-abs(x(1,(i-1)*df*2+3)))/max(abs(x(1:spacing:end,(i-1)*df*2+3)));
    du(:,i) = abs(x(1:spacing:end,(i-1)*df*2+4))/max(abs(x(1:spacing:end,(i-1)*df*2+4)));
    dv(:,i) = abs(x(1:spacing:end,(i-1)*df*2+5))/max(abs(x(1:spacing:end,(i-1)*df*2+5)));
    dw(:,i) = abs(x(1:spacing:end,(i-1)*df*2+6))/max(abs(x(1:spacing:end,(i-1)*df*2+6)));
end
u(isnan(u))=0;v(isnan(v))=0;w(isnan(w))=0;du(isnan(du))=0;dv(isnan(dv))=0;dw(isnan(dw))=0;
Y_fmax = zeros(df*2,1); Y_imax = zeros(df*2,1);v_wave = zeros(df*2,1);
Y_fmax(1) = t_vec(u(1:round(length(t_vec)), extract)==max(u(1:round(length(t_vec)), extract))); 
Y_fmax(2) =  t_vec(v(1:length(t_vec), extract)==max(v(1:length(t_vec), extract))); 
Y_fmax(3) =  t_vec(w(1:length(t_vec), extract)==max(w(1:length(t_vec), extract))); 
Y_fmax(4) =  t_vec(du(1:round(length(t_vec)), extract)==max(du(1:round(length(t_vec)), extract))); 
Y_fmax(5) =  t_vec(dv(1:length(t_vec), extract)==max(dv(1:length(t_vec), extract))); 
Y_fmax(6) =  t_vec(dw(1:length(t_vec), extract)==max(dw(1:length(t_vec), extract))); 

% 
Y_imax(1) = t_vec(u(1:round(length(t_vec)/4), extract)==max(u(1:round(length(t_vec)/4), extract))); 
Y_imax(2) =  t_vec(v(1:length(t_vec)/2, extract)==max(v(1:length(t_vec)/2, extract))); 
Y_imax(3) =  t_vec(w(1:length(t_vec)/2, extract)==max(w(1:length(t_vec)/2, extract))); 
Y_imax(4) =  t_vec(du(1:round(length(t_vec)/4), extract)==max(du(1:round(length(t_vec)/4), extract))); 
Y_imax(5) =  t_vec(dv(1:length(t_vec)/2, extract)==max(dv(1:length(t_vec)/2, extract))); 
Y_imax(6) =  t_vec(dw(1:length(t_vec)/2, extract)==max(dw(1:length(t_vec)/2, extract)));
for j = 1:df*2
    v_wave(j) = max(dis_vec/1000)/(Y_imax(j));
end 

%%
%interpolation 

dis_interp = linspace(0, sqrt(2)*Nx*L*cos(Theta0)*1000, 1000); %unit mm
t_interp = linspace(0, t_vec(end),1000);
[tinterp, disinterp] = meshgrid(t_interp,dis_interp);
wq = interp2(tmesh, dismesh, w',tinterp, disinterp);

%%
% figure(10);
% colormap(jet)
% hold on 
% contourf(dismesh, tmesh, u', 2000,'LineColor','None')
% %plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
% %plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
% 
% hold off 
% %xlim([0, max(dis_vec)])
% xticks([0,100,200,300,400, 500])
% xticklabels({'1','10','20','30','40','50'})
% % xticklabels({'1','','18'})
% %yticks([0, max(t)/4,max(t)/2,3*max(t)/4,max(t)] )
% ylim([0, max(t)])
% title('Num displacement U_x')
% % title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
% xlabel('Cell column index')
% ylabel('Time [s]')
% cc = colorbar();
% cc.Label.String = 'Normalized U_x';
% set(gca, 'Linewidth',2,'fontsize',18)
% % set(gcf, 'position',[730,200,640, 560])
% 
% figure(11);
% colormap(jet)
% hold on 
% contourf(dismesh, tmesh, v', 2000,'LineColor','None')
% %plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--M74A7512_resize.mov','LineWidth',4)
% %plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
% hold off 
% % xticks([0,Nx*5, Nx*10])
% xticks([0,100,200,300,400, 500])
% xticklabels({'1','10','20','30','40','50'})
% ylim([0, max(t)])
% title('Contour displacement plot U_y')
% % title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
% xlabel('Cell column index')
% ylabel('Time [ms]')
% cc = colorbar();
% cc.Label.String = 'Normalized displacement';
% set(gca, 'Linewidth',2,'fontsize',18)
% % set(gcf, 'position',[730,200,640, 560])
% % 

figure(12);
colormap(jet)
hold on 
contourf(dismesh, tmesh, w', 2000,'LineColor','None')
%plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
%plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
hold off 
xticks([1,100,200,300,400, 500])
xticklabels({'1','10','20','30','40','50'})
%yticks([0, max(t)/4,max(t)/2,3*max(t)/4,max(t)] )
ylim([0, max(t)])
title('Contour rotation plot U_\omega')
% title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
xlabel('Cell column index')
ylabel('Time [s]')
cc = colorbar();
cc.Label.String = 'Normalized U_\omega';
set(gca, 'Linewidth',2,'fontsize',18)
% set(gcf, 'position',[730,200,640, 560])
 
% 
% 
% figure(13);
% colormap(jet)
% hold on 
% contourf(dismesh, tmesh*1000, du', 2000,'LineColor','None')
% 
% hold off 
% xlim([0, max(dis_vec)])
% xticks([0,100,200,300,400, 500])
% xticks([0,100,200,300,400, 500])
% xticklabels({'1','10','20','30','40','50'})
% yticks([0, max(t)/4,max(t)/2,3*max(t)/4,max(t)] )
% ylim([0, max(t)])
% title('Contour velocity plot V_x')
% xlabel('Cell column index')
% ylabel('Time [ms]')
% cc = colorbar();
% cc.Label.String = 'Normalized V_x';
% set(gca, 'Linewidth',2,'fontsize',18)


% figure(14);
% colormap(jet)
% hold on 
% contourf(dismesh, tmesh, dv', 2000,'LineColor','None')
% %plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
% %plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
% hold off 
% xticks([0,Nx*5, Nx*10])
% xticks([0,100,200,300,400, 500])
% xticklabels({'1','10','20','30','40','50'})
% ylim([0, max(t)])
% title('Num velocity V_y')
% % title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
% xlabel('Cell column index')
% ylabel('Time [s]')
% cc = colorbar();
% cc.Label.String = 'Normalized velocity';
% set(gca, 'Linewidth',2,'fontsize',18)
% set(gcf, 'position',[730,200,640, 560])


figure(15);
colormap(jet)
hold on 
contourf(dismesh, tmesh, dw', 2000,'LineColor','None')

hold off 
xticks([0,100,200,300,400, 500])
xticklabels({'1','10','20','30','40','50'})
yticks([0, max(t)/4,max(t)/2,3*max(t)/4,max(t)] )
ylim([0, max(t)])
title('Num velocity V_w')
xlabel('Cell column index')
ylabel('Time [s]')
cc = colorbar();
cc.Label.String = 'Normalized V_w';
set(gca, 'Linewidth',2,'fontsize',18)

end 
