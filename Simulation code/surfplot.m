function [w_t, dw_t, t_w] = surfplot(t,x, Nx,Ny,L, Theta0, df, opt)
vid_flag = 0;
x(isnan(x))=0;
u = zeros(Nx*Ny, length(t));
v = zeros(Nx*Ny, length(t));
w = zeros(Nx*Ny, length(t));
du = zeros(Nx*Ny, length(t));
dv = zeros(Nx*Ny, length(t));
dw = zeros(Nx*Ny, length(t));

%extract displacement/velocity information 
for j = 1:Nx*Ny
        u(j,:) = x(:,(j-1)*df*2+1);
        v(j,:) = x(:,(j-1)*df*2+2);
        w(j,:) = x(:,(j-1)*df*2+3);
        du(j,:) = x(:,(j-1)*df*2+4);
        dv(j,:) = x(:,(j-1)*df*2+5);
        dw(j,:) = x(:,(j-1)*df*2+6);
end 
t_w = zeros(Ny, Nx); %time of transmitted transition wave 
dw_t = zeros(Ny, Nx); %angular speed at time of pass-through 
w_t = zeros(Ny, Nx); %rotation at the time of pass-through 
R_cells = 0;
for ii = 1:Nx*Ny
        n = rem((ii-1),Nx)+1;
        m = fix((ii-1)/Nx)+1;
        loc = find(abs(abs(w(ii,:))-pi/4) < 1e-3); %time to reach phase C
        if isempty(loc) == 1
            loc2 = find(abs(abs(w(ii,:))-pi/4) == min(abs(abs(w(ii,:))-pi/4)));
            dw_t(Ny-m+1,n) = dw(ii,loc2(1));
            w_t(Ny-m+1,n) = w(ii, loc2(1));
            t_w(Ny-m+1,n) = t(loc2(1)); 
            if t(loc2(1)) == t(end)
                R_cells = R_cells+1;
            end 
        else 
            t_w(Ny-m+1,n) = t(loc(1));
            dw_t(Ny-m+1,n) = dw(ii,loc(1));
            w_t(Ny-m+1,n) = w(ii, loc(1));
        end
end 

dis_vecy = linspace(0, Ny, Ny*10);
dis_vecx =    linspace(0, Nx, Nx*10);
[dismeshx, dismeshy] = meshgrid(dis_vecx, dis_vecy);
                 
t_ref = interp2(t_w, dismeshy, dismeshx);

%extract information on phase B

[bi,bj] = ind2sub(size(w_t),find(abs(t_w)== (t(end))));
[ci,cj] = ind2sub(size(w_t),find(abs(w_t)<0.7));
% b = zeros(length(bi),2); b(:,1) = bi; b(:,2)=bj;
% c = zeros(length(ci),2); c(:,1) = ci; c(:,2)=cj;
% tri_b = delaunay(bj,bi);
% tri_c = delaunay(cj,ci);
% for i = 1:length(bi)
%     t_bref(i,1) = t_w(bi(i),bj(i));
% end 
% for j = 1:length(ci)
%     t_cref(j,1) = t_w(ci(j),cj(j));
% end 
%2d contour time map
figure(1)

colormap jet
contourf(t_w, 2000,'LineColor','None')
xlim([1, Nx])
ylim([1, Ny])
set(gca, 'Ydir','reverse')
xlabel('Cell column index')
ylabel('Cell row index')
% yticks([1, Ny/6, 2*Ny/6, 3*Ny/6, 4*Ny/6, 5*Ny/6, Ny] )
% yticklabels({num2str(Ny), num2str(5*Ny/6), num2str(4*Ny/6), num2str(Ny/2),...
%      num2str(2*Ny/6), num2str(Ny/6), num2str(1)})
h = colorbar();
set(get(h,'title'),'string','Time [s]');
shading interp 
title(['Phase R cells: ',num2str(R_cells)])
view(2)


figure(2)
colormap jet
contourf(w_t, 2000,'LineColor','None')
xlim([1, Nx])
ylim([1, Ny])
set(gca, 'Ydir','reverse')
xlabel('Cell column index')
ylabel('Cell row index')
title(['Transmission t = ', num2str(max(max(t_w))), ' s'])
% yticks([1, Ny/4, 2*Ny/4, 3*Ny/4, 4*Ny/4] )
% yticklabels({num2str(Ny), num2str(3*Ny/4), num2str(2*Ny/4), num2str(Ny/4),...
%      num2str(1)})
h1 = colorbar();
set(get(h1,'title'),'string','Rotation angle');
shading interp 
view(2)

if vid_flag == 1
w = abs(w)/max(max(abs(w)));
frames =  10; 
filename = ['2D_Rotating_Square_wave_','x', num2str(Nx),'_y',num2str(Ny),...
    '_opt',num2str(opt),'_Impact',num2str(dw(1,1)),'_t',...
    num2str(t(end)),'.gif'];
    for i = 1:frames
    h = figure(10);
    colormap(jet)
    hold on 
    contourf(reshape(w(:,i*length(t)/frames),[Nx,Ny])', 2000,'LineColor','None')
    %plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
    %plot([0, max(dis_vec)],[0, Y_imax(3)], 'g--','LineWidth',4)
    title(['Displacement \omega, t = ', num2str(t(i*length(t)/frames)),'s'])
    hold off 
    xlim([1, Nx])
    %xticks([0,round(max(dis_vec)/4), round(max(dis_vec)/2),round(3*max(dis_vec)/4), round(max(dis_vec))])
    %yticks([0, max(t)/4,max(t)/2,3*max(t)/4,max(t)] )
    ylim([1, Ny])
    % title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
    xlabel('Cell column index')
    ylabel('Cell row index')
    cc = colorbar();
    cc.Label.String = 'Rotation angle \omega';
    %set(gca, 'Linewidth',2,'fontsize',20)
    %set(gcf, 'position',[730,200,640, 560])
    pause (0.1)
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if i == 1
            
    imwrite(imind,cm,filename,'gif','WriteMode','overwrite', 'Loopcount',inf);
            %imwrite(imind,cm,filename,'png','WriteMode','overwrite');
    
    else
            imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.05);
    end
    end
end 
%calculate v_wave 
spacing = 10;
w_ref = w(:,1:spacing:end);
v_wave = Nx*L*2/max(max(t_w));

figure(3)
colormap(jet)
hold on
contourf(w(1:Nx,1:spacing:end)', 2000,'LineColor','None')
%plot([0, max(dis_vec)],[0, Y_fmax(3)], 'g--','LineWidth',4)
%plot([1, Nx],[0, floor(find(t==max(max(t_w)))/spacing)], 'g--','LineWidth',4)
hold off
title(['Displacement \omega, v =', num2str(v_wave), ' m/s'])
xlim([1, Nx])
%xticks([0,round(max(dis_vec)/4), round(max(dis_vec)/2),round(3*max(dis_vec)/4), round(max(dis_vec))])
% yticks([0, size(w_ref,2)/(4),size(w_ref,2)/2,3*size(w_ref,2)/(4),size(w_ref,2)] )
% yticklabels({num2str(0), num2str(max(t)/4), num2str(max(t)/2),...
%      num2str(3*max(t)/4), num2str(max(t))})
ylim([1, size(w_ref,2)])
% title(['U_\omega, v = ', num2str(v_wave(3)), '[m/s]'])
xlabel('Cell column index')
ylabel('Time [s]')
cc = colorbar();
cc.Label.String = 'Rotation angle';
%set(gca, 'Linewidth',2,'fontsize',20)
%set(gcf, 'position',[730,200,640, 560])
end