#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  5 09:40:48 2021

@author: hangshu
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 30 23:36:45 2021

@author: hangshu
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.colorbar import Colorbar
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import cmath
import scipy.optimize as opt
from scipy import signal
from scipy import integrate
from scipy import linalg
from scipy.interpolate import interp1d
import os
import sys
import shutil   #High level file operation
import time
import warnings
from scipy import fftpack
from scipy import signal
#from IMS_VTK4Animation import CubicModule_VTKanimation
sys.path.append('../2d-rotating-square-analysis/linear-analysis/')

# Figure parameters =================================================
# When you insert the figure, you need to make fig height 2
plt.rcParams['font.family']     = 'sans-serif'
plt.rcParams['figure.figsize']  = 8, 6      # (w=3,h=2) multiply by 3
plt.rcParams['font.size']       = 18       # Original fornt size is 8, multipy by above number
plt.rcParams['text.usetex']     = False
#plt.rcParams['ps.useafm'] = True
#plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['lines.linewidth'] = 3.  
plt.rcParams['lines.markersize'] = 10.
plt.rcParams['legend.fontsize'] = 16        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.labelsize'] = 16        # Original fornt size is 8, multipy by above number
plt.rcParams['ytick.labelsize'] = 16        # Original fornt size is 8, multipy by above number
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
plt.rcParams['figure.subplot.left']  = 0.2
#plt.rcParams['figure.subplot.right']  = 0.7
plt.rcParams['figure.subplot.bottom']  = 0.2
plt.rcParams['savefig.dpi']  = 300

#==============================================================================
# compute dynamics of Rotating square chain
#==============================================================================
class RotSq_Dynamics_unit():
    def __init__(self, df, Kl, Kj, Ks, M, L, Theta0, ThetaLin, J, Theta_morse,
                 A, alpha, damp_flag, Nx, Ny, Num_link, Lx, Ly):
        
        # degree of freedom
        self.df = df
        
        # spring constants 
        self.Kl  = Kl
        self.Kj = Kj
        self.Ks = Ks


        # mass 
        self.M = M
        self.J = J
        
        # geometry 
        self.L = L 
        self.Theta0 = Theta0
        self.ThetaLin = ThetaLin
        self.Nx = Nx
        self.Ny = Ny
        self.Num_link = Num_link
        self.Lx = Lx
        self.Ly = Ly
        
        # magnetic potential 
        self.Theta_morse = Theta_morse
        self.A = A
        self.alpha = alpha
        
        #initalize FEM matices
        self.CO = np.zeros((self.Nx*self.Ny,2))
        self.CM = np.zeros((self.Nx*self.Ny+(self.Nx*self.Ny-self.Nx-self.Ny),2))
        
        count1 = 0
        # construct fem matrix
        for jj in range(1,self.Ny+1):
            for ii in range(1,self.Nx+1):
                self.CO[int(count1),:]=[(ii-1)*self.Lx, (jj-1)*self.Ly]
                count1=count1+1;
            
        count1=0;
        for nn in range(1,self.Nx+1):
            for mm in range(1,self.Ny):
                aa=nn+(mm-1)*self.Nx;bb=nn+(mm-1)*self.Nx+self.Nx;
               
                self.CM[int(count1),:]=[aa, bb];
                count1=count1+1;
     
        for nn in range(1,self.Ny+1):
            for mm in range(1,self.Nx):
                cc=mm+(nn-1)*self.Nx;
                dd=mm+(nn-1)*self.Nx+1;
                
                self.CM[int(count1),:]=[cc, dd];
                count1=count1+1;
        print(self.CO)
        print(self.CM)
    def Tmorse(self, Delta_Theta):
        Tmorse1=2*self.alpha*self.A*(np.exp(2*self.alpha*(Delta_Theta+2*
                  self.ThetaLin-2*self.Theta_morse))-np.exp(self.alpha*
                    (Delta_Theta+2*self.ThetaLin-2*self.Theta_morse)));
        Tmorse2=2*self.alpha*self.A*(np.exp(-2*self.alpha*(Delta_Theta+
                   2*self.ThetaLin+2*self.Theta_morse))-np.exp(-self.alpha*
                   (Delta_Theta+2*self.ThetaLin+2*self.Theta_morse)));
        y=1*(Tmorse1-Tmorse2);

        return(y)
    
    def damp(self, t, damp_flag):
        N = self.Nx*self.Ny; 
        if damp_flag ==0:
             cl = 0; cj = 0;
        elif damp_flag == 2:
             cl=3.4*3*1e-8*(t/0.01)**1.2;cj=3.4*136.7*1e-8*(t/0.01)**1.2;
        else:
             cl=3.4*3*1e-8*(t/0.01)**1.5;cj=3.4*136.7*1e-8*(t/0.01)**1.5; 
        
        return(cl, cj)

    def dynamic_solver(self, timeINT1, initCOND1, opt, Defect_loc, damp_flag=1):
        
        def Governing_eqns_2D(t,x):
            dxdt = np.zeros(2*self.df*len(self.CO[:,0]))
 
            cl,cj = self.damp(t,damp_flag)
            for i in range(len(self.CM[:,0])):
               

               n1=1+np.floor((self.CM[i,0]-1)/self.Nx)%2+self.CM[i,0]-self.Nx*np.floor((self.CM[i,0]-1)/self.Nx)-1;

               n2=n1+1;
               Kj1=self.Kj;
                #add defects
               if opt==1:
                    for jj in range(len(Defect_loc)):
                        if self.CM[i,0]==Defect_loc[jj] or self.CM[i,1]==Defect_loc[jj]+self.Nx+1:
                            Kj1=1000*self.Kj;
   
                
               if self.CO[int(self.CM[i,0]-1),1]==self.CO[int(self.CM[i,1]-1),1]:
                    dxdt[int(2*self.df*self.CM[i,0]-3)]=dxdt[int(2*self.df*self.CM[i,0]-3)]
                    +(1/self.M)*(self.Kl*(x[int(2*self.df*self.CM[i,1]-6)]
                    -x[int(2*self.df*self.CM[i,0]-6)]-self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)+2*self.L*np.cos(self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,0]-2)]=dxdt[int(2*self.df*self.CM[i,0]-2)]
                    +(1/self.M)*(self.Ks*(x[int(2*self.df*self.CM[i,1]-5)]-x[int(2*self.df*self.CM[i,0]-5)]
                    -(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,0]-1)]=dxdt[int(2*self.df*self.CM[i,0]-1)]
                    +(1/self.J)*(-Kj1*(x[int(2*self.df*self.CM[i,0]-4)]+x[int(2*self.df*self.CM[i,1]-4)]
                    +2*self.Theta0-2*ThetaLin)-self.Kl*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]
                    +self.Theta0)*(x[int(2*self.df*self.CM[i,1]-6)]-x[int(2*self.df*self.CM[i,0]-6)]
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)
                    +2*self.L*np.cos(self.Theta0))+(-1)**(n1)*self.Ks*self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)
                    *(x[int(2*self.df*self.CM[i,1]-5)]-x[int(2*self.df*self.CM[i,0]-5)]
                    -(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0))
                    -self.Tmorse(x[int(2*self.df*self.CM[i,0]-4)]
                    +x[int(2*self.df*self.CM[i,1]-4)]+2*(self.Theta0-self.ThetaLin)))
                                   
                    dxdt[int(2*self.df*self.CM[i,1]-3)]=dxdt[int(2*self.df*self.CM[i,1]-3)]
                    -(1/self.M)*(self.Kl*(x[int(2*self.df*self.CM[i,1]-6)]-x[int(2*self.df*self.CM[i,0]-6)]
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)+2*self.L*np.cos(self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,1]-2)]=dxdt[int(2*self.df*self.CM[i,1]-2)]
                    -(1/self.M)*(self.Ks*(x[int(2*self.df*self.CM[i,1]-5)]-x[int(2*self.df*self.CM[i,0]-5)]
                    -(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]
                    +self.Theta0)-(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,1]-1)]=dxdt[int(2*self.df*self.CM[i,1]-1)]
                    +(1/self.J)*(-Kj1*(x[int(2*self.df*self.CM[i,0]-4)]+x[int(2*self.df*self.CM[i,1]-4)]
                    +2*self.Theta0-2*ThetaLin)-self.Kl*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]
                    +self.Theta0)*(x[int(2*self.df*self.CM[i,1]-6)]-x[int(2*self.df*self.CM[i,0]-6)]
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)
                    +2*self.L*np.cos(self.Theta0))+(-1)**(n2)*self.Ks
                    *self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    *(x[int(2*self.df*self.CM[i,1]-5)]-x[int(2*self.df*self.CM[i,0]-5)]
                    -(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0))
                    -self.Tmorse(x[int(2*self.df*self.CM[i,0]-4)]+x[int(2*self.df*self.CM[i,1]-4)]
                    +2*(self.Theta0-self.ThetaLin)));
                                  
               elif self.CO[int(self.CM[i,0]-1),0] == self.CO[int(self.CM[i,1]-1),0]:
                
                    dxdt[int(2*self.df*self.CM[i,0]-3)]=dxdt[int(2*self.df*self.CM[i,0]-3)]
                    +(1/self.M)*(self.Ks*(x[int(2*self.df*self.CM[i,1]-6)]-x[int(2*self.df*self.CM[i,0]-6)]
                    +(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]
                    +self.Theta0)+(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,0]-2)]=dxdt[int(2*self.df*self.CM[i,0]-2)]
                    +(1/self.M)*(self.Kl*(x[int(2*self.df*self.CM[i,1]-5)]-x[int(2*self.df*self.CM[i,0]-5)]
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)+2*self.L*np.cos(self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,0]-1)]=dxdt[int(2*self.df*self.CM[i,0]-1)]
                    +(1/self.J)*(-Kj1*(x[int(2*self.df*self.CM[i,0]-4)]+x[int(2*self.df*self.CM[i,1]-4)]
                    +2*self.Theta0-2*ThetaLin)-self.Kl*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]
                    +self.Theta0)*(x[int(2*self.df*self.CM[i,1]-5)]
                    -x[int(2*self.df*self.CM[i,0]-5)]-self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]
                    +self.Theta0)-self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)
                    +2*self.L*np.cos(self.Theta0))-(-1)**(n1)*self.Ks*self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)
                    *(x[int(2*self.df*self.CM[i,1]-6)]-x[int(2*self.df*self.CM[i,0]-6)]
                    +(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    +(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0))
                    -self.Tmorse(x[int(2*self.df*self.CM[i,0]-4)]
                    +x[int(2*self.df*self.CM[i,1]-4)]+2*(self.Theta0-ThetaLin)));
                                   
                    dxdt[int(2*self.df*self.CM[i,1]-3)]=dxdt[int(2*self.df*self.CM[i,1]-3)]
                    -(1/self.M)*(Ks*(x[int(2*self.df*self.CM[i,1]-6)]-x[int(2*self.df*self.CM[i,0]-6)]
                    +(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]
                    +self.Theta0)+(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,1]-2)]=dxdt[int(2*self.df*self.CM[i,1]-2)]
                    -(1/self.M)*(self.Kl*(x[int(2*self.df*self.CM[i,1]-5)]
                    -x[int(2*self.df*self.CM[i,0]-5)]-self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)+2*self.L*np.cos(self.Theta0)));
                    
                    dxdt[int(2*self.df*self.CM[i,1]-1)]=dxdt[int(2*self.df*self.CM[i,1]-1)]
                    +(1/self.J)*(-Kj1*(x[int(2*self.df*self.CM[i,0]-4)]+x[int(2*self.df*self.CM[i,1]-4)]
                    +2*self.Theta0-2*ThetaLin)-self.Kl*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]
                    +self.Theta0)*(x[int(2*self.df*self.CM[i,1]-5)]-x[int(2*self.df*self.CM[i,0]-5)]
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    -self.L*np.cos(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0)
                    +2*self.L*np.cos(self.Theta0))-(-1)**(n2)*Ks*self.L*np.cos(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)*
                    (x[int(2*self.df*self.CM[i,1]-6)]-x[int(2*self.df*self.CM[i,0]-6)]
                    +(-1)**(n2)*self.L*np.sin(x[int(2*self.df*self.CM[i,1]-4)]+self.Theta0)
                    +(-1)**(n1)*self.L*np.sin(x[int(2*self.df*self.CM[i,0]-4)]+self.Theta0))
                    -self.Tmorse(x[int(2*self.df*self.CM[i,0]-4)]
                    +x[int(2*self.df*self.CM[i,1]-4)]+2*(self.Theta0-self.ThetaLin)));

            for k in range(len(self.CO[:,0])):
                k = k+1
                dxdt[2*self.df*k-6]=x[2*self.df*k-3];
                dxdt[2*self.df*k-5]=x[2*self.df*k-2];
                dxdt[2*self.df*k-4]=x[2*self.df*k-1];
                dxdt[2*self.df*k-3]=dxdt[2*self.df*k-3]-(1/self.M)*cl*(x[2*self.df*k-3]);
                dxdt[2*self.df*k-2]=dxdt[2*self.df*k-2]-(1/self.M)*cl*(x[2*self.df*k-2]);
                dxdt[2*self.df*k-1]=dxdt[2*self.df*k-1]-(1/self.J)*cj*(x[2*self.df*k-1]);
       
                # n = rem((k-1),Nx)+1; #define x axis node
                # m = fix((k-1)/Nx)+1; #define y axis  node
                # if n==1 || n==Nx
                #     dxdt(2*self.df*k-2:2*self.df*k)= dxdt(2*self.df*k-2:2*self.df*k)/10;
            print(dxdt)
            return(dxdt)
        spacing = round(len(timeINT1)/200)
        sol = integrate.solve_ivp(Governing_eqns_2D, [timeINT1[0], timeINT1[-1]],
                                  initCOND1, method = 'RK45', t_eval = timeINT1[0::spacing])   
    
        return(sol)

    def Visualization(self,sol):
                    #########################2D contour plots for disp and vlc###############
            #############Looking at first row of rotating squares##################
        # separate the displacement and velocity 
        u = np.zeros((len(sol.t),self.Nx));
        v = np.zeros((len(sol.t),self.Nx));
        w = np.zeros((len(sol.t),self.Nx));
        du = np.zeros((len(sol.t),self.Nx));
        dv = np.zeros((len(sol.t),self.Nx));
        dw = np.zeros((len(sol.t),self.Nx));
        
        for i in range(self.Nx):
            
            u[:,i] = abs(sol.y[i*self.df*2,:]);          
            v[:,i] = abs(sol.y[i*self.df*2+1,:]);
            w[:,i] = abs(sol.y[i*self.df*2+2,:]);
            du[:,i] = abs(sol.y[i*self.df*2+3,:]);
            dv[:,i] = abs(sol.y[i*self.df*2+4,:]);
            dw[:,i] = abs(sol.y[i*self.df*2+5,:]);

        
        plt.figure('Displacement')
        plt.plot(sol.t,w[:,0],'r')
        plt.plot(sol.t,w[:,1],'k')
        plt.plot(sol.t,w[:,2],'b')
        ###########################contour plots for disp and vlct data#################
        fig = plt.figure('Wave propogation 2D cell displacement horizontal')
        ax = fig.add_subplot(1,2,1)
        cmap = plt.get_cmap('jet')
        sm = ax.imshow(u, aspect='auto', origin = 'upper', interpolation ='spline16', cmap =cmap,
                      vmin=np.min(u), vmax=np.max(u), extent=[0,self.Nx,0,sol.t[-1]])
        ax.set_title('Displacement u')
        ax.set_ylabel('Time [s]')
        ax.set_xlabel('Square index')
        sm = mpl.cm.ScalarMappable(cmap=cmap)
        norm = mpl.colors.Normalize(vmin=np.min(u), vmax=np.max(u))
        cbar = fig.colorbar(sm,cmap=cmap, norm = norm, orientation='horizontal')
        cbar.set_label('Normalized displacement')

        ax1 = fig.add_subplot(1,2,2)
        cmap = plt.get_cmap('jet')
        sm1 = ax1.imshow(du, aspect='auto', origin = 'upper', interpolation ='spline16', cmap =cmap, 
                      vmin=np.min(du), vmax=np.max(du), extent=[0, self.Nx, 0,sol.t[-1]])
        ax1.set_title('Velocity u')
        ax1.set_ylabel('Time [s]')
        ax1.set_xlabel('Square index')
        sm = mpl.cm.ScalarMappable(cmap=cmap)
        norm1 = mpl.colors.Normalize(vmin=np.min(du), vmax=np.max(du))
        cbar1 = fig.colorbar(sm,cmap=cmap, norm = norm1, orientation='horizontal')
        cbar1.set_label('Normalized velocity')
        plt.tight_layout()
        
        
        
        # fig1 = plt.figure('Wave propogation 2D cell displacement vertical')
        # ax2 = fig1.add_subplot(1,2,1)
        # cmap = plt.get_cmap('jet')
        # sm2 = ax2.imshow(v, aspect='auto', origin = 'upper', interpolation ='spline16', cmap =cmap,
        #              vmin=np.min(v), vmax=np.max(v), extent=[0,self.Nx, 0,sol.t[-1]])
        # ax2.set_title('Displacement v')
        # ax2.set_ylabel('Time [s]')
        # ax2.set_xlabel('Square index')
        # sm = mpl.cm.ScalarMappable(cmap=cmap)
        # norm2 = mpl.colors.Normalize( vmin=np.min(v), vmax=np.max(v))
        # cbar2 = fig1.colorbar(sm,cmap=cmap, norm = norm2, orientation='horizontal')
        # cbar2.set_label('Normalized displacement')

        # ax3 = fig1.add_subplot(1,2,2)
        # cmap = plt.get_cmap('jet')
        # sm3 = ax3.imshow(dv, aspect='auto', origin = 'upper', interpolation ='spline16', cmap =cmap, 
        #              vmin=np.min(dv), vmax=np.max(dv), extent=[0,self.Nx, 0,sol.t[-1]])
        # ax3.set_title('Velocity v')
        # ax3.set_ylabel('Time [s]')
        # ax3.set_xlabel('Square index')
        # sm = mpl.cm.ScalarMappable(cmap=cmap)
        # norm3 = mpl.colors.Normalize( vmin=np.min(dv), vmax=np.max(dv))
        # cbar3 = fig1.colorbar(sm,cmap=cmap, norm = norm3, orientation='horizontal')
        # cbar3.set_label('Normalized velocity')
        # plt.tight_layout()
        
        
        
         ###########rotation ##############3

        # fig2 = plt.figure('Wave propogation 2D cell rotation')
        # ax4 = fig2.add_subplot(1,2,1)
        # cmap = plt.get_cmap('jet')
        # sm4 = ax4.imshow(w, aspect='auto', origin = 'upper', interpolation ='spline16', cmap =cmap, 
        #              vmin=np.min(w), vmax = np.max(w), extent=[0,self.Nx, 0,sol.t[-1]])
        # ax4.set_title('Rotation $\u03B8 _1$')            
        # ax4.set_ylabel('Time [s]')
        # ax4.set_xlabel('Square index')
        # sm = mpl.cm.ScalarMappable(cmap=cmap)
        # norm4 = mpl.colors.Normalize(vmin=np.min(w), vmax = np.max(w))
        # cbar4 = fig2.colorbar(sm,cmap=cmap, norm= norm4, orientation='horizontal')
        # cbar4.set_label('Normalized rotation')


        # ax5 = fig2.add_subplot(1,2,2)
        # cmap = plt.get_cmap('jet')
        # sm5 = ax5.imshow(dw, aspect='auto', origin = 'upper', interpolation ='spline16', cmap =cmap, 
        #              vmin=np.min(dw),  vmax = np.max(dw), extent=[0,self.Nx, 0,sol.t[-1]])
        # ax5.set_title('Angular velocity $\omega$')              
        # ax5.set_ylabel('Time [s]')
        # ax5.set_xlabel('Square index')
        # # sm = mpl.cm.ScalarMappable(cmap=cmap)
        # norm5 = mpl.colors.Normalize(vmin=np.min(dw), vmax = np.max(dw))
        # cbar5 = fig2.colorbar(sm,cmap=cmap, norm = norm5, orientation='horizontal')
        # cbar5.set_label('Normalized angular velocity')
        # plt.tight_layout()
        # #############create energy plot##################

        
        
        return(u,v,w,du,dv,dw)
    
if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    #==============================================================================
    df=3;                  #the number of freedoms on each node.
    Kl=1*670.8;
    Kj=1*0.4*0.00044325837185769376;
    Ks=1*Kl;
    M=1*2.438*1e-3;
    
    L=10.5*1e-3/(2*np.cos(np.pi/4));
    Theta0 = np.pi*(0/180);
    ThetaLin= np.pi*(0/180);
    J=(1/12)*M*L*L;
    Theta_morse=0.74422634;
    A=2*0.0005477163091763655;
    alpha=4.966064409064443;
    damp_flag = 1; #damping 
    # cl = 0.34; cj = 13.67;
    #=====================GEOMETRY AND MESH GENERATION========================%
    Nx      = 4;       #number of nodes along x
    Ny      = 5;       #number of nodes along y
    Num_link=(Nx-1)*Ny+(Ny-1)*Nx;
    Lx=1*2*L;
    Ly=1*2*L;
    
    #system modifications 
    Defect_loc = np.zeros(2)
    opt=0;#opt=1 #if defects are assigned, otherwise, no defect
    di1=10;
    dj1=2; 
    Defect_loc[0]=di1+(dj1-1)*Nx;
    di2=40;
    dj2=2; 
    Defect_loc[1]=di2+(dj2-1)*Nx;
    # di3=18;dj3=18;
    # Defect_loc(3)=di3+(dj3-1)*Nx;
    
    #simulation parameters 
    ex_loc1=0;
    ex_loc2= Nx*Ny-1;
    
    #save file
    CNT=1;
    count1=0;
    
    #simulation parameters 
    timeINT1=np.linspace(0,1,int(1e5));
    timeINT2=np.linspace(0,0.2,1000);
    initCOND1=np.zeros(2*df*Nx*Ny); p=2.4;
    initCOND1[6*(ex_loc1)+3]= -3*p;#initCOND1(6*(ex_loc1)-1)= -0.2*p ;
    initCOND1[6*(ex_loc2)+3]= 3*p; #initCOND1(6*(ex_loc2)-1)= 0.15*p ;
    
    
    rotsq_dym = RotSq_Dynamics_unit(df, Kl, Kj, Ks, M, L, Theta0, ThetaLin, J, Theta_morse,
                 A, alpha, damp_flag, Nx, Ny, Num_link, Lx, Ly)
    print('Dynamic simulation for {:d} by {:d} rotating square sample:'.format(Nx, Ny))
    tic = time.perf_counter()
    print("Simulation starting:")
    
    sol = rotsq_dym.dynamic_solver(timeINT1, initCOND1, opt, Defect_loc)
        
    t_elapse = time.perf_counter()-tic
    
    print("Simulation elapse time : {:.4f}s".format(t_elapse))
    
    print("Start visualization process:")
    
    u,v,w,du,dv,dw = rotsq_dym.Visualization(sol)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    