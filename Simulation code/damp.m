function [cl,cj]=damp(t, damp_flag, Nx, Ny)
N = Nx*Ny; 
if damp_flag ==0
    cl = 0; cj = 0;
else
    cl= 3*1e-8*(t/0.01)^1.2;cj=3.4*136.7*1e-8*(t/0.01)^1.2;%damping 
% elseif t>=0.12
%     cl=4*3.4*3*1e-8*(t/0.01)^2.5;cj=4*3.4*136.7*1e-8*(t/0.01)^2.5;%damping 
% else
%     cl=8*3.4*3*1e-8;cj=8*3.4*136.7*1e-8;%damping 
end 
end