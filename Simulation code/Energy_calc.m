function [Etot] = Energy_calc(t,x, CO, CM, Nx, df, Theta0, L, alpha, ThetaLin,...
                            Theta_morse, Kl, Ks, Kj, A, M,J)
spacing=round(length(t)/50);
E1=zeros(length(1:spacing:length(t)),1);
E2=zeros(length(1:spacing:length(t)),1);
E3=zeros(length(1:spacing:length(t)),1);
count1=1;
for i=1:spacing:length(t)
for j=1:length(CM(:,1))
        n1=1+mod(floor((CM(j,1)-1)/Nx),2)+CM(j,1)-Nx*floor((CM(j,1)-1)/Nx)-1;
        n2=n1+1;
%         n2=2+mod(floor((CM(j,2)-1)/Nx),2)+CM(j,2)-Nx*floor((CM(j,2)-1)/Nx)-1;
    if CO(CM(j,1),2)==CO(CM(j,2),2)
    dx=x(i,2*df*CM(j,2)-5)-x(i,2*df*CM(j,1)-5)-L*cos(x(i,2*df*CM(j,2)-3)+Theta0)...
        -L*cos(x(i,2*df*CM(j,1)-3)+Theta0)+2*L*cos(Theta0);
    dy=x(i,2*df*CM(j,2)-4)-x(i,2*df*CM(j,1)-4)-...
        (-1)^(n2)*L*sin(x(i,2*df*CM(j,2)-3)+Theta0)-...
        (-1)^(n1)*L*sin(x(i,2*df*CM(j,1)-3)+Theta0);
    elseif CO(CM(j,1),1)==CO(CM(j,2),1)
    dy=x(i,2*df*CM(j,2)-5)-x(i,2*df*CM(j,1)-5)+...
        (-1)^(n2)*L*sin(x(i,2*df*CM(j,2)-3)+Theta0)+...
        (-1)^(n1)*L*sin(x(i,2*df*CM(j,1)-3)+Theta0);
    dx=x(i,2*df*CM(j,2)-4)-x(i,2*df*CM(j,1)-4)-L*cos(x(i,2*df*CM(j,2)-3)+Theta0)...
        -L*cos(x(i,2*df*CM(j,1)-3)+Theta0)+2*L*cos(Theta0);
    end
    dtheta=x(i,2*df*CM(j,1)-3)+x(i,2*df*CM(j,2)-3)+2*Theta0-2*ThetaLin;
    Vmorse1=A*(exp(2*alpha*(dtheta+2*ThetaLin-2*Theta_morse))-...
    2*exp(alpha*(dtheta+2*ThetaLin-2*Theta_morse)));
    Vmorse2=A*(exp(-2*alpha*(dtheta+2*ThetaLin+2*Theta_morse))-...
    2*exp(-alpha*(dtheta+2*ThetaLin+2*Theta_morse)));
    Vmorse=Vmorse1+Vmorse2;
    E1(count1)=E1(count1)+1*(0.5*(Kl)*dx^2+0.5*(Ks)*dy^2+0.5*Kj*dtheta^2);
    E3(count1)=E3(count1)+Vmorse;
end
    count1=count1+1;
end
count2=1;
for ll=1:spacing:length(t)
for kk=1:size(CO,1)
    E2(count2)=E2(count2)+0.5*M*x(ll,2*df*kk-2)^2+0.5*M*x(ll,2*df*kk-1)^2+...
    0.5*J*x(ll,2*df*kk)^2;
end
 count2=count2+1;
end
Etot=E1+E2+E3;
figure(4);clf
plot(t(1:spacing:length(t)),E2/2,'b');hold on
plot(t(1:spacing:length(t)),E1/2+E3/2,'r')
plot(t(1:spacing:length(t)),Etot/2,'k')
legend('Kinetic energy','Potential energy','Total energy')
ylim([0 max(abs(Etot))])
end 